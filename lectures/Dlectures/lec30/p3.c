/*	Program 3 :
*/

#include<stdio.h>
void main(){

	char x = 'A';	
	char y = 'B';

	char *ptr1 = &x;
	char *ptr2 = &y;

	printf("%d\n", *ptr1 + *ptr2);

//	printf("%p\n", ptr1 + ptr2); // Error yeil
}

/*	char 'A' cha ASCII value 65
 *	char 'B' cha ASCII value 66
 *	dogancha addition 131 yeto mahnun output 131 yeto 
 *	karan format specifier "%d" ahe.
 *	
 *	character range = [-128 to 127]
 *	Jar format specifier "%c" asla asta tar,
 *	apla code madhe output 131 ala ahe,
 *	so 127 nantar ramp up hoto mhnje 127 nantar
 *	parat -128,-127,-126,-125 paryant yeil.
 *	i.e 131 == -125. "%c" format specifier asla asta 
 *	tar ouput -125 ala asta
*/	
