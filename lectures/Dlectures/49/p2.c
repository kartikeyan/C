/*	Program 2 : 
 *
 *	auto is initialized in the function
*/

#include<stdio.h>

void main(){

	auto int x = 10;

	printf("%d\n", x);

	printf("%p\n", &x);
}

//10
//0x7ffc1ad059c4

