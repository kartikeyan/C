/*	Program 5 :
*/

#include<stdio.h>

void main(){

	register int x = 10;

	printf("%d\n", x);

	printf("%p\n", &x);

}

/*	error : address of register variable 'x' requested
 *		
 *		printf("%p\n", &x);
*/		
