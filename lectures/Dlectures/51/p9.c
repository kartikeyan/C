/*	PRogram 9 :
 *
 *	Topic : Structure Assignment / Assigning value to structure variable.
 *
*/

#include<stdio.h>

struct pGround{

	char gameName[20];
	int noOfPlayers;
};

void main(){

	struct pGround obj = {"Cricket", 11 };	

	printf("%s\n", obj.gameName);
	printf("%d\n", obj.noOfPlayers);
}
