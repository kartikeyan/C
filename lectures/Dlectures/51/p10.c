/*	PRogram 10 :
 *
 *	Topic : Structure Assignment / Assigning value to structure variable.
 *
*/

#include<stdio.h>

struct Movie{

	char name[20];
	float rating;
	double budget;
};

void main(){

	struct pGround obj = {"Kantara", 9.45, 45.60};	

	printf("%s\n", obj.name);
	printf("%f\n", obj.rating);
	printf("%ld\n", obj.budget);
}
