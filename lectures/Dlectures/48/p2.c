/*	Program 2 :
 *
 *	8 - strupr (string Uppercase)
 *
 *	There is no strupr library function
 *
 *	Using User defined function
*/

#include<stdio.h>

char *mystrupr(char *str){

	while(*str != '\0'){
	
		if(*str >= 97 && *str <= 122){
		
			*str = *str - 32;
		}

		str++;
	}

	return str;
}

void main(){

	char str[10] = {'K','a','R','T','i','k','4','5','\0'};

	puts(str);

	mystrupr(str);

	puts(str);
}
