/*	Program 3 :
 *
 *	9 - strncpy (numer of string to be copied)
 *
 *	There is no strncpy library function
 *
 *	Using User defined function
*/

#include<stdio.h>

char *mystrncpy(char *dest, char *src, int num){

	while(num != 0 && *src != '\0'){
		
		if(num <= 0){
			
			break;
		}

		*dest = *src;
		dest++;
		src++;
		num--;
	}

	*dest = '\0';

	return dest;
}

void main(){

	char *str = "Kartikeyan45";

	char name[15];

	int num = -5;

	puts(str);
	puts(name);

	mystrncpy(name,str,num);

	puts(name);
}
