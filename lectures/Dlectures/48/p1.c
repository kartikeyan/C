/*	Program 1 :
 *
 *	7 - strlwr (string lowercase)
 *
 *	There is no strlwr library function
 *
 *	Using User defined function
*/

#include<stdio.h>

char *mystrlwr(char *str){

	while(*str != '\0'){
	
		if(*str >= 65 && *str <= 90){
		
			*str = *str + 32;
		}

		str++;
	}

	return str;
}

void main(){

	char str[10] = {'K','a','R','T','i','k','4','5','\0'};

	puts(str);

	mystrlwr(str);

	puts(str);
}
