/*	Program 5 : Take no. of rows from user
 *		    
 *		    - - - - A
 *		    - - - A B
 *		    - - A B C
 *		    - A B C D
 *		    A B C D E
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
		
		char ch = 'A';

		for(int sp = row - 1; sp >= i; sp--){
		
			printf("- ");
		}
		
		for(int j = 1; j <= i; j++){
		
			printf("%c ", ch);
			ch++;
		}

		printf("\n");
	}
}
