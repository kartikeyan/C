/*	Program 4 : Take no. of rows from the user
 *		    
 *		    - - *
 *		    - * *
 *		    * * *
*/

#include<stdio.h>

void main(){
	
	int row;
	printf("Enter no. of rows :\n");
	scanf("%d", &row);

/*	for(int i = 1; i <= row; i++){
	
		for(int j = row; j >= i; j--){
		
			printf("-");
		}
		printf("\n");
	}*/

/*	for(int i = 1; i <= row; i++){
		
		for(int j = 1; j <= i; j++){
		
			printf("*");
		}

		printf("\n");
	}*/

	for(int i = 1; i <= row; i++){
	
		for(int sp = row - 1; sp >= i; sp--){
		
			printf("_ ");
		}

		for(int j = 1; j <= i; j++){
		
			printf("* ");
		}

		printf("\n");
	}
}
