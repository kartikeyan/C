/*	PRogram 9 :
*/

#include<stdio.h>

int SumArr(int *ptr, int arrSize){

	int sum = 0;

	for(int i = 0; i < arrSize; i+=2){ 		
	
		sum = sum + *(ptr + i);
		
	}
	return sum;
}

void main(){

	int arr[][3] = {1,2,3,4,5,6,7,8,9};

	int arrSize = sizeof(arr) / sizeof(int);

	int sum = SumArr(*arr, arrSize);

	printf("%d\n", sum);
}
