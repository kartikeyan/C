/*	Program 6 :
*/

#include<iostream>

int retVal(int);

int main(){

	int x = 10;

	int ret = retVal(x);

	std::cout << ret << std::endl;
}

int retVal(int x){

	int y = x + 10;
	int z = y + 20;

	return x,y,z;
}

/*
 	The return statement in the retVal function returns the value of x. However, the expression x,y,z is used for the return value. In C, the comma operator evaluates each expression sequentially and returns the value of the last expression. Therefore, the function actually returns the value of z.
*/
