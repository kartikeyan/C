/*	Program 2 :
 *
 *	C language Default Paramter navacha concept nahiye
*/

#include<stdio.h>

void fun(int , int , int );

void main(){
	
	int x = 10;
	int y = 20;
	int z = 30;

	fun(x, y, z);

	fun(x, y);	// error : too few arguments to function "fun"
			//	   
			//	   fun(x ,y);
			//	   ^
}

void fun(int x, int y, int z = 50){	// error : expected ';', ',', ')' before '=' token
					//         
					//         void fun(int x, int y, int z = 50);
					//         				^

	printf("ADD = %d\n", x+y+z);
}
