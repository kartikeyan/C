/*	Program 1:
*/

#include<stdio.h>

void fun(int , int );		// (int , int) is Declaration

void main(){

	int x,y;
	printf("Enter values :\n");
	scanf("%d %d", &x, &y);

	fun(x, y);	// (x , y) is Arguments
	
	printf("IN MAIN\n");
	printf("x :%p\n", &x);
	printf("y :%p\n", &y);
}

void fun(int x, int y){		// (int x , int y) is Parameters

	printf("%d\n", x+y);

	printf("IN FUN\n");
	printf("x :%p\n", &x);
	printf("y :%p\n", &y);
}


/*	Ha example ye dakavnyasathi ahe ki,
 *
 *	main madhla x, y cha ani fun madhla x, y 
 *	cha "Khahich samband nahiye".
 *	Doni main ani fun function madla
 *	x ani y cha address kadla cha "different"
 *	asel main madla x,y ani fun madla x,y cha.
*/	
