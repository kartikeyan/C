/*	Program 5 :
 *
 *	Important PRoblem : It has two concepts 
 *
 *			    1 - Pointer to Pointer
 *			    2 - Pointer to Array
*/

#include<stdio.h>
void main(){

	int arr1[3] = {10,20,30};
	int arr2[3] = {40,50,60};

	int *iparr[] = {&arr1, &arr2}; /*	warning : initialization of 'int *' from incompatible pointer type 'int (*)[3]'
							
							  int *iparr = {&arr1, &arr2};
							  		^
							  note : near initialization for 'iparr[0]'

						warning : initialization of 'int *' from incompatible pointer type 'int (*)[3]'
							  
							  int *iparr = {&arr1, &arr2};
							  		       ^
							  note : near initialization for 'iparr[1]'
							  */


	printf("%p\n", iparr[0]); //100
	printf("%p\n", iparr[1]); //112
}
