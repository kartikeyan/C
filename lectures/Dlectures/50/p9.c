/*	Program 9 :
 *
 *	malloc declaration : (void *)malloc(size_t size);
 *				^		^
 *			     return type      Datatype
 *			     			^
 *			     			Unsigned int
*/

#include<stdio.h>
#include<stdlib.h>     // or  : void *malloc(size_t);

int mallocAdd(){

	int *ptr = (int *)malloc(sizeof(int));

	int *ptr = (int *)malloc(sizeof(int));

	*ptr = 10;
	*ptr = 20;

	return *ptr + *ptr;
}

void main(){

	int x = mallocAdd();

	printf("%d\n", x);
}

/*
 	p9.c: In function ‘mallocAdd’:
	p9.c:17:14: error: redefinition of ‘ptr’
	   17 |         int *ptr = (int *)malloc(sizeof(int));
	      |              ^~~
	p9.c:15:14: note: previous definition of ‘ptr’ with type ‘int *’
	   15 |         int *ptr = (int *)malloc(sizeof(int));
	      |              ^~~
*/
