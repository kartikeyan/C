/*	Program 3 : 
 *
 *	Using extern storage class
*/

#include<stdio.h>

int x = 20;

void fun(){

	printf("In fun\n");
}


/*	We have conbined 3 programs, and compiled it
 *	the o/p will be error because we are giving
 *	mltiple definitions to 'x'.So that why there is
 *	error as ld return exit status 1
