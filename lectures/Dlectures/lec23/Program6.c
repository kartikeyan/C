/*	Program 6 : A B C
 *		    A B C
 *		    A B C
*/

#include<stdio.h>

void main(){

	for(int i = 1; i <= 3; i++){
	
		char ch = 'A';

		for(int j = 1; j <= 3; j++){
			
			printf("%c ", ch);
			ch++;
		}	
		
		printf("\n");	
	}
}
