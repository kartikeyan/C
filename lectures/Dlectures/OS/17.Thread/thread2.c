
/*	lect 17 : Thread
 * 		
 *	Program 2 : C madla Thread
*/

#include<stdio.h>
#include<pthread.h>

void* fun(){

	printf("Start Fun\n");
	printf("%ld\n", pthread_self());
	printf("End Fun\n");

}

void main(){
		
	long int tid;

	printf("Start Main\n");

	printf("%ld\n", pthread_self());

	pthread_create(&tid, NULL, fun, NULL);

	printf("End Main\n");
}


