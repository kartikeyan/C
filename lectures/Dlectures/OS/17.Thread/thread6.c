
//	Program 6 : MultiThreading in C

#include<stdio.h>
#include<pthread.h>
#include<unistd.h>

void *fun(){
	
	printf("Fun thread id : %ld\n", pthread_self());

	for(int i = 0; i < 3; i++){
		
		printf("fun -> i = %d\n", i);
	}

	printf("In Fun\n");
}

void *gun(){
	
	pthread_t tid;

	printf("Gun thread id : %ld\n", pthread_self());

	pthread_create(&tid, NULL, fun, NULL);

	printf("End Gun\n");
}

void main(){
	
	pthread_t tid;

	printf("Main Thread id : %ld\n", pthread_self());

	pthread_create(&tid, NULL, gun, NULL);

	printf("Exit Main\n");

	pthread_exit(NULL);
}

