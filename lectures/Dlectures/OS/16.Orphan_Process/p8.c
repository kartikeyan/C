/*	lec No - 16
 *	
 *	Program 2 : 
*/

#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

int main(){

	int pid = fork();

	if(pid > 1){							
		
		for(int i = 1; i <= 5; i++){
			
			printf("In Parent\n");
			printf("Parent = %d\n", getpid());		
			printf("Child cha Ajoba = %d\n", getppid());		
		}

	}else if(pid == 0){						//Child
		
		for(int i = 1; i <= 10; i++){
			sleep(2);

			printf("child = %d\n", getpid());
			printf("Child cha parent = %d\n", getppid());			
		}

		execlp("a.out", "Dummy", NULL);
		
	}else{
	
		return -1;
	}
}
