/*	lec No - 16
 *	
 *	Program 1 : Orphan Process Code
*/

#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

int main(){

	int pid = fork();

	if(pid > 1){							
		
		for(int i = 1; i <= 5; i++){
									// ithe wait(NULL) cha call nahi dila mahnun, parent che kaam execute zalyavar parent marun jato
			printf("Parent = %d\n", getpid());		// child cha parent asnar "systemd(Service Process)"
			printf("Child cha Ajoba = %d\n", getppid());		
		}

	}else if(pid == 0){						//Child
		
		for(int i = 1; i <= 10; i++){
			sleep(2);

			printf("child = %d\n", getpid());
			printf("Child cha parent = %d\n", getppid());			
		}		
	}else{
	
		return -1;
	}
}
