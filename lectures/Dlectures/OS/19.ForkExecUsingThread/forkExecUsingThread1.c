
#include<stdio.h>
#include<unistd.h>

void main(){
	
	int pid = fork();

	if(pid == 0){
		
		execlp("ps", "process creation", NULL);
	}else{
		
		printf("%d\n", pid);
	}

	printf("End\n");
}
