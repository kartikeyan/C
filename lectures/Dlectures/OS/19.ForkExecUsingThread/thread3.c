#include<stdio.h>
#include<pthread.h>

void *fun(){
	
	printf("fun Thread\n");

	printf("fun thread = %ld\n", pthread_self());
}

void *run(){
	
	pthread_t tid;

	printf("Child Thread\n");

	printf("Child = %ld\n", pthread_self());

	pthread_create(&tid, NULL, fun, NULL);

	printf("Child Thread End\n");
}

void main(){
	
	pthread_t tid;

	printf("Main Thread : %ld\n", pthread_self());

	pthread_create(&tid, NULL, run, NULL);

	pthread_join(tid, NULL);

	printf("Main Thread\n");
}
