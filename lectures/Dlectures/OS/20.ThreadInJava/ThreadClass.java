
/*	Prog : Creating Thread in Java
 *		
 *		1] "Using Thread Class"
 *		2] "Using Runnable Interface"
*/

//	1] Thread Class

class ThreadDemo extends Thread{
	
	public void run(){
		
		System.out.println("Child Thread");
	}

	public static void main(String[] args){
		
		System.out.println("Hello");

		//Thread Create
		ThreadDemo obj = new ThreadDemo();

		//Thread Start
		obj.start();

		System.out.println("Main Thread");
	}
}
