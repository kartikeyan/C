/*	Program 1:
 *
 *	Topic : Pointer to an Array of Array
*/

#include<stdio.h>
void main(){

	int arr1[4] = {10,20,30,40};
	int arr2[4] = {50,60,70,80};
	int arr3[4] = {90,100,110,120};

	int (*ptr1[4])[3] = {&arr1, &arr2, &arr3}; // warning

	int (*ptr2[3])[4] = {&arr1, &arr2, &arr3}; // Here [3] represents how many array are there,
						   // [4] represents array of how many elements
	
	printf("%d\n", *(*(ptr1[0]))); //10

	printf("%d\n", *(*(ptr1[1]))); //50
				       
	printf("%d\n", *(*(ptr1[2]))); //90

	printf("%d\n", *(*(ptr2[0]))); //10

	printf("%d\n", *(*(ptr2[0]) + 1)); //20
	printf("%d\n", *(*(ptr2[0]) + 2)); //30
	printf("%d\n", *(*(ptr2[0]) + 3)); //40

	printf("%d\n", *(*(ptr2[1]))); //50

	printf("%d\n", *(*(ptr2[1]) + 1)); //60
	printf("%d\n", *(*(ptr2[1]) + 2)); //70
	printf("%d\n", *(*(ptr2[1]) + 3)); //80

	printf("%d\n", *(*(ptr2[2]))); //90

	printf("%d\n", *(*(ptr2[2]) + 1)); //100
	printf("%d\n", *(*(ptr2[2]) + 2)); //110
	printf("%d\n", *(*(ptr2[2]) + 3)); //120
}

/*
 	10
	50
	90

	10

	20
	30
	40

	50

	60
	70
	80

	90

	100
	110
	120
*/
