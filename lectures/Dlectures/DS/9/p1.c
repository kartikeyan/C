/*	PRogram 1 :
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	int data;
	struct Demo *next;
}demo;

demo *head = NULL;

demo *createNode(){

	demo *newNode = (demo*)malloc(sizeof(demo));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
	}else{
	
		demo *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void addFirst(){

	demo *newNode = createNode();

	if(head == NULL){			//jar ek pan node present nasel tar head = newNode assign kar
		head = newNode;
	}else{
	
		newNode->next = head;		//jar ek pesksha jast node asel tar newNode cha next madhe head kuthe bagtoy
		head = newNode;			//tihte bag ani head = newNode assign kar
	}
}

void addAtPos(int pos){

	demo *newNode = createNode();

	demo *temp = head;
	

	while(pos - 2){

		temp = temp->next;
		pos--;
	}

	newNode->next = temp->next;
	temp->next = newNode;
}

void countNode(){

	demo *temp = head;

	int count = 0;

	while(temp != NULL){
	
		count++;
		temp = temp->next;
	}

	printf("Count Of Node is :%d\n", count);
}

void printLL(){

	demo *temp = head;

	while(temp != NULL){
	
		printf("|Data is : %d|", temp->data);

		temp = temp->next;
	}
	printf("\n");
}

void main(){

	int nodeCount;
	printf("Enter NodeCount\n");
	scanf("%d", &nodeCount);

	for(int i = 0; i < nodeCount; i++){
	
		addNode();
	}

	printLL();

	addFirst();

	printLL();
	
	int pos;
	printf("Enter Node Position\n");
	scanf("%d", &pos);

	addAtPos(pos);

	printLL();

	countNode();
}
