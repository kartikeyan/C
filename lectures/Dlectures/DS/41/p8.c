
/*	
 *	Maximum SubArray : Kadane's Alogorithm
 *
 *	T.C = O(N) and S.C = O(1)
 *
 *	Given an integer array of size N.
 *	Find the contiguous subarray (containing atleast one number) which has largest sum and return it's sum
 *	
 *	Input : arr[] = {-2,1,-3,4,-1,2,1,-5,4}
 *
 *	Ouput : 6
 *
 *	Expla : [4,-1,2,1] has the largest sum = 6
*/

#include<stdio.h>

void maxArray(int arr[], int size){
	
	int max = arr[0], sum = 0;

	for(int i = 0; i < size; i++){
		
		sum = sum + arr[i];

		if(sum < 0){		// kadane's sangto ki jar sum less than 0 asel tar sum la reset or reassign kar 0 la	
			
			sum = 0;
		}
		
		if(sum > max){		// kadane's ala karan algo cha T.C ahe O(N)
			
			max = sum;
		}
	}

	printf("%d\n", max);
}

void main(){
	
	int arr[] = {-2,1,-3,4,-1,2,1,-5,4};

	int size = sizeof(arr) / sizeof(arr[0]);

	maxArray(arr, size);
}
