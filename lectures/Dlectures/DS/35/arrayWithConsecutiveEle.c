
/*	Program 4 : Array With Consecutive Elements
 *
 *	Given an array of positive integers A, check & return whether
 *	the array elements are consecutive or not.
 *
 *	Input : The only argument given is integer array A
 *	Output : Return 1 if array elements are consecutive else return 0.
 *
 *	Input : A = [3,2,1,4,5],   Input : [1,3,2,5]
 *	Output : 1		   Ouput : 0
*/	


#include<stdio.h>

void merge(int arr[], int start, int mid, int end){
	
	int ele1 = mid - start + 1;
	int ele2 = end - mid;

	int arr1[ele1], arr2[ele2];

	for(int i = 0; i < ele1; i++){
		
		arr1[i] = arr[start + i];
	}

	for(int j = 0; j < ele2; j++){
		
		arr2[j] = arr[mid + 1 + j];
	}

	int itr1 = 0, itr2 = 0, itr3 = start;

	while(itr1 < ele1 && itr2 < ele2){
		
		if(arr1[itr1] < arr2[itr2]){
			
			arr[itr3] = arr1[itr1];
			itr1++;
		}else{
		
			arr[itr3] =  arr2[itr2];
			itr2++;
		}

		itr3++;
	}

	while(itr1 < ele1){
		
		arr[itr3] = arr1[itr1];
		itr1++;
		itr3++;
	}

	while(itr2 < ele1){
		
		arr[itr3] = arr2[itr2];
		itr2++;
		itr3++;
	}
}

void mergeSort(int arr[], int start, int end){
	
	if(start < end){
		
		int mid = (start + end) / 2;

		mergeSort(arr, start, mid);
		mergeSort(arr, mid + 1, end);

		merge(arr, start, mid, end);
	}
}

int consecutive(int arr[], int size){
	
	int start = 0, end = size - 1;

	int sum1 = arr[start] + arr[end];

	int sum2 = arr[start+1] + arr[end - 1];

	if(sum1 == sum2){
		
		return 1;
	}else{
		
		return 0;
	}
}

int main(){
	
	int arr[] = {5,2,8,4,6,3,9};

	int size = sizeof(arr) / sizeof(arr[0]);

	mergeSort(arr, 0, size - 1);

	printf("Sorted Array is\n");

	for(int i = 0; i < size; i++){
		
		printf("%d\n", arr[i]);
	}
	
	int ret = consecutive(arr, size);
	printf("Is consective %d\n", ret);
}
