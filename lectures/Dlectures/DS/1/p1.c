/*	COmpleted C but I would rate myself this marks, 
 *	because according to me you should everything 
 *	when someone asks u any concept.
 *
 *	CHAPTER Marks
 *	Array	  6
 *	Pointer   6
 *	Function  5
 *	Structure 5
 *	File Ha.  6
 *	Strings   6
*/

/*	Points needed in Data Structures
 *
 *	1 - Structure
 *	2 - Dynamic Memory Aloocation
 *	3 - Structure Pointer
*/

//	Program 1 - Structure

#include<stdio.h>
#include<string.h>

struct CricPlayer{

	int jerNo;
	char pName[10];
	float sal;

}obj1 = {18, "Virat", 5.0};

void main(){

	struct CricPlayer obj2;

	obj2.jerNo = 45;

	strcpy(obj2.pName, "Rohit");

	obj2.sal = 5.0;

	printf("%d\n", obj1.jerNo);
	printf("%s\n", obj1.pName);
	printf("%f\n", obj1.sal);

	printf("%d\n", obj1.jerNo);
	printf("%s\n", obj1.pName);
	printf("%f\n", obj1.sal);
}
