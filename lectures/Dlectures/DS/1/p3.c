/*	Program 3 : Structure Pointer
*/

#include<stdio.h>

struct Movie{
	
	char mName[10];
	int count;
	float rating;

}obj1 = {"Drishyam", 5, 8.7};

void main(){

	typedef struct Movie mv;

	mv obj2 = {"Kantara", 10, 9.5};

	struct Movie *ptr1 = &obj1;

	mv *ptr2 = &obj2;

	//Access
	printf("%s\n", (*ptr1).mName);
	printf("%d\n", (*ptr1).count);
	printf("%f\n", (*ptr1).rating);

	printf("%s\n", ptr2->mName);
	printf("%d\n", ptr2->count);
	printf("%f\n", ptr2->rating);
}
