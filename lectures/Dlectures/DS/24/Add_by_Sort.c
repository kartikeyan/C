/*
 * 	Program1 : Implementing Priority Queue Using Linked List
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	int priority;

	struct Node *next;
}node;

node *head = NULL;

int LP = 1;
int HP = 0;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	getchar();

	newNode->priority = LP - 1;

	while(newNode->priority < LP || newNode->priority > HP){
	
		printf("Enter Priority\n");
		scanf("%d", &newNode->priority);
	}

	getchar();

	newNode->next = NULL;

	return newNode;
}

int addByAsce(){

	node *newNode = createNode();

	if(head == NULL){
		
		head = newNode;
		return 0;

	}else if(head->next == NULL){
		
		if(head->priority > newNode->priority){
		
			newNode->next = head;
			head = newNode;
		}else{
		
			head->next = newNode;
		}
	}else{
	
		if(newNode->priority < head->priority){
		
			newNode->next = head;
			head = newNode;

			return 0;
		}

		node *temp1 = head;

		while(temp1->next->next != NULL){
		
			if(temp1->priority <= newNode->priority && newNode->priority < temp1->next->priority){
			
				newNode->next = temp1->next;
				temp1->next = newNode;

				return 0;
			}

			temp1 = temp1->next;
		}

		if(temp1->priority <= newNode->priority && newNode->priority < temp1->next->priority){
		
			newNode->next = temp1->next;
			temp1->next = newNode;
		}else{
		
			temp1 = temp1->next;
			temp1->next = newNode;
		}

		return 0;
	}
}

void printLL(){

	node *temp = head;

	while(temp->next != NULL){
	
		printf("|%d|->", temp->data);
		printf("|%d|->", temp->priority);

		temp = temp->next;
	}

	printf("|%d|->", temp->data);
	printf("|%d|->", temp->priority);
}

void main(){

	while(LP >= HP){
	
		printf("Enter Lowest Priority\n");
		scanf("%d", &LP);

		printf("Enter Highest Priority\n");
		scanf("%d", &HP);
	}

	getchar();

	int ch = 0;

	while(ch != 2){
	
		printf("1.AddNode\n");
		printf("2.printLL\n");

		scanf("%d", &ch);

		getchar();

		switch(ch){
		
			case 1:
				addByAsce();
				break;
			case 2:
				printLL();
				break;
		}
	}
}
