
//	Program 1 : Insertion Sort

#include<stdio.h>

int sort(int arr[], int size){
	
	for(int i = 1; i < size; i++){
		
		int val = arr[i];
		
		int j = i - 1;					// j = i - 1 is initialized outside after for loop of j ends we need j value

		for(   ; j >= 0 && val < arr[j]; j--){		// j >= 0 condition is written first because if val < arr[j] is written first then we get segmentation fault after first iteration.
			
			arr[j+1] = arr[j];
		}
		
		arr[j+1] = val;
	}
}

void main(){
	
	int arr[] = {-1,5,3,6,-4,2};

	int size = sizeof(arr) / sizeof(arr[0]);
	
	sort(arr, size);

	printf("Sorted Elements are\n");
	for(int i = 0; i < size; i++){
		
		printf("%d\n", arr[i]);
	}
}
