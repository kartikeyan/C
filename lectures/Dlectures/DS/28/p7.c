/*	PRogram 7 :
 *
 *	Topic : Recursion in Array
*/

//	Searching an element in Character array
#include<stdio.h>
#include<stdbool.h>

bool searchArray(char arr[], int size, char search){

	for(int i = 0; i < size; i++){
	
		if(arr[i] == search){
			
			return i;
		}
	}

	return -1;
}

void main(){

	int size;
	printf("Enter Array Size\n");
	scanf("%d", &size);

	char *arr[20];

	printf("Enter Array Elements\n");	
	fgets(arr, 10, stdin);

	printf("Array Elements are\n");
	fputs(arr, stdout);
	printf("%s", arr);
	
	char search;
	printf("Enter Search Element\n");
	scanf("%s", &search);

	int ret = searchArray(arr, 10, search);
	printf("Seacrh is : %d\n",ret); 
}
