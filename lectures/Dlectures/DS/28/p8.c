/*	PRogram 8 :
 *
 *	Topic : Recursion in Array
*/

//	Searching an element in Character array
#include<stdio.h>
#include<stdbool.h>

bool searchArray(char *arr, int size, char search){

	if(size == 0){
	
		return false;
	}

	if(arr[size] == search){
	
		return true;
	}

	return searchArray(arr, size - 1, search);
}

void main(){

/*	int size;
	printf("Enter Array Size\n");
	scanf("%d", &size);

	char arr[size];

	printf("Enter Array Elements\n");	
	fgets(arr, size, stdin);


	printf("Array Elements are\n");
//	fputs(arr, stdout);
	printf("%s", arr);
	
	char search;
	printf("Enter Search Element\n");
	scanf("%s", &search);
*/	
	char arr[] = "kartik";

	char search = 'z';

	bool ret = searchArray(arr, 5, search);
	
	if(ret){
		printf("Search element Found\n");
	}else{
		printf("Search element not found\n");
	}
}
