/*	Program 3 :
*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct Movie{

	char Mname[20];
	int count;
	float price;

	struct Movie *next;
}Mov;

void accessData(Mov *ptr){

	printf("%s\n", ptr->Mname);
	printf("%d\n", ptr->count);
	printf("%f\n", ptr->price);
	printf("%p\n", ptr->next);
}

void main(){

	Mov *mv1 = (Mov *) malloc (sizeof(Mov));

	Mov *mv2 = (Mov *) malloc (sizeof(Mov));

	Mov *mv3 = (Mov *) malloc (sizeof(Mov));

	strcpy(mv1->Mname, "Dhoom1");
	mv1->count = 2;
	mv1->price = 120;

	mv1->next = mv2;

	strcpy(mv2->Mname, "Krish");
	mv2->count = 4;
	mv2->price = 150;

	mv2->next = mv3;

	strcpy(mv3->Mname, "RRR");
	mv3->count = 6;
	mv3->price = 140;

	mv3->next = NULL;

	accessData(mv1);
	accessData(mv2);
	accessData(mv3);
}
