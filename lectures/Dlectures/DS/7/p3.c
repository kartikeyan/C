/*	PRrogram 3 :
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Batsman{

	int jerNo;
	char pName[20];
	float avg;
	struct Batsman *next;
}Bat;

Bat *head = NULL;

void addNode(){

	Bat *newNode = (Bat*)malloc(sizeof(Bat));

	printf("Enter Batsman Jersey No\n");
	scanf("%d", &newNode->jerNo);

	printf("Enter Batsman Name\n");
	scanf("%s", newNode->pName);

	printf("Enter Batsman Avg\n");
	scanf("%f", &newNode->avg);

	newNode->next = NULL;

	if(head == NULL){
	
		head = newNode;
	}else{
	
		 Bat *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void printLL(){

	Bat *temp = head;

	while(temp != NULL){
	
		printf("|Batsman Jersey No. is :%d -> ", temp->jerNo);
		printf("Batsman Name is :%s ", temp->pName);
		printf("Batsman Avgerage is :%f|", temp->avg);

		temp = temp->next;
	}
}

void main(){

	addNode();
	addNode();
	addNode();
	printLL();
}
