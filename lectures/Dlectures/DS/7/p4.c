/*	PRogram 4 :
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Family{

	int members;
	char mName[20];
	float income;
	struct Family *next;
}Fam;

Fam *head = NULL;

void addNode(){

	Fam *newNode = (Fam*)malloc(sizeof(Fam));

	printf("How many Members ?\n");
	scanf("%d", &newNode->members);

	printf("Enter Members Name :\n");
	scanf("%s", newNode->mName);

	printf("Enter Family Income\n");
	scanf("%f", &newNode->income);

	newNode->next = NULL;

	if(head == NULL){
	
		head = newNode;
	}else{
	
		Fam *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void printLL(){

	Fam *temp = head;

	while(temp != NULL){
		
		printf("|How many Members:%d ->", temp->members);
		printf("Enter Members Name:%s ", temp->mName);
		printf("Enter Family Income:%f|", temp->income);

		temp = temp->next;
	}
}

void main(){

	addNode();
	addNode();
	addNode();
	printLL();
}
