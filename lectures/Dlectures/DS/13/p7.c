/*	Program 7 :
 *
 *	Topic : Singly Circular LL
 *
 *	8] deleteLast
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;
}node;

node *head = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	node *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
		newNode->next = head;
	}else{
	
		node *temp = head;

		while(temp->next != head){
		
			temp = temp->next;
		}

		temp->next = newNode;

		newNode->next = head;
	}
}

void addFirst(){

	node *newNode = createNode();

	if(head == NULL){
		
		head = newNode;
		newNode->next = head;
	}else{
	
		node *temp = head;

		while(temp->next != head){
		
			temp = temp->next;
		}

		temp->next = newNode;
		newNode->next = head;
		head = newNode;
	}
}

void addLast(){

	addNode();
}

int countNodes(){

	node *temp = head;
	
	int count = 0;

	do{
		temp = temp->next;
		count++;

	}while(temp != head);

//	printf("Count is : %d\n", count);

	return count;
}


int addAtPos(int pos){

	int count = countNodes();

	if(pos <= 0 || pos >= count + 2){
	
		printf("Invalid pos to insert\n");
		return -1;
	}else{
	
		if(pos == 1){
			
			addFirst();
		}else if(pos == count + 1){
		
			addLast();
		}else{
		
			node *newNode = createNode();

			node *temp = head;

			while(pos - 2){
			
				temp = temp->next;
				pos--;
			}

			newNode->next = temp->next;
			temp->next = newNode;
		}

		return 0;
	}
}


int deleteFirst(){

	int count = countNodes();

	if(head == NULL){
		
		printf("Nothing to delete\n");
		return -1;
	}else{ 
		if(count == 1){
	
			free(head);
			head = NULL;
		}else{

			node *temp = head;
			
			while(temp->next != head){
				
				temp = temp->next;
			}

			head = head->next;

			free(temp->next);

			temp->next = head;
		}
		return 0;
	}
}


int deleteLast(){

	int count = countNodes();

	if(head == NULL){
	
		printf("Nothing to delete\n");
		return -1;
	}else{
	
		if(count == 1){
		
			free(head);
			head == NULL;
		}else{
		
			node *temp = head;

			while(temp->next->next != head){
			
				temp = temp->next;
			}
			
			free(temp->next);
			temp->next = head;
		}

		return 0;
	}
}

int deleteAtPos(int pos){

	int count = countNodes();

	if(pos <= 0 || pos > count){
	
		printf("INvalid Pos\n");
		return -1;
	}else{
	
		if(pos == 1){
		
			deleteFirst();
		}else if(pos == count){
		
			deleteLast();
		}else{
		
			node *temp = head;

			while(pos - 2){
			
				temp = temp->next;
				pos--;
			}

			node *temp1 = temp->next;
			temp->next = temp->next->next;
			free(temp1);
		}

		return 0;
	}
}

int printLL(){
	
	if(head == NULL){
	
		printf("LL is empty\n");
		return -1;
	}

	node *temp = head;
	
	while(temp->next != head){
	
		printf("|%d|->", temp->data);

		temp = temp->next;
	}

	printf("|%d|\n", temp->data);
}

void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

/*	printLL();

	addFirst();

	printLL();

	addLast();

	printLL();

	countNodes();

	int pos;
	printf("Enter Pos to insert\n");
	scanf("%d", &pos);

	addAtPos(pos);

	*/
	
	printLL();

	deleteLast();

	printLL();

	int pos;
	printf("Enter Position\n");
	scanf("%d", &pos);

	deleteAtPos(pos);

	printLL();
}
