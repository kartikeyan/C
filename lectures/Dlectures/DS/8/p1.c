/*	Functions in Linked List
 *
 *	1] CreateNode()	   6] count()
 *	2] printLL()	   7] deleteFirst()
 *	3] addFirst()	   8] deleteLast()
 *	4] addAtPos()	   9] deleteAtPos()
 *	5] addLast()
*/

/*	Program 1 :
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Employee{

	char name[20];
	int id;

	struct Employee *next;
}Emp;

Emp *head = NULL;

Emp *createNode(){

	Emp *newNode = (Emp*)malloc(sizeof(Emp));
	
	getchar();					//Buffer madla \n gheto

	printf("Enter Employee Name\n");

	char ch;
	int i = 0;

	while((ch = getchar()) != '\n'){		//Employee char madla ek ek character ge,
							//ani compare \n sobat, jar nasel tar
		(*newNode).name[i] = ch;		//newNode chya name madhe 20 char cha array
		i++;					//madhe ek ek char bar
	}						

	printf("Enter Employee Id\n");
	scanf("%d", &newNode->id);

	newNode->next = NULL;

	return newNode;				
}							

void addNode(){

	Emp *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{	
		Emp *temp = head;

		while(temp->next != NULL){
			
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void printLL(){

	Emp *temp = head;

	while(temp != NULL){
	
		printf("|Employee Name is : %s ->", temp->name);
		printf("Employee id is : %d |", temp->id);

		temp = temp->next;
	}
	printf("\n");
}

void main(){

	int nodeCount;
	printf("Enter Node Count :\n");
	scanf("%d", &nodeCount);

	for(int i = 0; i < nodeCount; i++){
	
		addNode();
	}

	printLL();
}
