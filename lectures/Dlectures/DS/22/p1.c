/*	PRogram 1:
 *
 *	Topic : Implementing Queue USing LL
*/

#include<stdio.h>

int size = 0;
int front = -1, rear = -1;
int flag = 0;

int enqueue(int arr[]){

	if(front == -1){
	
		front++;
		rear++;
	}else if((rear == size - 1 && front == 0) || (rear == front - 1)){
	
		return -1;
	}else{
	
		if(rear == size -1){
		
			rear = -1;
		}

		rear++;
	}

	printf("Enter Data\n");
	scanf("%d", &arr[rear]);
	
}

int dequeue(int arr[]){

	if(rear == size -1){
		
		flag = 0;
		return -1;
	}else{
	
		flag = 1;

		int val = arr[front];

		if(front == rear){
		
			front = -1;
			rear = -1;
		}

		front++;

		return val;
	}
}

void main(){

	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	char choice;

	do{
		printf("1.enqueue\n");
		printf("2.dequeue\n");
		printf("3.ffront\n");
		printf("4.printQueue\n");

		int ch;
		printf("Enter Choice\n");
		scanf("%d", &ch);

		switch(ch){
		
			case 1:
				{
					int ret = enqueue(arr);

					if(ret == -1){
						
						printf("Queue OverFlow\n");
					}
				}
				break;
			case 2:
				{
					int ret = dequeue(arr);
					if(ret == -1){
						printf("Queue UnderFlow\n");
					}else{
						printf("Popped %d\n", ret);
					}
				}
				break;
			case 3:
				{
					int ret = ffront(arr);
					if(ret == -1){
						printf("Queue empty\n");
					}else{
					
						printf("Front %d\n", ret);
					}
				}
				break;
			case 4:
				{
					int ret = printQueue(arr);

					if(ret == -1){
						printf("Queue is empty\n");
					}
				}
				break;
			default:
				printf("Enter Correct Choice\n");
		}

		getchar();
		printf("Do u want to conitnue\n");
		scanf("%c", &choice);

	}while(choice == 'Y' || choice == 'y');
}
