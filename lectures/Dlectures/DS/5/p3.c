/*	PRogram 3 :
 *
 *	Wrong
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Movie{

	int count;
	char mName[20];
	float rating;
	struct Movie *next;
}Mov;

Mov *createNode(Mov *ptr){

	ptr->next = (Mov*)malloc(sizeof(Mov));
	ptr->next->next = NULL;
	return ptr->next;
}

void fillData(Mov *ptr){

	printf("Enter the count :\n");
	scanf("%d", &(ptr->count));

	printf("Enter the name of movie :\n");
	scanf("%s", (ptr->mName));

	printf("Enter rating of movie :\n");
	scanf("%f", &(ptr->rating));
}

void displayData(Mov *ptr){

	printf("Count :%d\n", ptr->count);
	printf("Name : %s\n", ptr->mName);
	printf("rating :%f\n", ptr->rating);
}

void main(){

	Mov *head = (Mov*)malloc(sizeof(Mov));

	Mov *temp = head;

	int n;

	printf("Enter the number of nodes you want to create :\n");
	scanf("%d", &n);

	for(int i = 1; i <= n; i++){
		
		temp = createNode(temp);
	}

	temp = head;

	for(int i = 1; i <= n; i++){
	
		fillData(temp);
		temp = temp->next;
	}

	temp = head;

	for(int i = 1; i <= n; i++){
	
		displayData(temp);
	}
	temp = head;
}
