/*	Program 3 :
 *
 *	Topic : Self Referential Pointer
 *
 *		Asa structure jo swatah sarkya structure cha 
 *		address store karto tyala mhantat Self Referential Structure
*/

#include<stdio.h>
#include<string.h>

typedef struct Company{

	int empCount;
	char name[20];
	float rev;

	struct Company *next;
}Com;

void main(){
	
	Com obj1, obj2, obj3;		// or struct Com obj1, obj2, obj3;

	Com *head = &obj1;
	
	head->empCount = 45;
	strcpy(head->name, "Veritas");
	head->rev = 55.5;

	printf("%d\n", head->empCount);
	printf("%s\n", head->name);
	printf("%f\n", head->rev);
	
	head->next = &obj2;

	head->next->empCount = 18;
	strcpy(head->next->name, "PTC");
	head->next->rev = 58.4;

	printf("%d\n", head->next->empCount);
	printf("%s\n", head->next->name);
	printf("%f\n", head->next->rev);
	
	head->next->next = &obj3;

	head->next->next->empCount = 10;
	strcpy(head->next->next->name, "EQ TECH");
	head->next->next->rev = 69.5;

	printf("%d\n", head->next->next->empCount);
	printf("%s\n", head->next->next->name);
	printf("%f\n", head->next->next->rev);

	head->next->next->next = NULL;
}


/*	Why we are accessing data through pointers
 *	because apan pude malloc ni memory magtoy
 *	ani toh asel heap section madhe, heap
 *	section madhe naav deta yet nahi, heap madhe
 *	apan data access karu shakto only through
 *	pointers.
*/
