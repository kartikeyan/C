/*	Program 1 :
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	struct Node *prev;
	int data;
	struct Node *next;
}node;

node *head = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	newNode->prev = NULL;

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	node *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
		head->next = head;
		head->prev = head;
	}else{
	
/*		node *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
		newNode->prev = temp;
		newNode->next = head;
		head->prev = newNode;

*/		
		head->prev->next = newNode;		// Without using temp
		newNode->prev = head->prev;
		newNode->next = head;
		head->prev = newNode;
	}
}

int addFirst(){
	
	node *newNode = createNode();

	if(head == NULL){

		head = newNode;
		head->next = head;
		head->prev = head;
	}else{
	
		newNode->next = head;
		newNode->prev = head->prev;
		head->prev->next = newNode;
		head->prev = newNode;
		head = newNode;

/*		head->prev = newNode;
 *		head = newNode;
 *		head->prev->next = head;
*/		
	}
}

void addLast(){

	addNode();
}

int countNodes(){
	
	int count = 0;

	if(head == NULL){
		
		printf("LL is empty\n");
		return -1;

	}else{
	
		node *temp = head;

		do{
			count++;
			temp = temp->next;

		}while(temp != head);

		printf("Count is  : %d\n", count);
	}

	return count;
}

int addAtPos(int pos){
	
	int count = countNodes();

	if(pos <= 0 || pos >= count + 2){
	
		printf("Invalid Pos\n");
		return -1;
	}else{
	
		if(pos == 1){
		
			addFirst();
		}else if(pos == count + 1){
		
			addLast();
		}else{
		
			node *temp = head;

			node *newNode = createNode();

			while(pos - 2){
			
				temp = temp->next;
				pos--;
			}

			newNode->next = temp->next;
			temp->next->prev = newNode;
			temp->next = newNode;
			newNode->prev = temp;
		}

		return 0;
	}
}

int deleteFirst(){

	if(head == NULL){
	
		printf("LL is empty\n");
		return -1;
	}else{
		if(head->next == head){
		
			free(head);
			head = NULL;
		}else{
		
			head->next->prev = head->prev;
			head = head->next;
			free(head->prev->next);
			head->prev->next = head;
		}
	}
}


int deleteLast(){

	if(head == NULL){
	
		printf("LL is empty\n");
		return -1;
	}else{
		if(head->next == head){
		
			free(head);
			head = NULL;
		}else{
		
			head->prev = head->prev->prev;
			free(head->prev->next);
			head->prev->next = head;
		}

		return 0;
	}
}


int deleteAtPos(int pos){
	
	int count = countNodes();

	if(pos <= 0 || pos > count){
	
		printf("Invalid Pos\n");
		return -1;
	}else{
	
		if(pos == 1){
		
			deleteFirst();
		}else if(pos == count){
		
			deleteLast();
		}else{
		
			node *temp = head;

			while(pos - 2){
			
				temp = temp->next;
				pos--;
			}
			
			temp->next = temp->next->next;
			free(temp->next->prev);
			temp->next->prev = temp;
		}

		return 0;
	}
}


int printLL(){

	if(head == NULL){
	
		printf("LL is empty\n");
		return -1;
	}else{
	
		node *temp = head;

		while(temp->next != head){
		
			printf("|%d|->", temp->data);

			temp = temp->next;
		}

		printf("|%d|\n", temp->data);
	}

	return 0;
}


void main(){
	
	char choice;

	do{
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addLast\n");
		printf("4.countNodes\n");
		printf("5.addAtPos\n");
		printf("6.deleteFirst\n");
		printf("7.deleteLast\n");
		printf("8.deleteAtPos\n");
		printf("9.printLL\n");

		int ch;
		printf("Enter Chocie\n");
		scanf("%d", &ch);

		switch(ch){
		
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				addLast();
				break;
			case 4:
				countNodes();
				break;
			case 5:
				{
				int pos;
				printf("Enter Pos\n");
				scanf("%d", &pos);


				addAtPos(pos);
				}
				break;
			case 6:
				deleteFirst();
				break;

			case 7:
				deleteLast();
				break;
			
			case 8:
				{
				int pos;
				printf("Enter Pos\n");
				scanf("%d", &pos);


				deleteAtPos(pos);
				}
				break;
			case 9:
				printLL();
				break;

			default:
				printf("Enter Correct Choice\n");

		}

		getchar();
		printf("Do u want to continue\n");
		scanf("%c", &choice);

	
	}while(choice == 'Y' || choice == 'y');

}
