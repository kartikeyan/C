/*	PRogram 9 :
 *
 *	Sum from 10 to 0 with Recursion
*/

#include<stdio.h>

int sum = 0;

void fun(int x){

	sum = sum + x;
	
	if(x != 1){
		fun(--x);
	}else{
	
		printf("%d\n", sum);
	}
}

void main(){
	fun(10);
}
