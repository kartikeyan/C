/*	PRogram 13 :
 *
 *	Factorial Recursion
 *	
 *	// Correct Version
*/

#include<stdio.h>

void factorail(int num){

	static int fact = 1;

	fact = fact * num;

	if(num != 1){
	
		factorail(--num);
	}else{

		printf("%d\n", fact);
	}
}

void main(){

	factorail(5);
}	

