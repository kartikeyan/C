/*	PRogram 7 :
 *
 *	Printing Number from 10 to 0 without Recursion
*/

#include<stdio.h>

void fun(int x){

	for(int i = x; i > 0; i--){
		printf("%d\n", i);
	}
}

void main(){
	fun(10);
}
