
/*	Program 2 : Merge 2 Unsorted Array in another array
 *
 *	Input : nums1 = [8,5,3,7],  nums2 = [2,10,4,6]
 *
 *	Ouput : nums3 = [2,3,4,5,6,7,8,10]
*/

#include<stdio.h>

void sort(int arr[],int size){
	
	for(int i = 0; i < size; i++){
		
		for(int j = 0; j < size - i - 1; j++){
			
			if(arr[j] > arr[j+1]){
				
				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
}

void sortedMerge(int arr1[], int arr2[], int arr3[], int size1, int size2){

	//Sorting array first
	sort(arr1, size1);
	sort(arr2, size2);
	
	// Merge 2 sorted arrays into arr3[]
	int i = 0, j = 0, k = 0;

	while(i < size1 && j < size2){
		
		if(arr1[i] <= arr2[j]){
			
			arr3[k] = arr1[i];
			i++;
			k++;
		}else{
			
			arr3[k] = arr2[j];
			j++;
			k++;
		}
	}

	while(i < size1){	//Merge Remaining elements of arr1[] into arr3[]
		
		arr3[k] = arr1[i];
		j++;
		k++;
	}

	while(j < size2){	//Merge remaining elements of arr2[] into arr3[]
		
		arr3[k] = arr2[j];
		j++;
		k++;
	}
}

void main(){

	int arr1[] = {8,5,3,7};

	int arr2[] = {2,10,4,6,9};
	
	int size1 = sizeof(arr1) / sizeof(arr1[0]);

	int size2 = sizeof(arr2) / sizeof(arr2[0]);
	
	//Final Merge List
	int n = size1 + size2;
	int arr3[n];
	
	sortedMerge(arr1, arr2, arr3, size1, size2);
	
	printf("Sorted Merge List\n");
	for(int i = 0; i < n; i++){
		
		printf("%d\n", arr3[i]);
	}	
}
