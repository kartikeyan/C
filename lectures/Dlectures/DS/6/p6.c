/*	PRogram 6 :
 *
 *	//Correct Way
*/	

#include<stdio.h>
#include<stdlib.h>

typedef struct Student{

	int id;
	float ht;

	struct Student *next;
}Stud;

Stud *head = NULL;	//Making head global

void addNodes(){

	Stud *newNode = (Stud*)malloc(sizeof(Stud));

	newNode->id = 1;
	newNode->ht = 5.5;
	newNode->next = NULL;

	head = newNode;
}

void printLL(){

	Stud *temp = head;

	while(temp != NULL){
	
		printf("%d ", temp->id);
		printf("%f ", temp->ht);

		temp = temp->next;
	}
	printf("\n");
}

void main(){

	addNodes();
	printLL();
}
