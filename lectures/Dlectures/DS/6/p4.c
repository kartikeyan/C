/*	Program 4 :
 *
 *	//Wrong Approach
 *
 *	//veritas question
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Student{

	int id;
	float ht;
	struct Student *next;
}Stud;

void addNodes(Stud *head){

	Stud *newNode = (Stud*)malloc(sizeof(Stud));

	newNode->id = 1;
	newNode->ht = 5.5;
	newNode->next = NULL;

	head = newNode;
}

void printLL(Stud *head){

	Stud *temp = head;

	while(temp != NULL){
	
		printf("%d\n", temp->id);
		printf("%f\n", temp->ht);

		temp = temp->next;
	}
}

void main(){

	Stud *head = NULL;

	addNodes(head);
	printLL(head);
}


