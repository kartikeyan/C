
/*	Non-Tail Recursion
*/

#include<stdio.h>

int tailDemo(int x){

	if(x == 1){
		
		return 1;
	}

	return 3+tailDemo(--x);		// ithe function chya last madhe arithemtic calculation ahe kiva mahnun Non-tail recursion
}

void main(){

	int ret = tailDemo(4);
	printf("%d\n", ret);
}
