/*	Program 3 : 
*/

#include<stdio.h>

void fun(int *x){

	printf("%p\n", x);
}

void main(){

	int a = 10;
	fun(a);		/* warning : passing argument 1 of "fun" makes pointer from
			   	     integer without a cast.
				     
				     fun(a);
				         ^
			   
			    note : expected "int *" but argument is of type "int"
			    	   
			    	   void fun(int *x)
				   	         ^

						 */
}
