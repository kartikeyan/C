/*	Program 7 :
*/

#include<stdio.h>

void main(){

	fun(10);
	fun('A');
}

void fun(int x){
	printf("%d\n",x);
}

/*	warning : implicit declaration of function "fun" 	= mhnje fun la main cha khali inialize kelay,
 *								  main la fun function la khali bhagnyachi savay nahiye
 *		  
 *		  fun(10);
 *		  ^
 *
 *	warning : conflicting types for "fun"			= fun cha returntype default "int" ahe.
 *		  
 *		  void fun(int x)
 *		  ^
 *
 *	note : previous implicit declaration of "fun" was here.
 *	       
 *	       fun(10);
 *	       ^
*/	       
