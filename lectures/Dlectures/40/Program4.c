/*	Program 4 :
*/

#include<stdio.h>

void funarr(int *arr){

	printf("%p\n", arr);
}

void main(){

	int arr1[] = {10, 20, 30, 40, 50};

	funarr(arr1);

	funarr(&arr1[1]);

}
