/*	Program 2 : 
*/

#include<stdio.h>

struct Demo{

	int x;
	float y;
};

void main(){

	struct Demo obj = {10, 20.5f};

	int arr[] = {10,20,30,40,50};

	printf("%p\n", &arr[0]);	//0x200
	printf("%p\n", arr);		//0x200

	printf("%p\n", &obj.x);		//0x100

	printf("%p\n", obj);		//?
}

/*	warning : format "%p" expects argument of type "void *", but argument 2 has type "struct Demo"
 *
 *		  printf("%p\n", obj);
 *		           ^      ^
 *		         void *   struct Demo
 *
 *	
 *	output : "0x41a400000000000a"
 *
 *	
 *	Output reasons : 1] obj ha variable ahe.
 *			 2] 10 ghetla ahe mahnun 'a' ala ahe address ahe.
*/


