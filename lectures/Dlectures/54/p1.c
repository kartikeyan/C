/*	Program 1 : 
 *
 *	Topic : Passing Structure to a Function
 *
 *		It can be done in two ways :  1 - Using "obj"
 *					      2 - Using "&obj"
 *
 *
 *		If you want to pass structure to a function
 *		then always pass it through "&obj"
 *
*/

#include<stdio.h>

struct Demo{

	int x;
	float y;
};

void main(){

	struct Demo obj = {10, 20.5f};

	printf("%p\n", obj);
}

/*	warning : format "%p" expects argument of type "void *", but argument 2 has type "struct Demo"
 *
 *		  printf("%p\n", obj);
 *		           ^      ^
 *		         void *   struct Demo
 *
 *	
 *	output : "0x41a400000000000a"
 *	
 *	Output reasons : 1] obj ha variable ahe.
 *			 2] 10 ghetla ahe mahnun 'a' ala ahe address ahe.
*/


