/*	Program 1:	WAP to print square using While loop
 *
*/

#include<stdio.h>

void main(){

	int i=1; // initialization

	while(i <= 10){
		printf("%d square is :%d\n", i, i*i);
		i++;
	}
}
