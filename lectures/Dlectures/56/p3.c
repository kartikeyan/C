/*	Program 3 :
*/

#include<stdio.h>

enum RoomMates{

	Harshal,
	Shreeyash,
	Mayur = 45,
	Kartik
};

void main(){

	enum RoomMates obj;

	printf("%d\n", Harshal);	//0
	printf("%d\n", Shreeyash);	//1
	printf("%d\n", Mayur);		//45
	printf("%d\n", Kartik);		//46
}	
