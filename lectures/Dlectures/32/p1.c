/*	Program 1 :
 *
 *	Topic : Void Pointer
*/

#include<stdio.h>
void main(){

	int x = 10;

	int *iptr = &x;

	void *vptr = &x;

	printf("%p\n", iptr);
	printf("%p\n", vptr);

	printf("%d\n", *iptr);

	printf("%d\n", *vptr);

}

/*
 * error : Dereferencing of void pointer not allowed
 *
 * void pointer la exact konacha address thevla ahe mahit nasto
 *
 * Value "dereference" karaycha asel tar void pointer la "EXPLICITLY TYPECASTING" karava lagto.
*/ 
