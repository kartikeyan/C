/*	PRogram 5 :
 *
 *	Topic : Functions
 *
 *		Declaration(Prototype) :  	
 *
 *		return-value functionName(parameters)
 *
 *						^ - Parameters can be 0 or more
*/

#include<>

work(){
	
	printf("Working.......\n");
	
}

void main(){
	
	printf("Start Main\n");

	work();

	printf("End Main\n");
}

/*	warning : return type defaults to "int"
 *
 *	work()
 *	^
*/	
