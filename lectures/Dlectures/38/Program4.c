/*	Program 4 : 
 *
 *	Topic : Constant Vaiables and Pointers
*/

#include<stdio.h>
void main(){

	int x = 10;

 	const int * const ptr = &x; /* constant int ani ptr la ahe,
				       variable int ani address constant ahe,
    				       x cha value ani address constant ahe.
			            */

	printf("%d\n", x);    // 10

	x = 30; 	/*	Error : assignment of read-only location "*ptr" 	
			// 	  	*ptr = 30
			//		     ^
			*/ 	  	     

	printf("%d\n", x);   //30
	printf("%d\n", *ptr); //30
}

/*	mhnje fakt x variable cha value ani pointer cha address constant ahe,
 *	pointer through x variable cha data change nahi karu shakat..
*/		
