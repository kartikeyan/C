/*	Program 2 : 
 *
 *	Topic : Constant Vaiables and Pointers
*/

#include<stdio.h>
void main(){

	int x = 10;

 	int * const ptr = &x; /* constant ptr la ahe,
				 variable address constant ahe,
				 x cha address constant ahe.
			      */

	printf("%d\n", x);    // 10

	*ptr = 30;	

	printf("%d\n", x);   //30
}

/*	mhnje fakt pointer cha address constant ahe,
 *	variable cha data change karu shakta.
*/		
