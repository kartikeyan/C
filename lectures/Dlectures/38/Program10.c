/*	PRogram 10 :
 *
 *	Topic : Function Types
 *		
 *		2 - Function with 1 input and output.
*/

#include<stdio.h>

void fun(char x){
	
	printf("%c\n", x);
}

void main(){
	
	fun('A');	//meaning becomes char x = A.
}
