/*	WAP to print the numbers in reverse order
 *	from given input or from given range.
*/

#include<stdio.h>

void main(){

	int x, y;

	printf("Enter value of x :\n");
	scanf("%d", &x);

	printf("Enter value of y :\n");
	scanf("%d", &y);

	for(int i=x; i>=y; i--){
		
		printf("Numbers in reverse order is : %d\n", i);
	}
}
