/*	WAP to take 5 times input of marks 
	and print sum of marks 
*/	

#include<stdio.h>

void main(){

	int add = 0;
	int x = 0;

	for(int i=1; i<=5; i++){
		
		printf("Enter %d marks\n", i);

		scanf("%d", &x);

		add = add + x;
	}

	printf("Total Marks : %d\n", add);
}
