#include<stdio.h>

void main(){

	int x,y;

	printf("Enter value of x:\n");
	scanf("%d", &x);

	printf("Enter value of y:\n");
	scanf("%d", &y);

	for(int i=x; i<=y; i++){
		
		if(i % 2 != 0){
			
			printf("%d Square is :%d\n", i, i*i);
		}else{
			printf("%d Cube is :%d\n", i, i*i*i);
		}
	}
}
