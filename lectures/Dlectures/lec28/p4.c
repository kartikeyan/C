/*	Topic : Assigning value Array to an Array
 *
 *		We cannot assign value from one array to another array like in below example.
 *		
 *		We can assign value of one variable to another variable
 *
 *		But cannot assign value of  array1 to another array2 because when we call 
 *		an array, it goes internally as address. As both address of arr1 and arr2 
 *		cannot be equal it gives error.
 *
 *		we can assign value by assigning to each index in an array.
 *
*/

#include<stdio.h>
void main(){

	int x = 10;
	int y;

	printf("x = %d\n", x);
	printf("y = %d\n", y);

	y = x;

	printf("x = %d\n", x);
	printf("y = %d\n", y);
}
