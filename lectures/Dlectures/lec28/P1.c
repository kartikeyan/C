/*	Program 1 : WAP to search an element in an array
*/

#include<stdio.h>
void main(){

	int size;
	printf("Enter Array size :\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements :\n");

	for(int i = 0; i < size; i++){
	
		scanf("%d", &arr[i]);
	}

	int search;
	printf("Enter Elements to be searched :\n");
	scanf("%d", &search);

	for(int i = 0; i < size; i++){
	
		if(search == arr[i]){
		
			printf("Element Found\n");
		}else{
			printf("Element not found\n");
		}
	}
}
