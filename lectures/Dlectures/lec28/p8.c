/*	Program 8 : Comparing arrays
 *
 *		    Below example is not an correct way
 *		    to compare two arrays
 *		    because each array has different address
*/

#include<stdio.h>
void main(){

	int arr1[3] = {1,2,3};
	int arr2[3] = {1,2,3};

	if(arr1 == arr2){
		
		printf("Arrays are Equal\n");
	}else{
	
		printf("Arrays not Equal\n");
	}
}
