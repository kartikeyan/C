
#include<stdio.h>
#include<string.h>

void myStrRev(char *str){
	
	int len = strlen(str);

	printf("%d\n", len);
		
	int start = 0, end = len - 1;

	for(int i = 0; i < len / 2; i++){
		
		char temp = str[end];
		str[end] = str[start];
		str[start] = temp;
		
		start++;
		end--;
	}
}

void main(){
	
	char arr[] = "Kartik Charkupalli";

	myStrRev(arr);

	puts(arr);
}
