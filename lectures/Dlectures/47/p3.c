/*	Program 3 :
 *	
 *	4 - strrev (string reverse)
 *	
 *	There is no strrev library function.
*/	

#include<stdio.h>
#include<string.h>

void main(){

	char str[] = "Kanha";

//	char arr[10] = strrev(str);
	
	strrev(str);

	puts(str);
}

/*
 	p3.c:17:9: warning: implicit declaration of function ‘strrev’; did you mean ‘strsep’? [-Wimplicit-function-declaration]
	   17 |         strrev(str);
	      |         ^~~~~~
	      |         strsep
	/usr/bin/ld: /tmp/ccvsAwUh.o: in function `main':
	p3.c:(.text+0x35): undefined reference to `strrev'
	collect2: error: ld returned 1 exit status
*/
