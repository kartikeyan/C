/*	Program 11 : 
 *
 *	4 - strcmp (string compare)
 *
 *	    Dec : int strcmp(char *src, char *dest);
 *
 *	    Using library Function
*/

#include<stdio.h>
#include<string.h>

void main(){

	char *str1 = "kartik";	
	char *str2 = "kartek";

	int diff = strcmp(str1, str2);

	if(diff == 0){
		printf("Strings are equal\n");
	}else{
		printf("Strings are not equal\n");
	}

	printf("%d\n", diff);	// 4
}

/*	Return type in strcmp is int, i.e difference
 *	are characters are returned.If difference is 0
 *	then strings are equal.If not then they are not equal.
 *
 *
 * 	str1 is compared with str2, in this case
 *	first 4 characaters are same in both str1
 *	and str2.'i' characater is compared with 'e'
 *	and their difference in ascii is 4 in this 
 *	case.Difference can be in negative also.
*/	
