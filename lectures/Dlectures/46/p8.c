/*	Program 8 :
*/

#include<stdio.h>
#include<string.h>

void main(){

	char *str1 = "Hardik Pandya";

	char str2[20];

	puts(str1);	

	puts(str2); 		

	strcpy(str1, str2);	

	puts(str1);

	puts(str2);
}

/*	str1 is constant char pointer which gets memory in
 *	"RODATA" frame and string cannot be changed.
 *
 *	Here in this case we are trying to copy str2 which is empty
 *	into str1 which is string, we cannot change str1 which is constant 
 *	pointer so that why segmentation fault occurs
*/	
