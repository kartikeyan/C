/*	Program 5 :
 *
 *	1 - Strlen (string length)
 *	    
 *	    Declaration : int strlen(char *);
 *
 * 	    Using library function
 *
 * 	    Strlen returns int(count) value, so return type is int.
*/	

#include<stdio.h>
#include<string.h>

void main(){

	char name[10] = {'K','L','R','a','h','u','l','\0'};	// 7

	char *str = "Virat Kohli"; 				// 11

	int lenName = strlen(name);
	int lenStr = strlen(str);

	printf("%d\n", lenName);
	printf("%d\n", lenStr);
}

//	strlen does not count '\0' but can count "spaces".
