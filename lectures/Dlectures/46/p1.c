/*	Program 1 :  
*/

#include<stdio.h>

void main(){

	char arr[10];

	printf("Enter name :\n");

	gets(arr);

	puts(arr);
}

/*	if you enter character of size greater than 10
 *	then "stack smashing is detected" takes place.
 *
 *	TO avoid this increse the size of the array
*/

/*
 	katikeyan@kartikeyan:~/bootcamp/lectures/Dlectures/46$ ./a.out 
	Enter name :
	kartik
	kartik
*/

/*
 	katikeyan@kartikeyan:~/bootcamp/lectures/Dlectures/46$ ./a.out 
	Enter name :
	kartik charkupalli
	kartik charkupalli
	*** stack smashing detected ***: terminated
	Aborted (core dumped)
*/
