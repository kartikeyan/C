/*	Program 2 :
*/

#include<stdio.h>

void main(){

	char arr[20];

	printf("Enter name :\n");

	gets(arr);

	puts(arr);
	
	char *str = arr;

	puts(str);
}

/*	string from the char array can be passed
 *	to the char pointer, because by assigning
 *	arr to char pointer, at compile time only 
 *	size of char pointer is reserved in RODATA
 *	for char pointer and for char arr in main's
 *	stack frame.
*/	
