/*	Program 6 : 
 *
 *	Inialization of Objects
*/

#include<stdio.h>

union Employee{

	int empId;
	float sal;
};

void main(){

	//problem
	union Employee emp1 = {10, 50.60};	

	//correct way
	union Employee emp2;

	emp2.empId = 15;
	printf("%d\n", emp2.empId);

	emp2.sal = 70.65;
	printf("%f\n", emp2.sal);
}

/*	warning : excess elements in an initializer list
 *		  
 *		  union Employee emp1 = {10, 50.60};
 *		  				^
*/		  				
