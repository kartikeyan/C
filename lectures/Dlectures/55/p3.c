/*	PRogram 3 : 
 *
 *	Topic : Typedef
 *
 *		It  is used to give allices i.e second name to datatype.
*/

#include<stdio.h>

typedef struct Employee{

	int empId;
	char empName[20];
	float empSal;
}Emp = {10, "kanha", 20.50};

void main(){

	struct Employee obj1 = {14700, "Jeevan", 95.50};

	Emp obj2 = {14701, "Kartik", 85.50};
}


// error : typedef "Emp" is initialized (use __typeof__ instead)
