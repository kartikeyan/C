/*	Program 7 : 
 *
 *	Inialization of Objects
*/

#include<stdio.h>

union Employee{

	int empId;
	float sal;
};

void main(){

	//problem
	union Employee emp1 = {10, 50.60};	
	printf("%d\n", emp1.empId);
	printf("%f\n", emp1.sal);
}

/*	warning : excess elements in an initializer list
 *		  
 *		  union Employee emp1 = {10, 50.60};
 *		  				^
 *	ouput : 10
 *		0.000000	  				
*/		  				
