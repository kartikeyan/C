/*	PRogram 8 :
 *
 *	Topic : Enum
*/

#include<stdio.h>

struct Emp{

	int x;
	float y;
	double z;
}obj1;

void main(){

	printf("%p\n", &obj1.x);	//0x100
	printf("%p\n", &obj1.y);	//0x104
	printf("%p\n", &obj1.z);	//0x108
}
