/*	Program 5 :
*/

#include<stdio.h>

struct Demo1{

	int x;
	float y;
	double z;
}obj1;

union Demo2{

	int x;
	float y;
	double z;
}obj2;

void main(){
	
	printf("%p\n", &obj1.x);	//0x100
	printf("%p\n", &obj1.y);	//0x104
	printf("%p\n", &obj1.z);	//0x108
	
	printf("%p\n", &obj2.x);	//0x200
	printf("%p\n", &obj2.y);	//0x200
	printf("%p\n", &obj2.z);	//0x200
}

/*	Union madhe ekach vela memory allocate hoto.
*/	
