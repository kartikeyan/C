/*	Program 17 :
 *
 *	Topic : Passing Function to a Argument
*/

#include<stdio.h>

void add(int x, int y){

	printf("%d\n", x+y);	// 30
}

void fun(int x, int y, void (*ptr)(int, int)){

	ptr(x, y);
}

void main(){

	fun(10, 20, add);
}
