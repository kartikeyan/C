/*	Program 12 :
 *
 *	Function Pointer madhe exact datatype pahije
*/

#include<stdio.h>

void add(int a, int b){

	printf("%d\n", a+b);	// 30
}

void sub(float a, float b){

	printf("%f\n", a-b);	// -0.0000 
}

void main(){

	void (*ptr)(int, int);

	ptr = add;
	ptr(10,20);

	ptr = sub;
	ptr(30,10);
}

/*	warning : assignment to "void (*)(int, int)" from incompatible pointer type "void (*)(float, float)"
 *		  
 *		  ptr = sub;
 *		      ^
*/		  
