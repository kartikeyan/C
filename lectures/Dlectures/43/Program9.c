/*	Program 9 :
 *
 *	Topic : Function Pointer / Pointer to a Function
 *
 *	Function Pointer la exact returnType pahije.
*/

#include<stdio.h>


int add(int a, int b){ 

	printf("%d\n", a+b);	// 30
}

void sub(int a, int b){

	printf("%d\n", a-b);	// 10
}

void main(){

	void (*ptr)(int, int);	

	ptr = add;			
	ptr(10,20);

	ptr = sub;
	ptr(20,10);
}					

/*	warning : assignment to "void (*)(int , int)" from incompatible pointer type "int (*)(int , int)"
 *		  
 *		  ptr = add;
 *		      ^
*/		  
