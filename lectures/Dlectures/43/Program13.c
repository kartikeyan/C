/*	Program 13 :
*/

#include<stdio.h>

void add(int a, int b){

	printf("1 = %d\n", a+b);	
	printf("2 = %d\n", a+b);
	printf("3 = %d\n", a+b);
}

void main(){

	void (*ptr)(int, int);

	ptr = add;

	ptr(10,20);
	
	printf("%p\n", ptr);

	ptr++;

	printf("%p\n", ptr);

	ptr(30,40);	
}

/*
 	1 = 30
	2 = 30
	3 = 30
	0x55833a08c149
	0x55833a08c14a
	1 = 70
	2 = 70
	3 = 70
*/
