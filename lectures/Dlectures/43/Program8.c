/*	Program 8 :
 *
 *	Topic : Function Pointer / Pointer to a Function
*/

#include<stdio.h>

void add(int a, int b){

	printf("%d\n", a+b);
}

void sub(int a, int b){


	printf("%d\n", a-b);
}

void main(){

	void (*ptr)(int, int);		// declaration : It means that ek function pointer ahe jyachakade don parameter
					//		 ahet ani function pointer cha return type ahe void.


	ptr = &add;			// add function cha address dila ahe mhnje add function cha first instruction
					// cha address asto ithe printf cha address asel.
	ptr(10,20);

	ptr = &sub;
	ptr(20,10);
}					// &arr ani arr kelyane function pointer la kahi farak padat nahi	

