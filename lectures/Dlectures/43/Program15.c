/*	Program 15 :
 *
 *	Topic : Array of Function Pointer
*/

#include<stdio.h>

void add(int x, int y){

	printf("%d\n", x+y);	// 50
}

void sub(int x, int y){

	printf("%d\n", x-y);	// 10
}	

void mult(int x, int y){

	printf("%d\n", x*y);	// 600
}

void div(int x, int y){

	printf("%d\n", x/y);	// 1
}

void main(){

	void (*ptr[4])(int, int) = {add,sub,mult,div};

	for(int i=0; i<4; i++){
		
		ptr[i](30,20);
	}
}

/*
 	50
	10
	600
	1
*/
