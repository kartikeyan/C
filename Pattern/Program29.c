/*	Program 29 : Take rows from the user
 *		     
 *		     D C B A 
 *		     e d c b
 *		     F E D C
 *		     g h e d
*/

#include<stdio.h>

void main(){
	
	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	int x = 68;
	int y = 68;

	for(int i = 1; i <= row; i++){
		
		x = y;

		for(int j = 1; j <= row; j++){
			
			if(i % 2 != 0){
				
				printf("%c ", x);
			}else{
				printf("%c ", x+32);
			}		
			x--;
		}

		printf("\n");
		y++;
	}
}
