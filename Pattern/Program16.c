/*	Program 16 : Take no. of rows from the user
 *		     
 *		     1 2 3
 *		     a b c
 *		     1 2 3
 *		     a b c
*/

#include<stdio.h>

void main(){
	
	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
			
		int x = 1;
		char ch = 'a';

		for(int j = 1; j <= row; j++){
		
			if(i % 2 == 0){
			
				printf("%c ", ch);
				ch++;
			}else{
			
				printf("%d ", x);
				x++;
			}
		}

		printf("\n");

	}
}
