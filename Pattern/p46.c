/*	Program 46 :
 *		     1  4  27
 *		     4  27 16
 *		     27 16 125
*/

#include<stdio.h>
void main(){

	int row;
	printf("Enter no. of rows :\n");
	scanf("%d", &row);
	
	int x = 1;

	for(int i = 1; i <= row; i++){
		
		x = i;

		for(int j = 1; j <= row; j++){
			
			if(x % 2 != 0){

				printf("%d ", x * x * x);
			}else{
			
				printf("%d ", x * x);
			}

			x++;
		}
		printf("\n");
	}
}
