/*	Program 19 : If take row from thge user
 *		     
 *		     A B C D
 *		     B C D E
 *		     C D F G
 *		     D E F G
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	char ch1 = 'A';
	char ch2 = 'A';

	for(int i = 1; i <= row; i++){
		
		ch1 = ch2;

		for(int j = 1; j <= row; j++){
		
			printf("%c ", ch1);
			ch1++;	
		}

		printf("\n");
		ch2++;
	}
}
