/*	Program 18 : Take no. of rows from the user
 *		     
 *		     A B C
 *		     D E F
 *		     G H I
*/

#include<stdio.h>

void main(){
	
	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	char ch = 'A';

	for(int i = 1; i <= row; i++){
	
		for(int j = 1; j <= row; j++){
		
			printf("%c ", ch);
			ch++;
		}
		printf("\n");
	}
}
