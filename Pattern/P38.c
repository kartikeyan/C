/*	Program 37 : Take no. of rows from the user
 *		     
 *		     - - *
 *		     - * *
 *		     * * *
 *		
 *		     Above pattern is a combination of 
 *
 *		     1]  - - -
 *		     	 - -
 *		     	 -
 *
 *		     2]  *
 *		     	 * *
 *		     	 * * *
 *		     	 * * * *
*/


/* 1

#include<stdio.h>

void main(){

	int row;
	printf("Enter row :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
	
		for(int j = row; j >= i; j--){
		
			printf("- ");
		}

		printf("\n");
	}
}*/

/*2

#include<stdio.h>

void main(){

	int row;
	printf("Enter row :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
	
		for(int j = 1; j <= i; j++){
		
			printf("* ");
		}
		printf("\n");
	}
}*/


// Now combination of both

#include<stdio.h>

void main(){

	int row;
	printf("Enter row :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
	
		for(int sp = row - 1; sp >= i; sp--){
		
			printf("- ");
		}

		for(int j = 1; j <= i; j++){
		
			printf("* ");
		}

		printf("\n");
	}
}
