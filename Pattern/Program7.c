/*	Program 7 : 1
 *		    4 5 
 *		    7 8 9
*/

#include<stdio.h>

void main(){

	int x = 1;

	for(int i = 1; i <= 3; i++){
	
		for(int j = 1; j <= i; j++){
		
			printf("%d ", x);
			x++;
		}
		printf("\n");
	}
}
