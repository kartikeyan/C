
/*
 	448. Find All Numbers Disappeared in an Array

	Given an array nums of n integers where nums[i] is in the range [1, n], return an array of all the integers in the range [1, n] that do not appear in nums.

	Example 1:

	Input: nums = [4,3,2,7,8,2,3,1]
	Output: [5,6]

	Example 2:

	Input: nums = [1,1]
	Output: [2]

	Constraints:

	    n == nums.length
	    1 <= n <= 105
	    1 <= nums[i] <= n
*/
import java.util.*;

class Sol{

	static List<Integer> findNumbers(int nums[]){
		
		HashSet<Integer> hs = new HashSet<>();

		for(int val : nums){
			
			hs.add(val);
		}

		List<Integer> al = new ArrayList<>();

		int n = nums.length;

		for(int i = 1; i <= n; i++){
			
			if(hs.contains(i) == false){
				
				al.add(i);
			}
		}

		return al;
	}
	
	public static void main(String[] args){
		
		int nums[] = {4,3,2,7,8,2,3,1};

		List<Integer> al = findNumbers(nums);

		System.out.println(al);		// [5, 6]
	}
}
