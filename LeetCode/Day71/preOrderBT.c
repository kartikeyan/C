
/*
 	144. Binary Tree Preorder Traversal

	Given the root of a binary tree, return the preorder traversal of its nodes' values.

	Input: root = [1,null,2,3]
	Output: [1,2,3]

	Input: root = []
	Output: []

	Input: root = [1]
	Output: [1]

	Constraints:

	    The number of nodes in the tree is in the range [0, 100].
    	    -100 <= Node.val <= 100
*/

#include<stdio.h>
#include<stdlib.h>

struct Node{
	
	int data;
	struct Node *left;
	struct Node *right;
};

struct Node *newNode(int val){
	
	struct Node *node = (struct Node*)malloc(sizeof(struct Node));

	node->data = val;
	node->left = NULL;
	node->right = NULL;

	return node;
}

void printPreOrder(struct Node *root){
	
	if(root == NULL){
		
		return;
	}

	printf("%d\n", root->data);

	printPreOrder(root->left);

	printPreOrder(root->right);
}

void main(){
	
	struct Node* root = newNode(1);
	root->left = newNode(2);
	root->right = newNode(3);
	
	root->left->left = newNode(4);
	root->left->right = newNode(5);

	printf("PreOrder traversal of binary tree is \n");
	printPreOrder(root);
}
