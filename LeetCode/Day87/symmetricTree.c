
/*
 	101. Symmetric Tree

	Given the root of a binary tree, check whether it is a mirror of itself (i.e., symmetric around its center).

	Input: root = [1,2,2,3,4,4,3]
	Output: true

	Input: root = [1,2,2,null,3,null,3]
	Output: false

	Constraints:

	    The number of nodes in the tree is in the range [1, 1000].
	    -100 <= Node.val <= 100
*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
	int key;
	struct Node *left, *right;
} Node;

Node* newNode(int key)
{
	Node* temp = (Node *)malloc(sizeof(Node));
	temp->key = key;
	temp->left = temp->right = NULL;
	return (temp);
}

bool isMirror(Node* root1, Node* root2)
{

	if (root1 == NULL && root2 == NULL)

		return true;

	if(root1 == NULL && root2 != NULL){

		return false;
	}

	if(root1 != NULL && root2 == NULL){
		
		return false;
	}

	if(root1->key != root2->key){
		
		return false;
	}
	
	return isMirror(root1->left, root2->right) && isMirror(root1->right, root2->left);

}

bool isSymmetric(Node* root)
{
	
	return isMirror(root, root);
}

int main()
{
	
	Node* root = newNode(1);

	root->left = newNode(2);
	root->right = newNode(2);

	root->left->left = newNode(3);
	root->left->right = newNode(4);

	root->right->left = newNode(4);
	root->right->right = newNode(3);

	if (isSymmetric(root))
		printf("Symmetric");
	else

		printf("Not symmetric");

	printf("\n");
	return 0;
}



