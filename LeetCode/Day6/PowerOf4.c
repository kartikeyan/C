/*	
 *	Prog 2 : Given an integer n, return true if it is a power of four.Otherwise return false.
 *		 An integer n is a power of foue, if there exists an integer x such that n == 4^x
 *
 *		 Input : n = 16,	Input : 1
 *		 Ouput : true		Output : true
 *
 *		 Input : n = 5
 *		 Ouput : false
*/

#include<stdio.h>
#include<stdbool.h>

bool isPower(int num){
	
	if(num == 1){
		
		return true;
	}

	if(num <= 0){
		
		return false;
	}

	if(num % 4 != 0){
		
		return false;
	}

	return isPower(num / 4);
}

void main(){
	
	int num;
	printf("Enter NUm\n");
	scanf("%d", &num);

	int ret = isPower(num);
	printf("%d\n", ret);
}
