
/*
 	2148. Count Elements With Strictly Smaller and Greater Elements 

	Given an integer array nums, return the number of elements that have both a strictly smaller and a strictly greater element appear in nums.

	Input: nums = [11,7,2,15]
	Output: 2
	Explanation: The element 7 has the element 2 strictly smaller than it and the element 11 strictly greater than it.
	Element 11 has element 7 strictly smaller than it and element 15 strictly greater than it.
	In total there are 2 elements having both a strictly smaller and a strictly greater element appear in nums.

	Input: nums = [-3,3,3,90]
	Output: 2
	Explanation: The element 3 has the element -3 strictly smaller than it and the element 90 strictly greater than it.
	Since there are two elements with the value 3, in total there are 2 elements having both a strictly smaller and a strictly greater element appear in nums.

	Constraints:

	    1 <= nums.length <= 100
	    -105 <= nums[i] <= 105
*/

#include<stdio.h>
#include<limits.h>

int countElements(int arr[], int size){
		
	int max = INT_MIN;

	int min = INT_MAX;

	for(int i = 0; i < size; i++){
			
		if(arr[i] > max){
				
			max = arr[i];
		}

		if(arr[i] < min){
				
			min = arr[i];
		}
	}

	int count = 0;

	for(int i = 0; i < size; i++){
			
		if(arr[i] < max && arr[i] > min){
				
			count++;
		}
	}

	return count;
}
	
void main(){
		
	int arr[] = {11,7,2,15};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = countElements(arr, size);

	printf("%d\n",ret);
}
