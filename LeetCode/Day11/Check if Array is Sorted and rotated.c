
/*	Program 2 : Check if Array is Sorted and rotated
 *
 *	Given an array "nums", return "true", if the array was originally sorted in non-decreasing
 *	order, then rotated some number of positions(including zero).Otherwise, return "false"
 *
 *	Input : nums[] = {3,4,5,1,2};
 *	Ouput : true
*/

#include<stdio.h>
#include<stdbool.h>

bool check(int arr[], int size){
	
	int count = 0;

	for(int i = 1; i < size; i++){
		
		if(arr[i-1] > arr[i]){
			
			count++;
		}
	}

	if(arr[size-1] > arr[0]){
		
		count++;
	}

	return count <= 1;
}

void main(){
	
	int arr[] = {3,4,5,1,2};

	int size = sizeof(arr) / sizeof(arr[0]);

	bool ret = check(arr, size);

	if(ret == true){
		
		printf("Array is Sorted and rotated\n");
	}else{
		
		printf("Array is not sorted and rotated\n");
	}

}
