
/*	
	1913. Maximum Product Difference Between Two Pairs

	The product difference between two pairs (a, b) and (c, d) is defined as (a * b) - (c * d).

    	For example, the product difference between (5, 6) and (2, 7) is (5 * 6) - (2 * 7) = 16.

	Given an integer array nums, choose four distinct indices w, x, y, and z such that the product difference between pairs (nums[w], nums[x]) and (nums[y], nums[z]) is maximized.

	Return the maximum such product difference.

	Input: nums = [5,6,2,7,4]
	Output: 34
	Explanation: We can choose indices 1 and 3 for the first pair (6, 7) and indices 2 and 4 for the second pair (2, 4).
	The product difference is (6 * 7) - (2 * 4) = 34.

	Input: nums = [4,2,5,9,7,4,8]
	Output: 64
	Explanation: We can choose indices 3 and 6 for the first pair (9, 8) and indices 1 and 5 for the second pair (2, 4).
	The product difference is (9 * 8) - (2 * 4) = 64.

	Constraints:

	    4 <= nums.length <= 104
	    1 <= nums[i] <= 104
*/

#include<stdio.h>

void merge(int nums[], int start, int mid, int end){

	int ele1 = mid - start + 1, ele2 = end - mid;

    	int nums1[ele1], nums2[ele2];

    	for(int i = 0; i < ele1; i++){
        	nums1[i] = nums[start + i];
    	}

    	for(int j = 0; j < ele2; j++){
        	nums2[j] = nums[mid + 1 + j];
	}

	int itr1 = 0, itr2 = 0, itr3 = start;

    	while(itr1 < ele1 && itr2 < ele2){

        	if(nums1[itr1] < nums2[itr2]){
            		nums[itr3] = nums1[itr1];
  	          	itr1++;
        	}else{
            		nums[itr3] = nums2[itr2];
	            itr2++;
        	}

	        itr3++;
	 }

    	while(itr1 < ele1){

        	nums[itr3] = nums1[itr1];
        	itr1++;
        	itr3++;
    	}

        while(itr2 < ele2){
        	nums[itr3] = nums2[itr2];
	        itr2++;
        	itr3++;
	}
}

void mergeSort(int nums[], int start, int end){
	
	if(start < end){

        	int mid = start + (end - start) / 2;

        	mergeSort(nums, start, mid);
        	mergeSort(nums, mid+1, end);

        	merge(nums, start, mid, end);
    	}
}

int maxProductDiff(int arr[], int size){
	
	mergeSort(arr, 0, size-1);

	return (arr[size-1] * arr[size-2]) - (arr[0] * arr[1]);
}

void main(){
	
	int arr[] = {5,6,2,7,4};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = maxProductDiff(arr, size);

	printf("%d\n", ret);
}
