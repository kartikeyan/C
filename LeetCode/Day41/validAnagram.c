
/*	PRog : Valid Anagram
 *
 *	Given two strings s and t, return true if t is an anagram of s, and false otherwsie
 *
 *	An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.
 *
 *	Input : s = "anagram", t = "nagaram"
 *	Ouput : true
 *
 *	Input : s = "rat", t = "car"
 *	Ouput : false
 *
 *	Constraints :
 *
 *		1 <= s.length, t.length <= 5 * 10^4
 *
 *		s and t consist of lowercase English letters
*/

#include<stdio.h>
#include<stdbool.h>

bool isAnagram(char *arr1, char *arr2){
	
	int count1[256] = {0};
	int count2[256] = {0};

	int i;

	for(i = 0; arr1[i] && arr2[i]; i++){
		
		count1[arr1[i]]++;
		count2[arr2[i]]++;
	}

	if(arr1[i] || arr2[i]){
		
		return false;
	}

	for(int i = 0; i < 256; i++){
		
		if(count1[i] != count2[i]){
			
			return false;
		}
	}

	return true;
}

void main(){
	
	char arr1[] = "anagram";
	char arr2[] = "nagaram1";

	bool ret = isAnagram(arr1, arr2);

	if(ret == true){
		
		printf("Anagram Strings\n");
	}else{
		printf("Not an anagram strings\n");
	}
}
