
/*
 	345. Reverse Vowels of a String

	Given a string s, reverse only all the vowels in the string and return it.

	The vowels are 'a', 'e', 'i', 'o', and 'u', and they can appear in both lower and upper cases, more than once.

	Input: s = "hello"
	Output: "holle"

	Input: s = "leetcode"
	Output: "leotcede"

	Constraints:

	    1 <= s.length <= 3 * 105
	    s consist of printable ASCII characters.
*/

class Sol{

	static boolean isVowel(char c){
		
		return (c == 'a' || c == 'A' || c == 'e' || c == 'E' || c == 'i' || c == 'I' || c == 'o' || c == 'O' || c == 'u' || c == 'U');

	}

	static String reverseVowels(String s){
		
		int start = 0, end = s.length() - 1;

		String st = "aeiouAEEIOU";

		char str[] = s.toCharArray();

		while(start < end){
			
			if(!isVowel(str[start])){
				
				start++;
				continue;
			}

			if(!isVowel(str[end])){
				
				end--;
				continue;
			}

			char temp = str[start];
			str[start] = str[end];
			str[end] = temp;

			start++; 
			end--;
		}

		String str2 = String.copyValueOf(str);

		return str2;
	}
	
	public static void main(String[] args){
		
		String str = "leetcode";

		String ret = reverseVowels(str);

		System.out.println("Ret is " + ret);
	}
}
