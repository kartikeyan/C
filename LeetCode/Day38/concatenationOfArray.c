
/*	Prog : Concatenation OF Array
 *
 *	Given an integer array nums of length n, you want to create an array ans of length 2n where ans[i] == nums[i] and ans[i+n] == nums[i] for 0 <= i < n (0-indexed)
 *
 *	Specially, ans is the concatenation of two nums arrays
 *
 *	Return the array ans
 *
 *	Input : nums = [1,2,1]
 *	Ouput : [1,2,1,1,2,1]
 *	Expla : The array ans is formed as follows :
 *		- ans = [nums[0],nums[1],nums[2],nums[0],nums[1],nums[2]]
 *		- ans = [1,2,1,1,2,1]
 *
 *
 *	Input : nums = [1,3,2,1]
 *	Ouput : [1,3,2,1,1,3,2,1]
 *	Expla : The array ans is formed as follows :
 *		- ans = [nums[0],nums[1],nums[2],nums[3],nums[0],nums[1],nums[2], nums[3]]
 *		- ans = [1,3,2,1,1,3,2,1]
 *
 *	
 *	Constraints :
 *
 *		n == nums.length
 *
 *		1 <= n <= 1000
 *
 *		1 <= nums[i] <= 1000
*/		

#include<stdio.h>
#include<stdlib.h>

int *getConcatenation(int arr[], int size, int* returnSize){
	
	int *nums = (int*)calloc(sizeof(int), 2 * size);

	*returnSize = 2 * size;

	int k = 0;

	for(int i = 0; i < size; i++){
		
		nums[k++] = arr[i];
	}

	for(int i = 0; i < size; i++){
		
		nums[k++] = arr[i];
	}
	
	return nums;	
}

void main(){
	
	int arr[] = {1,3,2,1};

	int size = sizeof(arr) / sizeof(arr[0]);

	int *returnSize;

	int *ptr = getConcatenation(arr, size, returnSize);
}
