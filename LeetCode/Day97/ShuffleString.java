
/*
 	1528. Shuffle String

	You are given a string s and an integer array indices of the same length. The string s will be shuffled such that the character at the ith position moves to indices[i] in the shuffled string.

	Return the shuffled string.

	Input: s = "codeleet", indices = [4,5,6,7,0,2,1,3]
	Output: "leetcode"
	Explanation: As shown, "codeleet" becomes "leetcode" after shuffling.

	Input: s = "abc", indices = [0,1,2]
	Output: "abc"
	Explanation: After shuffling, each character remains in its position.

	Constraints:

    		s.length == indices.length == n
    		1 <= n <= 100
    		s consists of only lowercase English letters.
    		0 <= indices[i] < n
    		All values of indices are unique.
*/

class Sol{

	static String restoreString(String str, int indices[]){
		
		char arr[] = new char[str.length()];

		for(int i = 0; i < arr.length; i++){
			
			arr[indices[i]] = str.charAt(i);
		}

		return new String(arr);
	}
	
	public static void main(String[] s){
		
		String str = "codeleet";

		int indices[] = {4,5,6,7,0,2,1,3};

		String ret = restoreString(str, indices);

		System.out.println("Ret is " + ret);
	}
}
