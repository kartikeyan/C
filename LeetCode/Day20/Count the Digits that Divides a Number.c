
/*	Prog : Count the Digits that Divides a Number
 *
 *	Given an integer num, retunr the number of digits in num that divide num.
 *	An integer val divides nums, if "nums % val == 0".
 *
 *	Input : num = 7
 *	Ouput : 1
 *	Expla : 7 divides itself, hence the answer is 1.
 *
 *	Input : num = 121
 *	Ouput : 2
 *	Expla : 121 is divisible by 1, but not 2. Since 1 occurs twice as a digit, we return 2
 *
 *	Input : num = 1248
 *	Ouput : 4
 *	Expla : 1248 divides by all of its digits, hence the answer is 4
*/

#include<stdio.h>

int countDigits(int num){
	
	int count = 0, temp = num;

	while(temp){
		
		int rem = temp % 10;

		if(num % rem == 0){
			
			count++;
		}

		temp = temp / 10;
	}

	return count;
}

void main(){
	
	int num = 121;

	int ret = countDigits(num);

	printf("Ret is %d\n", ret);
}
