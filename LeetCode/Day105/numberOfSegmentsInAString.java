
/*
 	434. Number of Segments in a String

	Given a string s, return the number of segments in the string.

	A segment is defined to be a contiguous sequence of non-space characters.

	Input: s = "Hello, my name is John"
	Output: 5
	Explanation: The five segments are ["Hello,", "my", "name", "is", "John"]

	Input: s = "Hello"
	Output: 1

	Constraints:

	    0 <= s.length <= 300
	    s consists of lowercase and uppercase English letters, digits, or one of the following characters "!@#$%^&*()_+-=',.:".
	    The only space character in s is ' '.
*/

import java.util.*;

class Sol{

	static int countSegments(String s){
			
		StringTokenizer str = new StringTokenizer(s, " ");
		
		//	1
	/*
		System.out.println(str.countTokens());

		return str.countTokens();
	*/
		//	2

		int count = 0;

		while(str.hasMoreTokens()){
			
			count++;
			str.nextToken();
		}

		return count;
	}
	
	public static void main(String[] args){
		
		String s = "Hello, my name is kartik";

		int ret = countSegments(s);

		System.out.println("Segments are : " + ret);
	}
}

/*
 	5
	Segments are : 5
*/
