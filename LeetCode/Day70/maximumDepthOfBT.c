
/*
 	104. Maximum Depth of Binary Tree

	Given the root of a binary tree, return its maximum depth.

	A binary tree's maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.

	Input: root = [3,9,20,null,null,15,7]
	Output: 3

	Input: root = [1,null,2]
	Output: 2


	Constraints:

	    The number of nodes in the tree is in the range [0, 104].
	    -100 <= Node.val <= 100
*/

#include<stdio.h>
#include<stdlib.h>

struct Node{
	
	int data;
	struct Node *left;
	struct Node *right;
};

int maxDepth(struct Node *root){
	
	if(root == NULL){
		
		return 0;
	}

	int lDepth = maxDepth(root->left);

	int rDepth = maxDepth(root->right);

	if(lDepth > rDepth){
		
		return lDepth+1;
	}else{
		
		return rDepth+1;
	}
}

struct Node *newNode(int val){
	
	struct Node *node = (struct Node*)malloc(sizeof(struct Node));

	node->data = val;

	node->left = NULL;
	node->right = NULL;

	return node;
}

void main(){

	struct Node *root = newNode(1);

	root->left = newNode(2);
	root->right = newNode(3);

	root->left->left = newNode(4);
	root->left->right = newNode(5);

	printf("Height of tree is %d ", maxDepth(root));

	printf("\n");
}
