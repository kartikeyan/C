
/*

260. Single Number III

Given an integer array nums, in which exactly two elements appear only once and all the other elements appear exactly twice. Find the two elements that appear only once. You can return the answer in any order.

You must write an algorithm that runs in linear runtime complexity and uses only constant extra space.

Input: nums = [1,2,1,3,2,5]
Output: [3,5]
Explanation:  [5, 3] is also a valid answer.

Input: nums = [-1,0]
Output: [-1,0]

Input: nums = [0,1]
Output: [1,0]

Constraints:

    2 <= nums.length <= 3 * 104
    -231 <= nums[i] <= 231 - 1
    Each integer in nums will appear twice, only two integers will appear once
*/

#include<stdio.h>
#include<stdlib.h>

void singleNumber(int *arr, int size, int *new){
	
	int i,j, k = 0;

	for(int i = 0; i < size; i++){
		
		int count = 0;

		for(int j = 0; j < size; j++){
			
			if(arr[j] == arr[i]){
				
				count++;
			}

			if(count > 1){
				
				break;
			}
		}

		if(count == 1){
			
			new[k++] = arr[i];
		}
	}
}

void main(){
	
	int arr[] = {1,2,1,3,2,5};

	int size = sizeof(arr) / sizeof(arr[0]);

	int *new = (int*)calloc(sizeof(int), 2);

	singleNumber(arr, size, new);

	for(int i = 0; i < 2; i++){
		
		printf("%d\n", new[i]);
	}
}

