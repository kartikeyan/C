
/*	Prog : Two Sum
 *
 *	Given an array of integers nums and an integer target, return indices of the two numbers such that that add up to target.
 *
 *	You may assume that each input would have exactly one solution and you may not use the same element twice.
 *
 *	You can return the answer in any order.
 *
 *	
 *	Input : nums = [2,7,11,15], target = 9
 *	Ouput : [0,1]
 *	Expla : Because nums[0] + nums[1] == 9, we return [0,1].
 *
 *	Input : nums = [3,2,4], target = 6
 *	Ouput : [1,2]
 *
 *	Input : nums = [3,3], target = 6
 *	Ouput : [0,1]
 *
 *	Constraints :
 *
 *		2 <= nums.length <= 10^4
 *
 *		-10^9 <= nums[i] <= 10^9
 *
 *		-10^9 <= target <= 10^9
 *
*/

#include<stdio.h>
#include<stdlib.h>

int *twoSum(int arr[], int size, int target, int *new){

	for(int i = 0; i < size; i++){
		
		for(int j = i + 1; j < size; j++){
			
			if(arr[i] + arr[j] == target){

				new[0] = i;
				new[1] = j;

				break;
			}
		}
	}
}

void main(){
	
	int arr[] = {2,7,11,15};

	int target = 9;

	int size = sizeof(arr) / sizeof(arr[0]);
	
	int *new = (int*)calloc(sizeof(int), 2);

	twoSum(arr, size, target, new);
	
	for(int i = 0; i < 2; i++){
		
		printf("%d\n", new[i]);
	}
}
