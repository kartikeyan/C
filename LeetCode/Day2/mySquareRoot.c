
/*	PRogram 2 : Square Root of X
 *
 *	Given a non-integer x, return the square root of x rounded
 *	down to the nearest integer. The returned integer should
 *	be non-negative as well.
 *
 *	Input : x = 4 
 *	Ouput : 2    
 *
 *	Input : x = 8
 *	Output : 2
 *	Explanation : The square root of 8 is 2.82842..., 
 *		      and since we round it down to
 *		      the nearest integer, 2 is returned
*/

#include<stdio.h>

int mySqrt(int num){
	
	int start = 0 , end = num;

	long int ans = -1, mid;

	while(start <= end){
		
		mid = start + (end - start) / 2;

		long int square = mid * mid;

		if(square == num){
			
			return mid;
		}
		if(square < num){
			
			ans = mid;
			start = mid + 1;
		}
		else{
			end = mid - 1;
		}
	}

	return ans;
}

void main(){
	
	int num;
	printf("Enter Number\n");
	scanf("%d", &num);

	int ret = mySqrt(num);
	printf("Square Root is : %d\n", ret);
}
