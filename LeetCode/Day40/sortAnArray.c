
/*	Prog : Sort an Array
 *
 *	Given an array of integers nums, sort  the array in ascending order and return it.
 *
 *	You must solve the problem "without using any built-in" functions in "O(nlog(n))" time complexity and with the smallest space complexity possible.
 *
 * 	Input : nums = [5,2,3,1]
 * 	Ouput : [1,2,3,5]
 * 	Explac:
 *
 * 	After sorting array, the positions of some numbers are not changed (for example, 2 and 3), while the positions of other numbers are changed (for example, 1 and 5)
 *
 * 	Input : nums = [5,1,1,2,0,0]
 * 	Ouput : [0,0,1,1,2,5]
 *
 * 	Constraints : 
 *
 * 		1 <= nums.length <= 5 * 10^4
 *
 * 		-5 * 10^4 <= nums[i] <= 5 *10^4
*/

#include<stdio.h>
#include<stdlib.h>

void merge(int *new, int start, int mid, int end){

	int ele1 = mid - start +1;
	int ele2 = end - mid;

	int new1[ele1], new2[ele2];

	for(int i = 0; i < ele1; i++){
		
		new1[i] = new[start + i];
	}

	for(int i = 0; i < ele2; i++){
	
		new2[i] = new[mid + 1 + i];
	}

	int itr1 = 0, itr2 = 0, itr3 = start;

	while(itr1 < ele1 && itr2 < ele2){
		
		if(new1[itr1] < new2[itr2]){
			
			new[itr3] = new1[itr1];
			itr1++;
		}else{
			
			new[itr3] = new2[itr2];
			itr2++;
		}

		itr3++;
	}

	while(itr1 < ele1){
		
		new[itr3] = new1[itr1];
		itr1++;
		itr3++;
	}

	while(itr2 < ele2){
		
		new[itr3] = new2[itr2];
		itr2++;
		itr3++;
	}
}

void mergeSort(int *new, int start, int end){
	
	if(start < end){
		
		int mid  = (start + end) / 2;

		mergeSort(new, start, mid);
		mergeSort(new, mid+1, end);

		merge(new, start, mid, end);
	}
}

int *sortArray(int *arr, int size, int *returnSize, int *new){

//	int *new = (int*)calloc(sizeof(int), size);

	*returnSize = size;

	int start = 0, end = size - 1;

	for(int  i = 0; i < size; i++){
		
		new[i] = arr[i];
	}

	mergeSort(new, start, end);

	return new;
}

void main(){
	
	int arr[] = {5,2,3,1};

	int size = sizeof(arr) / sizeof(arr[0]);

	int *returnSize;
	
	int *new = (int*)calloc(sizeof(int), size);	

	for(int i = 0; i < size; i++){

		printf("%d ", arr[i]);
	}

	printf("\n");

	int *ptr = sortArray(arr, size, returnSize, new);

	for(int i = 0; i < size; i++){
		
		printf("%d ", new[i]);
	}

	printf("\n");
}
