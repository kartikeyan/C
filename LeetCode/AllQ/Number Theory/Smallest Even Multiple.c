
/*	Program 1 : Smallest Even Multiple
 *
 *	Given an "positive" integer n, return the smallest positive integer
 *	that is a ultiple of "both" 2 and "n".
 *
 *	Input : n = 5
 *	Ouput : 10
 *	Explanation : The smallest multiple of both 5 and 2 is 10
 *
 *	Input : n = 6
 *	Ouput : 6
 *	Explanation : The smallest multiple of both 6 and 2 is 6.
 *		      Note that a number is a multiple of itself
*/

#include<stdio.h>

int smallestEvenMultiple(int n){
	
	if(n % 2 == 0){
		
		return n;
	}else{
		
		return n * 2;
	}
}

void main(){
	
	int n = 5;

	int ret = smallestEvenMultiple(n);

	printf("%d\n", ret);
}
