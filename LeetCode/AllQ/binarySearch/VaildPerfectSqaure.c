/*
 * 	PRogram 2 : Vaild Perfect Sqaure
 *
 * 	Given a positive integer num, return true if num is a
 * 	perfect sqaure or false otherwise
 *
 * 	Input : num = 16   , num = 14
 * 	Ouptut : true      , false
*/

#include<stdio.h>
#include<stdbool.h>

bool isPerfectSquare(int num){

	int start = 0, end = num;

	while(start <= end){
	
		long mid = start + (end - start) / 2;

		long square = mid * mid;

		if(square == num){
			
			return true;

		}else if(square < num){
			
			start = mid + 1;

		}else{
			end = mid - 1;
		}
	}

	return false;
}

bool isPerfectSquare(int num){
	
	long int i = 1, square = 0;

	for(i = 1; i <= num; i++){
	
		square = i * i;

		if(square == num){
			
			return true;
		}

		if(square > num){
			
			return false;
		}
	}

	return false;
}

int main(){

	int num;
	printf("Enter Num\n");
	scanf("%d", &num);

	bool ret = isPerfectSquare(num);
	printf("Ahe bhava %d\n", ret);
}
