
/*

137. Single Number II

Given an integer array nums where every element appears three times except for one, which appears exactly once. Find the single element and return it.

You must implement a solution with a linear runtime complexity and use only constant extra space.

Input: nums = [2,2,3,2]
Output: 3

Input: nums = [0,1,0,1,0,1,99]
Output: 99


    1 <= nums.length <= 3 * 104
    -231 <= nums[i] <= 231 - 1
    Each element in nums appears exactly three times except for one element which appears once.

*/

#include<stdio.h>

int singleNumber(int *arr, int size){
	
	for(int i = 0; i < size; i++){
		
		int num = arr[i];
		int count = 0;

		for(int j = 0; j < size; j++){
			
			if(arr[j] == num){
				
				count++;
			}
		}

		if(count == 1){
			
			return num;
		}
	}

	return -1;
}

void main(){
	
	int arr[] = {0,1,0,1,0,99};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = singleNumber(arr, size);

	printf("%d\n", ret);
}
