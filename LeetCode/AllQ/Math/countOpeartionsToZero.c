/*	
 *
 *	Program 2 : LeetCode No : 2169 : 
 *
 *		Q] "Count Operations to obtain Zero"
 *
 *	Given an two non-negative integers num1 and num2.
 *
 *	In one operation, if num1 >= num2, u must subtract num2 from num1,
 *	otherwise subtract num1 from num2.
 *
 *	Return the "number of operations" required to make either
 *	num1 = 0 or num2 = 0.
 *
 *	Input : num1 = 2, num2 = 3
 *	Output : 3
*/

#include<stdio.h>

/*
int countOperations(int num1, int num2){

	int count = 0;

	while(num1 != 0 && num2 != 0){
	
		if(num1 >= num2){
			
			num1 = num1 - num2;
		}else{
			
			num2 = num2 - num1;
		}

		count++;
	}

	return count;
}
*/

int countOperations(int num1, int num2){
	
	if(num1 == 0 || num2 == 0){
		return 0;
	}

	if(num1 == num2){
		
		return 1;
	}

	int count = 0;

	while(1){
	
		if(num1 >= num2){
			
			num1 = num1 - num2;
			count++;
		}else{
			num2 = num2 - num1;
			count++;
		}

		if(num1 == 0 || num2 == 0){
			
			return count;
		}
	}
}


void main(){
	
	int num1, num2;
	printf("Enter Num1 and Num2\n");
	scanf("%d %d", &num1 , &num2);

	int ret = countOperations(num1, num2);
	printf("Number of steps are : %d\n", ret);
}
