
/*	Program 2 : Subtract the Product and Sum of Digits of an integer
 *
 *		    Given an integer number n, return the difference between 
 *		    the product of its digits and the sum of its digits.
 *
 *		    Input : n = 234  ,  	 Input :  = 4421
 *		    Ouput : 15	     ,   	 Output : 21
 *	Explanation:				 Expalna: Prod = 4 * 4 * 2 * 1 = 32
 *		    Product of digits 			  Sum = 4 + 4 + 2 + 1 = 11
 *		    = 2 * 3 * 4 = 24			  Result = 32 - 11
 *		    Sum of digits				 = 21
 *		    = 2 + 3 + 4 = 9
 *
 *		    Result : 24 - 9 = 15
*/

#include<stdio.h>

int subtractProductAndSum(int n){
	
	int sum = 0, prod = 1;

	while(n){
		
		int digit = n % 10;
		sum = sum + digit;
		prod = prod * digit;

		n = n / 10;
	}

	return prod - sum;
}

void main(){
	
	int n;
	printf("Enter N\n");
	scanf("%d", &n);

	int ret = subtractProductAndSum(n);

	printf("Difference is : %d\n", ret);
}
