/*	Program 2 : Power of Three
 *
 *	Given an integer n, return "true" if it is a power of three. Otherwise return "false".
 *	An integer n is a power of two, if there exists an integer x such that n == 3^x.
 *
 *	Input : n == 27			Input : n == 0			Input : n = -1
 *	Output : true			Output : false			Output : false
 *	Explanation : 27 = 3^3 		Explanation : 36x = 0
*/

#include<stdio.h>
#include<stdbool.h>

bool isPowerOfTwo(int n){

	if(n == 1){
		return true;
	}
	if(n < 1){
		return false;
	}

	return isPowerOfTwo(n / 3);
}

/*
bool isPowerOfTwo(int n){
	
	if(n <= 0){
		return false;
	}

	if(n == 1){
		return true;
	}

	if(n % 3 != 0){
		return false;
	}

	return isPowerOfTwo(n / 3);
}
*/
void main(){
	
	int num;
	printf("Enter Num\n");
	scanf("%d", &num);

	bool ret = isPowerOfTwo(num);
	
	printf("%d\n", ret);
}
