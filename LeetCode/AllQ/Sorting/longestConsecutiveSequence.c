
/*


128. Longest Consecutive Sequence

Given an unsorted array of integers nums, return the length of the longest consecutive elements sequence.

You must write an algorithm that runs in O(n) time.

Input: nums = [100,4,200,1,3,2]
Output: 4
Explanation: The longest consecutive elements sequence is [1, 2, 3, 4]. Therefore its length is 4.

Input: nums = [0,3,7,2,5,8,4,6,0,1]
Output: 9

*/

#include<stdio.h>

void merge(int arr[], int start, int mid, int end){

        int ele1 = mid - start + 1;
        int ele2 = end - mid;

        int arr1[ele1], arr2[ele2];

        for(int i = 0; i < ele1; i++){

                arr1[i] = arr[start + i];
        }

        for(int j = 0; j < ele2; j++){

                arr2[j] = arr[mid + 1 + j];
        }

        int itr1 = 0, itr2 = 0, itr3 = start;

        while(itr1 < ele1 && itr2 < ele2){

                if(arr1[itr1] < arr2[itr2]){

                        arr[itr3] = arr1[itr1];
                        itr1++;
                }else{

                        arr[itr3] = arr2[itr2];
                        itr2++;
                }

                itr3++;
        }

        while(itr1 < ele1){

                arr[itr3] = arr1[itr1];
                itr1++;
                itr3++;
        }

        while(itr2 < ele2){

                arr[itr3] = arr2[itr2];
                itr2++;
                itr3++;
        }

}


void mergeSort(int arr[], int start, int end){

        if(start < end){

                int mid = (start + end) / 2;

                mergeSort(arr, start, mid);
                mergeSort(arr, mid+1, end);

                merge(arr, start, mid, end);
        }
}

int max(int ans, int curr){
	
	if(ans > curr){
		
		return ans;
	}else{
		
		return curr;
	}
}

int longest(int *arr, int size){

	if(size == 0){
		
		return 0;
	}

	mergeSort(arr, 0, size - 1);

	int ans = 1;
	int prev = arr[0];
	int curr = 1;

	for(int i = 1; i < size; i++){

		if(arr[i] == prev+1){
			
			curr++;
		}
		else if(arr[i] != prev){
			
			curr = 1;
		}

		prev = arr[i];

		ans = max(ans, curr);
	}

	return ans;
}

void main(){
	
	int arr[] = {11,3,1,2,3,4};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = longest(arr, size);

	printf("%d\n", ret);
}
