
/*	
 	34. Find First and Last Position of Element in Sorted Array

 Given an array of integers nums sorted in non-decreasing order, find the starting and ending position of a given target value.

If target is not found in the array, return [-1, -1].

You must write an algorithm with O(log n) runtime complexity.

Input: nums = [5,7,7,8,8,10], target = 8
Output: [3,4]

Input: nums = [5,7,7,8,8,10], target = 6
Output: [-1,-1]

Input: nums = [], target = 0
Output: [-1,-1]

Constraints:

    0 <= nums.length <= 105
    -109 <= nums[i] <= 109
    nums is a non-decreasing array.
    -109 <= target <= 109
*/

#include<stdio.h>
#include<stdlib.h>

void searchRange(int *arr, int size, int *new, int target){
	
	arr[0] = -1;
	arr[1] = -1;

	int start = 0, end = size - 1, mid, k = 0, ans = -1;

	while(start <= end){
		
		mid = (start + end) / 2;

		if(arr[mid] == target){
			
			ans = mid;
			end = mid - 1;

		}else if(arr[mid] < target){
			
			start = mid + 1;
		}else{
			end = mid - 1;
		}
	}

	new[0] = ans;

	start = 0, end = size - 1;

	while(start <= end){
		
		mid = (start + end) / 2;

		if(arr[mid] == target){
			
			ans = mid;
			start = mid + 1;
		}else if(arr[mid] < target){
			
			start = mid + 1;
		}else{
			end = mid - 1;
		}
	}

	new[1] = ans;

}

void main(){
	
	int arr[] = {5,7,7,8,8,10};

	int size = sizeof(arr) / sizeof(arr[0]);

	int target = 8;

	int *new = (int*)calloc(sizeof(int), 2);

	searchRange(arr, size, new, target);

	for(int i = 0; i < 2; i++){
		
		printf("%d\n", new[i]);
	}
}
