
/*	Prog : Maximum SubArray
 *
 *	Given an integer array "nums", find the subarray with the largest sum and return its sum
 *
 *	Input : nums = [-2,1,-3,4,-1,2,1,-5,4]
 *	Ouput : 6
 *	Expla : The subarray [4,-1,2,1] has the largest sum 6
 *
 *	Input : nums = [1]
 *	Ouput : 1
 *	Expla : The subarray [1] has the largest sum 1
 *
 *	Input : nums = [5,4,-1,7,8]
 *	Ouput : 23
 *	Expla : The subarray [5,4,-1,7,8] has the largest sum 23
*/

#include<stdio.h>

int maxSubArray(int arr[], int size){
	
	int max = arr[0], sum = 0;

	for(int i = 0; i < size; i++){
		
		if(sum > 0){
			
			sum = sum + arr[i];
		}else{
			
			sum = arr[i];
		}

		if(sum > max){
			
			max = sum;
		}
	}

	return max;
}

void main(){
	
	int arr[] = {5,4,-1,7,8};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = maxSubArray(arr, size);

	printf("Max is : %d\n", ret);
}
