
/*
 	2540. Minimum Common Value

	Given two integer arrays nums1 and nums2, sorted in non-decreasing order, return the minimum integer common to both arrays. If there is no common integer amongst nums1 and nums2, return -1.

	Note that an integer is said to be common to nums1 and nums2 if both arrays have at least one occurrence of that integer.

	Input: nums1 = [1,2,3], nums2 = [2,4]
	Output: 2
	Explanation: The smallest element common to both arrays is 2, so we return 2.

	Input: nums1 = [1,2,3,6], nums2 = [2,3,4,5]
	Output: 2
	Explanation: There are two common elements in the array 2 and 3 out of which 2 is the smallest, so 2 is returned.

	Constraints:

	    1 <= nums1.length, nums2.length <= 105
	    1 <= nums1[i], nums2[j] <= 109
	    Both nums1 and nums2 are sorted in non-decreasing order.
*/

#include<stdio.h>
	
int getCommon(int arr1[], int arr2[], int size1, int size2){

	int i = 0, j = 0;

	while(i < size1 && j < size2){
			
		if(arr1[i] == arr2[j]){
				
			return arr1[i];
		}
		else if(arr1[i] > arr2[j]){
			j++;
		}else{
			i++;
		}
	}

	return -1;
}

void main(){
		
	int arr1[] = {1,2,3};

	int size1 = sizeof(arr1) / sizeof(arr1[0]);

	int arr2[] = {2,4};

	int size2 = sizeof(arr2) / sizeof(arr2[0]);

	int ret = getCommon(arr1, arr2, size1, size2);

	printf("%d\n",ret);

}
