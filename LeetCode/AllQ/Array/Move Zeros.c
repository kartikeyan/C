
/*	Program 1 : Move Zeros
 *
 *	Given an integer array "nums", move all 0's to the end of it whilr
 *	maintaining the relative order of the non-zero elements.
 *
 *	Input : nums[] = {0,1,0,3,12};
 *	Ouput : {1,3,12,0,0}
*/

#include<stdio.h>

void move(int arr[], int size){
	
	int k = 0, temp;

	for(int i = 0; i < size; i++){
		
		if(arr[i] != 0){
			
			temp = arr[i];
			arr[i] = arr[k];
			arr[k] = temp;

			k++;
		}
	}
}

void main(){
	
	int arr[] = {0,1,0,3,12};

	int size = sizeof(arr) / sizeof(arr[0]);

	move(arr, size);

	for(int i = 0; i < size; i++){
		
		printf("%d\n", arr[i]);
	}
}
