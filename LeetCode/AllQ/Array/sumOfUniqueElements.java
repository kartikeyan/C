
/*	
 	1748. Sum of Unique Elements

	You are given an integer array nums. The unique elements of an array are the elements that appear exactly once in the array.

	Return the sum of all the unique elements of nums.

	Input: nums = [1,2,3,2]
	Output: 4
	Explanation: The unique elements are [1,3], and the sum is 4.
	Input: nums = [1,1,1,1,1]
	Output: 0
	Explanation: There are no unique elements, and the sum is 0.

	Input: nums = [1,2,3,4,5]
	Output: 15
	Explanation: The unique elements are [1,2,3,4,5], and the sum is 15.

	Constraints:

	    1 <= nums.length <= 100
	    1 <= nums[i] <= 100
*/

class sol{

	static int sumOfUnique(int arr[], int size){
		
		int sum = 0;

		for(int i = 0; i < size; i++){
			
			int count = 0;

			for(int j = 0; j < size; j++){
			
				if(arr[i] == arr[j]){
					
					count++;
				}
			}

			if(count == 1){
				
				sum += arr[i];
			}
		}

		return sum;
	}
	
	public static void main(String[] s){
		
		int arr[] = {1,2,3,2};

		int size = arr.length;

		int ret = sumOfUnique(arr, size);

		System.out.println(ret);
	}
}

