
/*
 	1287. Element Appearing More Than 25% In Sorted Array

	Given an integer array sorted in non-decreasing order, there is exactly one integer in the array that occurs more than 25% of the time, return that integer.

	Input: arr = [1,2,2,6,6,6,6,7,10]
	Output: 6

	Input: arr = [1,1]
	Output: 1
	
	Constraints:

	    1 <= arr.length <= 104
	    0 <= arr[i] <= 105
*/

#include<stdio.h>

int findSpecial(int arr[], int size){
		
	int quater = size / 4;

	for(int i = 0; i < size; i++){
			
		if(arr[i] == arr[i + quater]){
				
			return arr[i];
		}
	}

	return -1;
}
	
void main(){
		
	int arr[] = {1,2,2,6,6,6,6,7,10};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = findSpecial(arr, size);

	printf("%d\n",ret);
	
}
