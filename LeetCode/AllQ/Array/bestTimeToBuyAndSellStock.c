
/*
 	121. Best Time to Buy and Sell Stock

	You are given an array prices where prices[i] is the price of a given stock on the ith day.

	You want to maximize your profit by choosing a single day to buy one stock and choosing a different day in the future to sell that stock.

	Return the maximum profit you can achieve from this transaction. If you cannot achieve any profit, return 0

	Input: prices = [7,1,5,3,6,4]
	Output: 5
	Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
	Note that buying on day 2 and selling on day 1 is not allowed because you must buy before you sell.

	Input: prices = [7,6,4,3,1]
	Output: 0
	Explanation: In this case, no transactions are done and the max profit = 0.

	Constraints:

	    1 <= prices.length <= 105
	    0 <= prices[i] <= 104
*/

#include<stdio.h>

	// Approach 1
/*
int maximum(int maxProfit, int cost){
		
	if(maxProfit > cost){
		return maxProfit;
	}
	return cost;
}

int minimum(int minimum, int cost){
		
	if(minimum > cost){
		return cost;
	}
	return minimum;
}

int Profit(int arr[], int size){
		
	int min = arr[0];
	int maxProfit = 0;

	for(int i = 1; i < size; i++){
			
		int cost = arr[i] - min;

		maxProfit = maximum(maxProfit, cost);

		min = minimum(min, arr[i]);
	}

	return maxProfit;
}
*/	
int Profit(int arr[], int size){
		
	int min = arr[0];
	int maxProfit = 0;

	for(int i = 1; i < size; i++){
			
		if(arr[i] < min){
				
			min = arr[i];
		}
		else if(arr[i] - min > maxProfit){
				
			maxProfit = arr[i] - min;
		}
	}

	return maxProfit;
}

void main(){
		
	int prices[] = {7,1,5,3,6,4};

	int size = sizeof(prices) / sizeof(prices[0]);

	int ret = Profit(prices, size);

	printf("%d\n", ret);
	
}
