
/*	PRogram 2 : Count Number of Pairs With Absolute difference k.
 *
 *	Given an integer array "nums" and an integer "k", return the number of pairs "(i,j)"
 *	where i < j such that "nums[i] - nums[j] == k".
 *
 *	The value of |x| is defined such as,
 *		x if x >= 0
 *		-x if x < 0
 *
 *	Input : nums[] = {1,2,2,1}, k = 1
 *	Ouput : 4
*/

#include<stdio.h>

int countKDiff(int arr[], int size, int k){
	
	int count = 0;

	for(int i = 0; i < size; i++){
		
		for(int j = i + 1; j < size; j++){
			
			if(arr[i] - arr[j] == k || arr[j] - arr[i] == k){
				
				count++;
			}
		}
	}

	return count;
}

void main(){
	
	int arr[] = {1,2,2,1};

	int size = sizeof(arr) / sizeof(arr[0]);
	
	int k;
	printf("Enter K\n");
	scanf("%d", &k);

	int ret = countKDiff(arr, size, k);
	printf("Count is : %d\n", ret);
}
