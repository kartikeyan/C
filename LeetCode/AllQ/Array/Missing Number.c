
/*	Prog : Missing Number
 *
 *	Given an array "nums" containing "n" ditinct numbers in the range[0,n], return the only number in the range that is missing from the array
 *
 *	Input : nums = [3,0,1] 
 *	Ouput : 2
 *	Expla : n = 3 since there are 3 numbers, so all numbers are in the range[0,3], 2 is missing number in the range since it does not appear in nums
 *
 *	Input : nums = [0,1]
 *	Ouput : 2
 *	Expla : n = 2 since there are 2 numbers, so all numbers are in the range[0,2], 2 is missing number in the range since it does not appear in nums.
 *
 *	Input : nums = [9,6,4,2,3,5,7,0,1]
 *	Ouput : 8
 *	Expla : n = 9 since there are 9 numbers, so all numbers are in the range[0,9], 8 is the missing number in the range since it does not appear in nums
 *
 *	Constraints :
 *
 *		1 <= n <= 10^4
 *
 *		0 <= nums[i] <= n
*/

#include<stdio.h>

void merge(int arr[], int start, int mid, int end){
	
	int ele1 = mid - start + 1;
	int ele2 = end - mid;

	int arr1[ele1], arr2[ele2];

	for(int i = 0; i < ele1; i++){
		
		arr1[i] = arr[start + i];
	}

	for(int j = 0; j < ele2; j++){
		
		arr2[j] = arr[mid + 1 + j];
	}

	int itr1 = 0, itr2 = 0, itr3 = start;

	while(itr1 < ele1 && itr2 < ele2){
		
		if(arr1[itr1] < arr2[itr2]){
			
			arr[itr3] = arr1[itr1];
			itr1++;
		}else{
			
			arr[itr3] = arr2[itr2];
			itr2++;
		}

		itr3++;
	}

	while(itr1 < ele1){
		
		arr[itr3] = arr1[itr1];
		itr1++;
		itr3++;
	}

	while(itr2 < ele2){
		
		arr[itr3] = arr2[itr2];
		itr2++;
		itr3++;
	}
}

void mergeSort(int arr[], int start, int end){
	
	if(start < end){
		
		int mid = (start + end) / 2;

		mergeSort(arr, start, mid);
		mergeSort(arr, mid+1,end);

		merge(arr,start,mid,end);
	}
}

int missingNumber(int arr[], int size){
	
	mergeSort(arr, 0, size - 1);

	for(int i = 0; i < size; i++){
		
		if(i != arr[i]){
			
			return i;
		}
	}
	return size;
}

void main(){
	
	int arr[] = {9,6,4,2,3,5,7,0,1};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = missingNumber(arr, size);

	printf("%d\n", ret);
}
