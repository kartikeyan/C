
/*
 	1446. Consecutive Characters

	The power of the string is the maximum length of a non-empty substring that contains only one unique character.

	Given a string s, return the power of s.

	Input: s = "leetcode"
	Output: 2
	Explanation: The substring "ee" is of length 2 with the character 'e' only.

	Input: s = "abbcccddddeeeeedcba"
	Output: 5
	Explanation: The substring "eeeee" is of length 5 with the character 'e' only.

	Constraints:

	    1 <= s.length <= 500
	    s consists of only lowercase English letters.
*/

class Sol{

	static int maxPower(String s){
		
		int c1 = 1, rem = 1;

		for(int i = 0; i < s.charAt(i) - 1; i++){
			
			if(s.charAt(i) == s.charAt(i+1)){
				
				c1++;
			}else{
				
				rem = Math.max(rem, c1);
				c1 = 1;
			}
		}

		rem = Math.max(rem, c1);

		return rem;
	}
	
	public static void main(String[] args){
		
		String str = "abbcccddddeeeeedcba";

		int ret = maxPower(str);

		System.out.println("Max is " + ret);
	}
}

