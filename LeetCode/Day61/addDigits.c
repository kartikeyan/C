
/*
 	258. Add Digits

	Given an integer num, repeatedly add all its digits until the result has only one digit, and return it.

	Input: num = 38
	Output: 2
	Explanation: The process is
		38 --> 3 + 8 --> 11
		11 --> 1 + 1 --> 2 
		Since 2 has only one digit, return it.

	Input: num = 0
	Output: 0

	Constraints:

	    0 <= num <= 231 - 1
*/

#include<stdio.h>

int addDigits(int n){

	// Approach 1
/*
	if(n < 10){
			
		return n;
	}

	int sum = 0;

	while(n != 0){
			
		sum = sum + n % 10;
		n = n / 10;
	}

	return addDigits(sum);
*/
	
	// Approach 2

/*	
	int sum = 0;

	if((n / 10) == 0){
			
		return n;
	}else{
			
		while(n != 0){
				
			sum = sum + (n % 10);
			n = n / 10;
		}
		return addDigits(sum);
	}

*/	
	// Approach 3
	return n - 9 * ((n-1)/9);
}
		
void main(){
		
	int n = 38;

	int ret = addDigits(n);

	printf("%d\n",ret);
	
}
