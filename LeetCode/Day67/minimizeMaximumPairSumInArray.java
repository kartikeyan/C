
/*
 	1877. Minimize Maximum Pair Sum in Array

	The pair sum of a pair (a,b) is equal to a + b. The maximum pair sum is the largest pair sum in a list of pairs.

    	For example, if we have pairs (1,5), (2,3), and (4,4), the maximum pair sum would be max(1+5, 2+3, 4+4) = max(6, 5, 8) = 8.

	Given an array nums of even length n, pair up the elements of nums into n / 2 pairs such that:

    	Each element of nums is in exactly one pair, and
	The maximum pair sum is minimized.

	Return the minimized maximum pair sum after optimally pairing up the elements.

	Input: nums = [3,5,2,3]
	Output: 7
	Explanation: The elements can be paired up into pairs (3,3) and (5,2).
		     The maximum pair sum is max(3+3, 5+2) = max(6, 7) = 7.

	Input: nums = [3,5,4,2,4,6]
	Output: 8
	Explanation: The elements can be paired up into pairs (3,5), (4,4), and (6,2).
	The maximum pair sum is max(3+5, 4+4, 6+2) = max(8, 8, 8) = 8.


	Constraints:

	    n == nums.length
	    2 <= n <= 105
	    n is even.
	    1 <= nums[i] <= 105
*/

class Sol{

	static void merge(int nums[], int start, int mid, int end){

		int ele1 = mid - start + 1, ele2 = end - mid;

    	//	int nums1[ele1], nums2[ele2];
		int nums1[] = new int[ele1];
		int nums2[] = new int[ele2];

    		for(int i = 0; i < ele1; i++){
        		nums1[i] = nums[start + i];
	    	}

    		for(int j = 0; j < ele2; j++){
        		nums2[j] = nums[mid + 1 + j];
    		}

    		int itr1 = 0, itr2 = 0, itr3 = start;

    		while(itr1 < ele1 && itr2 < ele2){

        		if(nums1[itr1] < nums2[itr2]){
            			nums[itr3] = nums1[itr1];
            			itr1++;
        		}else{
            			nums[itr3] = nums2[itr2];
            			itr2++;
        		}

        		itr3++;
    		}

    		while(itr1 < ele1){

        		nums[itr3] = nums1[itr1];
        		itr1++;
        		itr3++;
    		}

    		while(itr2 < ele2){
        		nums[itr3] = nums2[itr2];
        		itr2++;
        		itr3++;
    		}
	}

	static void mergeSort(int nums[], int start, int end){
		
		if(start < end){
			
			int mid = start + (end - start) / 2;

			mergeSort(nums, start, mid);
			mergeSort(nums, mid+1, end);

			merge(nums, start, mid, end);
		}
	}

	static int minPairSum(int arr[], int size){
		
		mergeSort(arr, 0, size-1);

		int start = 0, end = size - 1;

		int pair = 0, max = 0;

		while(start < end){
			
			pair = arr[start] + arr[end];

			if(pair > max){
				
				max = pair;
			}
		}

		return max;
	}
	
	public static void main(String[] s){
		
		int arr[] = {3,5,4,2,4,6};

		int size = arr.length;

		int ret = minPairSum(arr, size);

		System.out.println(ret);
	}
}
