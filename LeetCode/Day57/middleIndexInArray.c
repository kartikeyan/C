
/*	
 	1991. Find the Middle Index in Array

	Given a 0-indexed integer array nums, find the leftmost middleIndex (i.e., the smallest amongst all the possible ones).

	A middleIndex is an index where nums[0] + nums[1] + ... + nums[middleIndex-1] == nums[middleIndex+1] + nums[middleIndex+2] + ... + nums[nums.length-1].

	If middleIndex == 0, the left side sum is considered to be 0. Similarly, if middleIndex == nums.length - 1, the right side sum is considered to be 0.

	Return the leftmost middleIndex that satisfies the condition, or -1 if there is no such index.

	Input: nums = [2,3,-1,8,4]
	Output: 3
	Explanation: The sum of the numbers before index 3 is: 2 + 3 + -1 = 4
	The sum of the numbers after index 3 is: 4 = 4

	Input: nums = [1,-1,4]
	Output: 2
	Explanation: The sum of the numbers before index 2 is: 1 + -1 = 0
	The sum of the numbers after index 2 is: 0

	Input: nums = [2,5]
	Output: -1
	Explanation: There is no valid middleIndex.

	Constraints:

	    1 <= nums.length <= 100
	    -1000 <= nums[i] <= 1000
*/

#include<stdio.h>

int pivotIndex(int arr[], int size){
		
	int allSum = 0, temp = 0;

	for(int i = 0; i < size; i++){
			
		allSum = allSum + arr[i];
	}

	for(int j = 0; j < size; j++){
			
		allSum = allSum - arr[j];

		if(allSum == temp){
				
			return j;
		}

		temp = temp + arr[j];
	}

	return -1;
}

/*
int pivotIndex(int arr[], int size){
		
	int allSum = 0, lSum = 0;

	for(int i = 0; i < size; i++){
			
		allSum = allSum + arr[i];
	}

	int rSum = allSum;

	for(int j = 0; j < size; j++){
			
		rSum = rSum - arr[j];

		if(lSum == rSum){
				
			return j;
		}

		lSum = lSum + arr[j];
	}

	return -1;
}
*/
/*	
int pivotIndex(int arr[], int size){
		
	int total = 0, left = 0;

	for(int i = 0; i < size; i++){
			
		total = total + arr[i];
	}

	for(int j = 0; j < size; j++){

		if(left == total - left - arr[j]){
				
			return j;
		}

		left = left + arr[j];
	}

	return -1;
}
*/
/*
int pivotIndex(int arr[], int size){
		
	int lSum[size];
	int rSum[size];

	lSum[0] = 0;
	rSum[size-1] = 0;

	int j = size-2;

	for(int i = 1; i < size; i++){
			
		lSum[i] = lSum[i-1] + arr[i-1];
		rSum[j] = rSum[j+1] + arr[j+1];

		--j;
	}

	for(int i = 0; i < size; i++){

		if(lSum[i] == rSum[i]){
			
			return i;
		}
	}

	return -1;
}
*/
void main(){
		
	int arr[] = {2,3,-1,8,4};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = pivotIndex(arr, size);

	printf("%d\n", ret);
}

