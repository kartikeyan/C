
/*	Program 1 : Remove Duplicates from Sorted Array
 *
 *	Given an Integer array nums sorted in "non-decreasing order",remove tge duplicates "in-place"
 *	such that each unique element appears only "once".The "relative order" of the elements should 
 *	be kept "same".
 *
 *	Input : nums = [1,1,2]
 *	Ouput : 2,  nums = [1,2,.....]
 *
 *	Input : nums = [0,0,1,1,1,2,2,3,3,4]
 *	Ouput : 5,  nums = [0,1,2,3,4,......]
*/

//	We are using Two Pointer Approach

#include<stdio.h>

int removeDup(int arr[], int size){
	
	int i = 0;

	for(int j = 1; j < size; j++){
		
		if(arr[i] != arr[j]){
				
			i++;
			arr[i] = arr[j];
		}
	}

	return i + 1;
}

void main(){

	int arr[] = {1,1,2};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = removeDup(arr, size);

	printf("%d\n", ret);
}
