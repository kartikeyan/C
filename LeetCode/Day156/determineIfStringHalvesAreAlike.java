
/*
 	1704. Determine if String Halves Are Alike

	You are given a string s of even length. Split this string into two halves of equal lengths, and let a be the first half and b be the second half.

	Two strings are alike if they have the same number of vowels ('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'). Notice that s contains uppercase and lowercase letters.

	Return true if a and b are alike. Otherwise, return false.

	Example 1:

	Input: s = "book"
	Output: true
	Explanation: a = "bo" and b = "ok". a has 1 vowel and b has 1 vowel. Therefore, they are alike.

	Example 2:

	Input: s = "textbook"
	Output: false
	Explanation: a = "text" and b = "book". a has 1 vowel whereas b has 2. Therefore, they are not alike.
	Notice that the vowel o is counted twice.


	Constraints:

	    2 <= s.length <= 1000
	    s.length is even.
	    s consists of uppercase and lowercase letters.
*/

class Sol{

	static boolean halvesAreAlike(String s){
		
		char arr[] = s.toCharArray();

		int c1 = 0, c2 = 0;

		for(int i = 0; i < arr.length / 2; i++){
			
			if(arr[i] == 'a' || arr[i] == 'e' || arr[i] == 'i' || arr[i] == 'o' || arr[i] == 'u' || arr[i] == 'A' || arr[i] == 'E' || arr[i] == 'I' || arr[i] == 'O' || arr[i] == 'U'){
				
				c1++;
			}
		}

		for(int i = arr.length / 2; i < arr.length; i++){
			
			if(arr[i] == 'a' || arr[i] == 'e' || arr[i] == 'i' || arr[i] == 'o' || arr[i] == 'u' || arr[i] == 'A' || arr[i] == 'E' || arr[i] == 'I' || arr[i] == 'O' || arr[i] == 'U'){
				
				c2++;
			}
		}
	
		if(c1 == c2){
			
			return true;
		}else
			return false;
	}
	
	public static void main(String[] args){

		String s = "book";

		boolean ret = halvesAreAlike(s);

		if(ret == true){
			
			System.out.println("Strings are Alike");
		}else
			System.out.println("Not Alike");
	}
}
