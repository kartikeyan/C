
/*
 	1832. Check if the Sentence Is Pangram

	A pangram is a sentence where every letter of the English alphabet appears at least once.

	Given a string sentence containing only lowercase English letters, return true if sentence is a pangram, or false otherwise.

	Input: sentence = "thequickbrownfoxjumpsoverthelazydog"
	Output: true
	Explanation: sentence contains at least one of every letter of the English alphabet.

	Input: sentence = "leetcode"
	Output: false

	Constraints:

	    1 <= sentence.length <= 1000
	    sentence consists of lowercase English letters.
*/

class Sol{

	static boolean checkIfPangram(String sentence){
		
		int count[] = new int[26];

		for(int i = 0; i < sentence.length(); i++){
			
			count[sentence.charAt(i) - 'a']++;
		}

		for(int i = 0; i < 26; i++){
			
			if(count[i] == 0){
				
				return false;
			}
		}

		return true;
	}
	
	public static void main(String[] s){
		
		String sentence = "thequickbrownfoxjumpsoverthelazydog";

		boolean ret = checkIfPangram(sentence);

		if(ret == true){
			
			System.out.println("Sentence is Pangram");
		}else{
			System.out.println("Sentence is not Pangram");
		}
	}
}
