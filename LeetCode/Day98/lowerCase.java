
class Sol{

	static String toLowerCase(String str){
		
		char arr[] = str.toCharArray();

		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] < 97  && (arr[i] >= 65 && arr[i] <= 90)){
				
				arr[i] += 32;
			}
		}

		return new String(arr);
	}
	
	public static void main(String[] args){
		
		String str = "KARTIK";

		String ret = toLowerCase(str);

		System.out.println("Returned String is " + ret);
	}
}
