/*	Program 4 : WAP to concate 2 Linked Lists
 *
 *
 *
*/

#include<stdio.h>
#include<stdlib.h>

struct Node{

	int data;
	struct Node *next;
};

struct Node *head1 = NULL;
struct Node *head2 = NULL;

struct Node *createNode(){

	struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(struct Node **head){

	struct Node *newNode = createNode();

	if(*head == NULL){

		*head = newNode;
	}else{
	
		struct Node *temp = *head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

int printSLL(struct Node *head){

	if(head == NULL){
		printf("Invalid\n");
		return -1;
	}else{
	
		struct Node *temp = head;

		while(temp->next != NULL){
		
			printf("|%d|->", temp->data);

			temp = temp->next;
		}

		printf("|%d|\n", temp->data);
	}
}

void concateLL(){

	struct Node *temp = head1;		//	temp1 la head1 chya last node parayanr jeun challoy, temp cha last node la head2 attach kartoy

	while(temp->next != NULL){
	
		temp = temp->next;
	}

	temp->next = head2;
}

void main(){

	int nodeCount;
	printf("Enter Node Count : LinkedList1\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode(&head1);
	}

	printf("Enter Node Count : LinkedList2\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode(&head2);
	}

	printSLL(head1);

	printSLL(head2);

	concateLL();

	printSLL(head1);

}


	
