/*	Program - 
 *	
 *	Concate Last N Nodes 
*/

// 	Doubly Linked List

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	
	struct Node *prev;
	int data;
	struct Node *next;

}node;

node *head1 = NULL;
node *head2 = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	newNode->prev = NULL;

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(node **head){

	node *newNode = createNode();

	if(*head == NULL){
	
		*head = newNode;
	}else{
	
		node *temp = *head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
		newNode->prev = temp;
	}
}

int countNodes(){

	node *temp2 = head2;

	int count = 0;

	while(temp2 != NULL){
	
		count++;
		temp2 = temp2->next;
	}

	return count;
}

void concate(){

	node *temp1 = head1;

	while(temp1->next != NULL){
	
		temp1 = temp1->next;
	}

	temp1->next = head2;
	head2->prev = temp1;
}

void concateLastNLL(int num){
	
	int count = countNodes();

	node *temp = head1;

	while(temp->next != NULL){
	
		temp = temp->next;
	}

	node *temp2 = head2;

	int val = countNodes() - num;

	while(val){
	
		temp2 = temp2->next;
		val--;
	}

	temp->next = temp2;
	head2->prev = temp;
}

int printLL(node *head){

	if(head == NULL){

		printf("LL is empty\n");
		return -1;
	}
	
	else{
		node *temp = head;

		while(temp->next != NULL){

			printf("|%d|->", temp->data);

			temp = temp->next;
		}
	
		printf("|%d|\n", temp->data);
	}
	return 0;
}

void main(){

	int nodeCount;
	printf("Enter Node Count : linkedList1\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode(&head1);
	}

	printf("Enter Node Count : linkedList2\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode(&head2);
	}

	printLL(head1);

	printLL(head2);

/*	concate();

	printLL(head1);
*/
	int num;
	printf("Enter No. of nodes to connect\n");
	scanf("%d", &num);

	concateLastNLL(num);

	printLL(head1);
}
