/*
 *	Program 
 *	
 *	In-Place Reverse
*/

// 	Doubly Circular Linked List

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	
	struct Node *prev;
	int data;
	struct Node *next;

}node;

node *head = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	newNode->prev = NULL;

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	node *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
		head->prev = head;
		head->next = head;
	}else{
	
		node *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
		newNode->prev = temp;
		newNode->next = head;
		head->prev = newNode;
	}
}

node *inPlace(){
	
	if(head == NULL){
	
		printf("LL is empty\n");
		return NULL;
	}else{
		
		if(head->next == NULL){
		
			printf("Only 1 Node is present\n");
			return head;
		}else{
	
			node *temp1 = NULL;	
			node *temp2 = NULL;

			while(temp2 != NULL){
		
				
			}

			head->next = temp1;
			head = temp1;

			return head;
		}
	}
}

int printLL(){

	if(head == NULL){

		printf("LL is empty\n");
		return -1;
	}
	
	else{
		node *temp = head;

		while(temp->next != NULL){

			printf("|%d|->", temp->data);

			temp = temp->next;
		}
	
		printf("|%d|\n", temp->data);
	}
	return 0;
}

void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	printLL();

	node *newNode = inPlace();

	printLL();
}
