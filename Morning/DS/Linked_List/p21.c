/*
 *	Find Out if Singly LinKed List is Palindrome
*/

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct Node{

	int data;
	struct Node *next;
}node;

struct Node *createNode(node *head){

	struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

node *addNode(node *head){

	struct Node *newNode = createNode(head);

	if(head == NULL){

		head = newNode;
	}else{
	
		struct Node *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}

	return head;
}

int countNodes(node *head){

	int count = 0;

	struct Node *temp = head;

	while(temp != NULL){
	
		count++;
		temp = temp->next;
	}

	return count;
}

node* midNode(node *head){

	node *fast = head->next;
	node *slow = head;

	while(fast != NULL && fast->next != NULL){
	
		fast = fast->next->next;
		slow = slow->next;
	}

	return slow;
}

node *reverse(node *head){

	if(head == NULL){
		return NULL;
	}else{ 

		if(head->next == NULL){
		
			return NULL;
		}else{

			node *temp1 = NULL;
			node *temp2 = NULL;

			while(head != NULL){
		
				temp2 = head->next;
				head->next = temp1;
				temp1 = head;
				head = temp2;
			}

			head = temp1;

			return head;
		}
	}
}

bool isPalindrome(node *head){

	if(head == NULL){
		
		return false;
	}else{
		
		if(head->next == NULL){
			
			return true;
		}else{
			
			// step1 = find MidNode
			node *middle = midNode(head);
			 
			//step2 = reverse list after MidNode	
			node *temp = middle->next;
			middle->next = reverse(temp);

			 //step3 = Compare Both halfes
			 node *temp1 = head;
			 node *temp2 = middle->next;

			 while(temp2 != NULL){
		 		
				 if(temp1->data != temp2->data){
			 
			 		return false;
				 }

				 temp1 = temp1->next;
				 temp2 = temp2->next;
			 }

			 //step4 = repeat step2 mhnje pahila original LL yeil

			temp = middle->next;
			middle->next = reverse(temp);

			return true;
		}
	}
}

int printLL(node *head){

	if(head == NULL){
		printf("Invalid\n");
		return -1;
	}else{
	
		struct Node *temp = head;

		while(temp->next != NULL){
		
			printf("|%d|->", temp->data);

			temp = temp->next;
		}

		printf("|%d|\n", temp->data);
	}
}

void main(){
	
	struct Node *head = NULL;

	char choice;

	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.isPalindrome\n");
		printf("4.reverse\n");
		printf("5.MidNode\n");

		int ch;
		printf("Enter CHoice\n");
		scanf("%d", &ch);

		switch(ch){
		
			case 1:
				head = addNode(head);
				break;
			case 2:
				printLL(head);
				break;
			case 3:
				{
					bool val = isPalindrome(head);

					if(val == false){
				
						printf("LL is not Palindrome\n");
					}else{
				
						printf("LL is Palindrome\n");
					}
				}
				break;
			case 4:
				{
				node *newNode = reverse(head);
				}
				break;

			case 5:
				{
				node *newNode = midNode(head);

				printf("%d\n", newNode->data);
				}
				break;
			default:
				printf("Do u want to continue\n");
		}

		getchar();
		printf("\nDo u want to continue\n");
		scanf("%c", &choice);
	
	}while(choice == 'Y' || choice == 'y');
}


	
