/*	PRogram : Merge 2 sorted Linked List
*/

Node *solve(){
	
	// if first list have only 1 node, point its next to entire second list
	if(head1->next == NULL){
		
		head1->next = head2;

		return head1;
	}

	node *curr1 = head1;
	node *next1 = curr1->next;
	node *curr2 = head2;
	node *next1 = curr2->next;

	while(next1 != NULL && curr2 != NULL){
	
		if(curr2->val >= curr1->val && (curr2->val <= next1->val)){
		
			//add Node in between the first Node
			curr1->next = curr2;
			next2 = curr2->next;
			curr2->next = next1;

			//update Pointers
			curr1 = curr2;
			curr2 = next2;

		}else{
		
			//curr1 and next1 ko aage badhana heh
			curr1 = next1;
			next1 = next1->next;

			if(next1 == NULL){
			
				curr1->next = curr2;
				return head1;
			}
		}	
	}

	return head1;
}

struct Node *mergeTwoLists(){

	if(head1 == NULL){
	
		return head2;
	}
	if(head2 == NULL){
	
		return head1;
	}

	if(head1->val <= head2->val){
	
		return solve(head1, head2);
	}else{
	
		return solve(head2, head1);
	}
}
