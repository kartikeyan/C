/*	Program 5 : WAP to concate Last N nodes of second Linked List
 *
 *	Refere p1 - p5 codes for this code
 *
 *	Singly LinKed List
 *
*/

#include<stdio.h>
#include<stdlib.h>

struct Node{

	int data;
	struct Node *next;
};

struct Node *head1 = NULL;
struct Node *head2 = NULL;

struct Node *createNode(){

	struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(struct Node **head){

	struct Node *newNode = createNode();

	if(*head == NULL){

		*head = newNode;
	}else{
	
		struct Node *temp = *head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

int printSLL(struct Node *head){

	if(head == NULL){
		printf("Invalid\n");
		return -1;
	}else{
	
		struct Node *temp = head;

		while(temp->next != NULL){
		
			printf("|%d|->", temp->data);

			temp = temp->next;
		}

		printf("|%d|\n", temp->data);
	}
}

void concateLL(){

	struct Node *temp = head1;

	while(temp->next != NULL){
	
		temp = temp->next;
	}

	temp->next = head2;
}

int countNodes(){

	struct Node *temp = head2;
	
	int count = 0;

	while(temp != NULL){
	
		count++;
		temp = temp->next;
	}

	return count;
}

void concateLastNLL(int num){
	
	int count = countNodes();

	if(num == 0){
		
		concateLL();
	}else{
	
		if(num <= 0 || num > count){
		
			printf("Invalid\n");
		} 
	}

	struct Node *temp = head1;

	while(temp->next != NULL){
		
		temp = temp->next;
	}

//	int val = countNodes() - num;

	struct Node *temp2 = head2;

	while(temp2->next != NULL){
	
		temp2 = temp2->next;
	}

	int val = countNodes() - num;

	while(val){
	
		temp2 = temp2->next;
		val--;
	}

//	free(temp2->next);
//	temp2->next = NULL;

	temp->next = temp2;
}

void main(){

	int nodeCount;
	printf("Enter Node Count : LinkedList1\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode(&head1);
	}

	printf("Enter Node Count : LinkedList2\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode(&head2);
	}

	printSLL(head1);

	printSLL(head2);

	int num;
	printf("Enter No. of nodes to connect\n");
	scanf("%d", &num);

	concateLastNLL(num);

	printSLL(head1);
}


	
