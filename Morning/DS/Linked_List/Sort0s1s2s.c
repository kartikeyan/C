
node *sortList(node *head){

	int zero = 0;
	int one = 0;
	int two = 0;
	
	node *temp = head;

	while(temp != NULL){
	
		if(temp->data == 0){
			
			zero++;
		}else if(temp->data == 1){
		
			one++;
		}else if (temp->data == 2){
			two++;
		}

		temp = temp->next;
	}

	temp = head;

	while(temp != NULL){
		
		if(zero != 0){
			
			temp->data = 0;
			zero--;
		}else if(one != 0){
		
			temp->data = 1;
			one--;
		}else if(two != 0){
		
			temp->data = 2;
			two--;
		}

		temp = temp->next;
	}

	return head;
}

//	Approach - 2
//
//	If data replacement is not allowed
//
//	That is only changing links

void insertAtTail(node *tail, node *curr){

	tail->next = curr;
	tail = curr;
}

node *sortList(node *head){

	node *zerohead = newNode(-1);
		
	node *zerotail = zeroHead;

	node *onehead = newNode(-1);

	node *onetail = onehead;

	node *twohead = newNode(-1);

	node *twotail = twohead;

	node *curr = head;

	//create separate list 0s, 1s, 2s
	while(curr != NULL){
	
		int val = curr->data;

		if(val == 0){
		
			insertAtTail(zerotail, curr);
		}else if(val == 1){
		
			insertAtTail(onetail, curr);
		}else if(val == 2){
		
			insertAtTail(twotail, curr);
		}
	}


	//merge 3 lists
	
	// 1 s list is not empty
	if(onehead->next != NULL){
	
		zerotail->next = onehead->next;
	}else{
	
		// 1 s list is empty
		zerotail->next = twohead->next;
	}	

	onetail->next = twohead->next;
	twotail->next = NULL;

	// setup head
	head = zerohead->next;

	//delete dummy nodes
	delete zerohead;
	delete onehead;
	delete twohead;

	return head;
	
}

