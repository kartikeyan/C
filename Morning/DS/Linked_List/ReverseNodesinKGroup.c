
struct Node* reversekGroup(struct Node* head, int k){

	//base case
	if(head == NULL){
		
		return NULL;
	}

	//step 1 : Reverse first K node
	node *next = NULL;
	node *curr = head;
	node *prev = NULL;

	int count = 0;

	while(curr != NULL && count < k){
	
		next = curr->next;
		curr->next = prev;
		prev = curr;
		curr = next;

		count++;
	}

	//step 2 : recursion
	if(next != NULL){
	
		head->next = reversekGroup(next, k);
	}

	//step 3
	return prev;
}

/*	Time Comple : O(n)
 *	Space Comp : O(n)
*/
