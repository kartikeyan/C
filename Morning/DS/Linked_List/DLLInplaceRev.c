/*
 *	Program 2 
 *
 * 	Refer program - 6 for this
 *	
 *	In-Place Reverse - "Approach 1"
*/

// 	Doubly Linked List

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	
	struct Node *prev;
	int data;
	struct Node *next;

}node;

node *head = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	newNode->prev = NULL;

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	node *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
	}else{
	
		node *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
		newNode->prev = temp;
	}
}

node *inPlace(){
	
	if(head == NULL){
	
		printf("LL is empty\n");
		return NULL;
	}else{
		
		if(head->next == NULL){
		
			printf("Only 1 Node is present\n");
			return head;
		}else{
	
			node *temp = NULL;	

			while(head != NULL){
		
				temp = head->prev;
				head->prev = head->next;
				head->next = temp;
				head = head->prev;
			}

			head = temp->prev;

			return head;
		}
	}
}

int printLL(){

	if(head == NULL){

		printf("LL is empty\n");
		return -1;
	}
	
	else{
		node *temp = head;

		while(temp->next != NULL){

			printf("|%d|->", temp->data);

			temp = temp->next;
		}
	
		printf("|%d|\n", temp->data);
	}
	return 0;
}

void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	printLL();

	node *newNode = inPlace();

	printLL();
}
