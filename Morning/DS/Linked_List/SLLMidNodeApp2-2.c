/*
 *	Find Middle Node of Singly LinKed List 
 *
 *	Approach - "2"
 *
*/

#include<stdio.h>
#include<stdlib.h>

struct Node{

	int data;
	struct Node *next;
};

struct Node *head = NULL;

struct Node *createNode(){

	struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	struct Node *newNode = createNode();

	if(head == NULL){

		head = newNode;
	}else{
	
		struct Node *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

int midNode(){

	if(head == NULL){
		
		printf("LL is empty\n");
	}else{
	
		if(head->next == NULL){
		
			return head->data;
		}else{

			struct Node *slowptr = head;
			struct Node *fastptr = head->next;

			while(fastptr != NULL && fastptr->next != NULL){
			
				fastptr = fastptr->next->next;
				slowptr = slowptr->next;
			}
			
//			return temp;
			return slowptr->data;
		}
	}
}

int printLL(){

	if(head == NULL){
		printf("Invalid\n");
		return -1;
	}else{
	
		struct Node *temp = head;

		while(temp->next != NULL){
		
			printf("|%d|->", temp->data);

			temp = temp->next;
		}

		printf("|%d|\n", temp->data);
	}
}

void main(){

	char choice;

	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.midNode\n");

		int ch;
		printf("Enter CHoice\n");
		scanf("%d", &ch);

		switch(ch){
		
			case 1:
				addNode();
				break;
			case 2:
				printLL();
				break;
			case 3:
				{
				int val = midNode();

				printf("Mid Node is : %d\n", val);
				}
				break;
			default:
				printf("Do u want to continue\n");
		}

		getchar();
		printf("\nDo u want to continue\n");
		scanf("%c", &choice);
	
	}while(choice == 'Y' || choice == 'y');
}

/*	Odd no. of nodes asel tar correct middle node bhetto,
 *	pan even asel tar for eg- 6 nodes ahet, middle node 
 *	asto 3 kiva 4 problem statement madhe kalto ki apla
 *	mid node "middle Node" ahe ka "((middle Node) + 1)"
 *
 * 	Ya code madhe apan consider kelay 3 node as a middle node
*/ 	


	
