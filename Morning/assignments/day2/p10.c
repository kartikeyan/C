
/*	Prog 10 : Row to Cloumn 0
 *
 *	U are given a 2 D integer matrix A, make all the elements in a row or
 *	column zero if the A[i][j] = 0.
 *
 *	Specifically, make the entire i^th row and the j^th column zero.
 *
 *	Input : 1 2 3 4
 *		5 6 7 8
 *		9 2 0 4
 *
 *	Ouput : 1 2 0 0 
 *		0 0 0 0 
 *		0 0 0 0
 *
 *	Explanation : A[2][4] = A[3][3] = 0, so make 2 ^nd row, 3 ^rd rod, 3 ^rd cloumn and 4 ^th column zero
*/

#include<stdio.h>

int n,m;

void setZero(int arr[][m]){
	
	for(int i = 0; i < n; i++){
		
		for(int j = 0; j < m; j++){
			
			if(arr[i][j] == 0){
				
				for(int k = 0; k < n; k++){
					
					if(arr[k][j] != 0){	
						arr[k][j] = -1;
					}
				}

				for(int k = 0; k <= m; k++){
					
					if(arr[i][k] != 0){
						
						arr[i][k] = -1;
					}
				}
			}
		}
	}

	for(int i = 0; i < n; i++){
		
		for(int j = 0; j < m; j++){
			
			if(arr[i][j] == -1){
				
				arr[i][j] = 0;
			}
		}
	}

	for(int i = 0; i < n; i++){
		
		for(int j = 0; j < m; j++){
			
			printf("  %d   ", arr[i][j]);
		}

		printf("\n");
	}
}

void main(){
	
	printf("Enter size of row in 2-D Array : ");

	scanf("%d", &n);

	printf("Enter size of column in 2-D Array : ");

	scanf("%d", &m);

	int arr[n][m];

	for(int i = 0; i < n; i++){

		for(int j = 0; j < m; j++){
		
			scanf("%d", &arr[i][j]);
		}
	}
	
	setZero(arr);
}
