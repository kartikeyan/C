
/*	Program 3 : Main Diagonal Sum
 *
*/

#include<stdio.h>

int rows,columns;

int sum(int arr[rows][columns], int rows, int columns){
	
	int sum = 0;

	for(int i = 0; i < rows; i++){

		for(int j = 0; j < columns; j++){
			
			if(i == j){
				sum = sum + arr[i][j];
			}
		}
	}

	return sum;
}
void main(){
	printf("Enter Rows and Columns\n");
	scanf("%d%d", &rows, &columns);
	
	int arr[rows][columns];

	printf("Enter Array Elements\n");
	for(int i = 0; i < rows; i++){
		
		for(int j = 0; j < columns; j++){
			
			scanf("%d", &arr[i][j]);
		}
	}

	printf("Array Element are\n");

	for(int i = 0; i < rows; i++){
		
		for(int j = 0; j < columns; j++){
			
			printf("%d\n", arr[i][j]);
		}
	}
	
	int ret = sum (arr,rows,columns);

	printf("sum = %d\n", ret);
}
