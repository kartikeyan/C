/*	Program 8 : Take no. of rows from the user
 *		    
 *		    rows = 4
 *
 *		    D C B A B C D
 *		      c b a b c
 *		        B A B
 *		          a
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows :\n");
	scanf("%d", &row);
	
	int num;

	for(int i = 1; i <= row; i++){

		for(int sp = 1; sp < i; sp++){
		
			printf("\t");
		}
		
		int num = row - i + 1;

		for(int j = 1; j <= (row - i) * 2 + 1; j++){
			
			if(i % 2 == 1){
				if(j <= row - i){
			
					printf("%c\t", 64+num);	
					num--;
				}else{
			
					printf("%c\t", 64+num);
					num++;
				}
			}else{
			
				if(j <= row - i){
				
					printf("%c\t", 96+num);
					num--;
				}else{
				
					printf("%c\t", 96+num);
					num++;
				}
			}
			
		}
		printf("\n");
	}
}
