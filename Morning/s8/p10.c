/*	Program 10 : Take no. of rows from the user
 *		    
 *		    rows = 4
 *
 *		    * * * # * * * 
 *		      * * # * *
 *		        * # *
 *		          #
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){

		for(int sp = 1; sp < i; sp++){
		
			printf("\t");
		}

		for(int j = 1; j <= (row - i) * 2 + 1; j++){
			
			if(j == row - i + 1){
			
				printf("#\t");
			}else{
			
				printf("*\t");
			}
		}
		printf("\n");
	}
}
