/*	Program 11 : Take no. of rows from the user
 *
 *	d d d d
 *	C C C
 *	b b 
 *	A
 */

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	char ch1 = 'd';
	char ch2 = 'D';

	for(int i = 1; i <= row; i++){

		for(int j = row; j >= i; j--){		
		
			if(i % 2 != 0){
				printf("%c ", ch1);
			}else{
				printf("%c ", ch2);
			}
			
		}
		ch1--;
		ch2--;
		printf("\n");
		
	}
}
