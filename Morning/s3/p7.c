/*	Program 7 : Take no. of rows from the user
 *
 *	4 3 2 1
 *	3 2 1
 *	2 1
 *	1
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	int x = 4;

	for(int i = 1; i <= row; i++){

		for(int j = row; j >= i; j--){		
		
			printf("%d ", x);
			x--;
		} 

		x = row - i;
		printf("\n");
	}
}
