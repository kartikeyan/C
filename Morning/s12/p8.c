/*	Program 8 : Take no. of rows from the user.
 *		    
 *		    rows = 4
 *			
 *			  1
 *			1 b 3
 *		      1 b 3 d 5	
 * 		    1 b 3 d 5 f 7
*/

#include<stdio.h>

void main(){

	int rows, cols = 1, sp;

	printf("Enter no. of rows :\n");
	scanf("%d", &rows);
	
	sp = rows;

	for(int i = 1; i <= rows; ){
		
		if(sp-i >= 1){
		
			printf("\t");
			sp--;

		}else if(cols <= (i - 1) * 2 + 1){
		
			if(cols % 2 == 1){
			
				printf("%d\t", cols);
			}else{
			
				printf("%c\t", 96+cols);
			}

			cols++;
		}else{
		
			printf("\n");
			cols = 1;
			sp = rows;
			i++;

		}
	}
}
