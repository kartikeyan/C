/*	Program 8 : Take no. of rows from the user.
 *	            
 *	            rows = 4
 *
 *			  D
 *			A C D
 *		      D C B C D	
 *	            D C B A B C D
 *	              D C B C D
 *	                D C D
 *	                  D
 *	                  
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows :\n");
	scanf("%d", &rows);

	int sp, cols;

	int ch = rows;

	for(int i = 1; i < rows * 2; i++){
		
		ch = rows;
	
		if(i <= rows){
		
			sp = rows - i;
			cols = i;
		}else{
		
			sp = i - rows;
			cols = rows * 2 - i;
		}

		for(int space = 1; space <= sp; space++){
		
			printf("\t");
		}

		for(int j = 1; j < cols * 2; j++){
			
			if(j <= cols){
				
				printf("%c\t", 64+ch);
				ch--;
			}else{
	
				printf("%c\t", 64+ch);
				ch++;
			}
		}

		printf("\n");
	}
}
