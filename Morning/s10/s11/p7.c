/*	Program 7 : Take no. of rows from the user.
 *	            
 *	            rows = 4
 *
 *			  A
 *			b A b
 *		      C b A b C	
 *	            d C b A b C d
 *	              C b A b C
 *	                b A b
 *	                  A
 *	                  
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows :\n");
	scanf("%d", &rows);

	int sp, cols,ch;

	for(int i = 1; i < rows * 2; i++){
	
		if(i <= rows){
		
			sp = rows - i;
			cols = i;
		}else{
		
			sp = i - rows;
			cols = rows * 2 - i;
		}

		for(int space = 1; space <= sp; space++){
		
			printf("\t");
		}

		ch = cols;

		for(int j = 1; j < cols * 2; j++){
			
			if(j < cols){
				if((cols+j) % 2 == 0){

					printf("%c\t", 64+ch);
					ch--;
				}else{
	
					printf("%c\t", 96+ch);
					ch--;
				}
			}else{
			
				if((cols+j) % 2 == 0){
				
					printf("%c\t", 96+ch);
					ch++;
				}else{
				
					printf("%c\t", 96+ch);
					ch++;
				}
			}
		}

		printf("\n");
	}
}
