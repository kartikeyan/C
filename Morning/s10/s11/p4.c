/*	Program 4 : Take no. of rows from the user.
 *	            
 *	            rows = 4
 *
 *			  1
 *			2 1 2
 *		      3 2 1 2 3	
 *	            4 3 2 1 2 3 4
 *	              3 2 1 2 3
 *	                2 1 2
 *	                  1
 *	                  
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows :\n");
	scanf("%d", &rows);

	int sp, cols, num;

	for(int i = 1; i < rows * 2; i++){
	
		if(i <= rows){
		
			sp = rows - i;
			cols = i;
		}else{
		
			sp = i - rows;
			cols = rows * 2 - i;
		}

		for(int space = 1; space <= sp; space++){
		
			printf("\t");
		}

		int num = cols;

		for(int j = 1; j < cols * 2; j++){
			
			if(j < cols){
				printf("%d\t", num);
				num--;
			}else{
				printf("%d\t", num);
				num++;
			}
		}

		printf("\n");
	}
}
