/*	Program 5 : Take no. of rows from the user.
 *		    
 *		    a b c
 *		    d e f
 *		    g h i
 *		    
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	int x = 97;

	for(int i = 1; i <= row; i++){
	
		for(int j = 1; j <= row; j++){
			
			printf("%c ", x);
			x++;
		}
		printf("\n");
	}
}
