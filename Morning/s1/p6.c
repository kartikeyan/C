/*	Program 6 : Take no. of rows from the user.
 *		    
 *		    1 1 1
 *		    2 2 2
 *		    3 3 3   
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
	
		for(int j = 1; j <= row; j++){
			
			printf("%d ", i);
		}
		printf("\n");
	}
}
