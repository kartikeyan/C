/*	Program 9 : Take no. of rows from the user.
 *	
 *	Here For rows = 3
 *
 *	-  -  1
 *	-  4  7
 *	10 13 16
*/	

#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows:\n");
	scanf("%d", &row);

	int num = 1;

	for(int i = 1; i <= row; i++){
	
		for(int space = row; space > i; space--){
		
			printf("  ");
		}

		for(int j = 1; j <= i; j++){
			
			printf("%d ", num);
			num += row;
		}
		printf("\n");
	}
}
