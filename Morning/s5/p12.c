/*	Program 12 : Take no. of rows from the user.
 *	
 *	Here For rows = 5
 *	
 *    -	- - - A       - - - -       A
 *    -	- - b a    =  - - -     +   b a
 *    -	- C E G       - -           C E G
 *    -	d c b a	      -		    d c b a
 *    E G I K M			    E G I K H
*/    

#include<stdio.h>
void main(){

	int row;
	printf("Enter no. of rows:\n");
	scanf("%d", &row);
	
	char ch = 60 + row;
	
	for(int i = 1; i <= row; i++){
	
		for(int space = row; space > i; space--){
		
			printf("  ");
		}

		for(int j = 1; j <= i; j++){
		
			if(i % 2 != 0){
				printf("%c ", ch);
				ch += 2;
			}else{
				printf("%c ", ch+31);
				ch--;
			}
			
		}
		printf("\n");
	}
}
