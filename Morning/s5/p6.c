/*	Program 6 : Take no. of rows from the user.
 *	
 *	Here For rows = 4
 *
 *	- - - 1
 *	- - 1 2
 *	- 1 2 3
 *	1 2 3 4
*/	
#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows:\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
	
		for(int space = row; space > i; space--){
		
			printf("  ");
		}
		
		int num = 1;

		for(int j = 1; j <= i; j++){
			
			printf("%d ", num);
			num++;
		}

		printf("\n");
	}
}
