/*	Program 10 : Take no. of rows from the user.
 *	
 *	Here For rows = 4
 *	
 *	- - - D       - - -     D
 *	- - c D   =   - -    +  c  D
 *	- B c D       -         B  c  D
 *	a B c D			a  B  c  D
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows:\n");
	scanf("%d", &row);

	char ch = 64 + row; 


	for(int i = 1; i <= row; i++){
	
		for(int space = row; space > i; space--){
		
			printf("  ");
		}

		for(int j = 1; j <= i; j++){
		
			if(){
				printf("%c ", );
			}else{
				printf("%c ", );
			}
			
		}
		printf("\n");
	}
}
