/*	Program 13 : Take no. of rows from the user
 *		    
 *		    A b C
 *		    d E f
 *		    G h i
 *		   
*/

#include<stdio.h>

void main(){
	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	int x = 65;
	for(int i=1; i <= row; i++){
		for(int j = 1; j <= row; j++){
			if(x % 2 == 0){
				printf("%c ", x + 32);
			}
			else{
				printf("%c ", x);
			}
			x++;	
		}
		printf("\n");
	}

}
