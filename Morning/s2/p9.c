/*	Program 9 : Take no. of rows from the user
 *		    
 *		    a B c
 *		    d E d
 *		    g H i
 *		   
*/

#include<stdio.h>

void main(){
	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	int x = 65;
	for(int i=1; i <= row; i++){
		for(int j = 1; j <= row; j++){
			if(j % 2 == 0){
				printf("%c ", x);
			}
			else{
				printf("%c ", x+32);
			}
			x++;
		}
		printf("\n");
	}

}
