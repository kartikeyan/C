/*	Program 10 : Take no. of rows from the user
 *		    
 *		    1 2 3
 *		    2 3 4
 *		    3 4 5
 *		   
*/

#include<stdio.h>

void main(){
	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	for(int i=1; i <= row; i++){
		int x = i;
		for(int j = 1; j <= row; j++){
			printf("%d ", x);
			x++;
		}
		printf("\n");
	}

}
