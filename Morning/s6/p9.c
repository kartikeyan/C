/*	Program 9 : Take no. of rows from the user.
 *
 *		    Here rows = 4
 *
 *		    100 9  64 7      =   	     100 9 64 7
 *		        36 5  16	-	+    36  5 16
 *		           3  4	        - - 	     3   4
 *		              1	        - - -	     1
*/

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);
	
	int x = 10;

	for(int i = 1; i <= rows; i++){
		
		for(int space = 1; space < i ; space++){

			printf("  ");
		}	

		for(int j = rows; j >= i; j--){
				
			if(x % 2 == 0){
				
				printf("%d ", x*x);
			}else{
				printf("%d ", x);
			}
			x--;
		}

		printf("\n");
	}
}
