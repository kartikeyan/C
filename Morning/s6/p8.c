/*	Program 8 : Take no. of rows from the user.
 *
 *		    Here rows = 4
 *
 *		   D D D D  =   	     D D D D
 *		     c c c	-	+    c c c
 *		       B B	- - 	     B B
 *		         a	- - -	     a
*/

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);
	
	char ch1 = 'D';
	char ch2 = 'd';

	for(int i = 1; i <= rows; i++){
		
		for(int space = 1; space < i ; space++){

			printf("  ");
		}	

		for(int j = rows; j >= i; j--){
				
			if(i % 2 == 0){
				printf("%c ", ch2);
			}else{
				printf("%c ", ch1);
			}
		}

		ch1--;
		ch2--;

		printf("\n");
	}
}
