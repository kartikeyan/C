/*	Program 11 : Take no. of rows from the user.
 *
 *		    Here rows = 4
 *
 *		    1 3 5 7 9       =   -	     1 3 5 7 9
 *		      9 7 5 3		- -	+    9 7 5 3
 *		        3 5 7	        - - - 	     3 5 7
 *		          7 5		- - - -	     7 5
 *		            5	   		     5
*/

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);
	
	int x = 1;

	for(int i = 1; i <= rows; i++){
		
		for(int space = 1; space < i ; space++){

			printf("  ");
		}	

		for(int j = rows; j >= i; j--){
			
			if(i % 2 == 0){
			
				printf("%d ", x);
				x -= 2;
			}else{
				printf("%d ", x);
				x += 2;
			}
		}

		printf("\n");
	}
}
