/*	Program 10 : Take no. of rows from the user.
 *
 *		    Here rows = 4
 *
 *		    a B c D      =   		     a B c D
 *		      e F g		-	+    e F g
 *		        H i	        - - 	     H i
 *		          J	   	- - -	     J
*/

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);:w
	
	char ch1 = 'a';
	char ch2 = 'A';

	for(int i = 1; i <= rows; i++){
		
		for(int space = 1; space < i ; space++){

			printf("  ");
		}	

		for(int j = rows; j >= i; j--){
				
			if(j % 2 == 0){
				
				printf("%c ", ch2);
			}else{
				printf("%c ", ch1);
			}
			
			ch1++;
			ch2++;
		}

		printf("\n");
	}
}
