/*	Program 3 : Take no. of rows from the user.
 *
 *		    - - - *
 *		    - - * * *
 *		    - * * * * *
 *		    * * * * * * *
*/

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter rows :\n");
	scanf("%d", &rows);

	for(int i = 1; i <= rows; i++){
	
		for(int space = rows; space > i; space--){
		
			printf("  ");
		}

		for(int j = 1; j <= 2 * i - 1; j++){
			
			printf("* ");
		}

		printf("\n");
		
	}
}
