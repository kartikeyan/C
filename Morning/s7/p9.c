/*	Program 7 : Take no. of rows from the user.
 *
 *		    
 *		    -  -  -   1
 *		    -  -   4  7  4
 *		    -   7 10 13 10 7
 *		    10 13 16 19 16 13 10   
*/

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter rows :\n");
	scanf("%d", &rows);

	int x = 1;

	for(int i = 1; i <= rows; i++){
	
		for(int space = rows; space > i; space--){
		
			printf("  ");
		}

		for(int j = 1; j <= 2 * i - 1; j++){
			
			if(j < i){
				printf("%d ", x);
				x += 3;
			}else{
				printf("%d ",x);
				x += 3;
			}
		}
		printf("\n");
	}
}
