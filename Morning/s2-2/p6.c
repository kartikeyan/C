/*	Program 6 : Take no. of rows from the user.
 *		    
 *		    Rows = 4
 *
 *		    4
 *		    3 3 
 *		    2 2 2
 *		    1 1 1 1
 *
*/		    
#include<stdio.h>

void main(){
	int row;
	printf("Enter rows:\n");
	scanf("%d", &row);

	for(int i = 4; i >= 1; i--){

		for(int j = 4; j >=i; j--){
			
			printf("%d ", i);
		}
		printf("\n");
	}
}
