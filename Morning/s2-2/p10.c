/*	Program 10 : Take no. of rows from the user.
 *		    
 *		    Rows = 4
 *
 *		    10
 *		    I H 
 *		    7 6 5
 *		    D C B A
 *		  
*/		    
#include<stdio.h>

void main(){
	int row;
	printf("Enter rows:\n");
	scanf("%d", &row);
	
	int x = 10;
	char ch = 'I';

	for(int i = 1; i <= row; i++){

		for(int j = 1; j <= i; j++){
			
			if(i % 2 != 0){
				printf("%d ", x);
				x--;
			}else{
				printf("%c ", ch);
				ch--;
			}
			
		}
		printf("\n");
	}
}
