
/*
 	How to avoid Dangling Pointer Errors in C

	We have seen three ways where Dangling Pointers can be created.

    		1] Deallocation of memory blocks performed using free() method.
    		2] Variable having limited scope during a function call.
    		3] Variable goes out of scope if the variable is declared inside some inner block of code.

	We can avoid these conditions by assigning NULL in case of deallocation of memory and using static variables in case of variables having local scope.

*/

/*
 	1. Assigning NULL after De-allocation of memory

		We should assign NULL to the ptr pointer as soon as the memory block pointed by the ptr has been deallocated using the free() function to avoid creating the dangling pointer problem in our program.

*/

#include<stdio.h>
#include<stdlib.h>

void main(){
	
	int *ptr = (int*)malloc(sizeof(int));

	*ptr = 5;

	// memory block deallocated using free() function
    	free(ptr);

    	// ptr is a dangling pointer
    	// prints garbage value
    	printf("%d\n", *ptr);

    	// this line added will help us to determine that ptr is not pointing to any location currently
    	// this pointer is known as NULL Pointer
    	ptr = NULL;

    	// not a Dangling Pointer anymore
    	// no output value
    	printf("%d", *ptr);
}

/*
 	1469696549
	Segmentation fault (core dumped)
*/

/*
 	Explanation :

    		1] First, We have allocated an integer memory block of 4-bytes using malloc() during runtime in the ptr pointer.
    		2] Value 5 has been assigned to the memory block pointed by ptr.

    		3] Using free(ptr), we have deallocated the memory block pointed by ptr.

    		4] printf("%d\n", *ptr); will print some garbage value as we have already freed the memory pointer by ptr.

    		5] We have added a statement ptr = NULL in the code. This ensures that the compiler knows ptr pointer is not a Dangling Pointer anymore and contains a defined value NULL. This pointer is known as a Null Pointer.

    		6] Now, printf("%d", *ptr); will print nothing as ptr is NULL and program exits with runtime error since we are trying to access memory that does not exist.

*/
