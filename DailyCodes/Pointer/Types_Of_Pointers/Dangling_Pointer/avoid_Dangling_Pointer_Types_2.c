
/*
 	2. Static variables with global scope

		1] Static variables are the variables that remain in the memory until the execution of the program finishes. We can use the static keyword to declare a static variable in our program.
		2] x is a static integer variable with global scope and will stay in the memory until the execution of the program finishes. 
		3]Global scope means we will be able to use our variable anywhere in our program, we can declare our variable anywhere in the program inside any function or in an inner block of code.

*/

#include<stdio.h>

// definition of notADanglingPointer() function
int *notADanglingPointer(){

    	// temp variable has a global scope
    	static int temp = 10;

    	// returning address of temp variable
    	return &temp;
}

int main(){

    	// ptr will point to temp variable
    	// as temp variable has a global scope now, it will not be destroyed after the execution of below line
    	int *ptr = notADanglingPointer();

    	// ptr is not a Dangling Pointer anymore
    	// ptr contains address of static variable temp
    	// and is pointing to temp with value 10
	
    	printf("%d\n", *ptr);		// 10
}

/*
 	Explanation :

    		1] notADanglingPointer() function now returns the address of a global variable.

    		2] ptr contains the address of temp variable i.e. a global variable.

    		3] ptr is acting as a Normal Pointer in this code.
    		4] Output will be value 10 i.e. stored in temp variable.

	Now, Let us see how we can resolve the Dangling Pointer Problem in case variable goes out of scope.

    		1] A pointer ptr is declared in the main() function, it is acting as a wild pointer.

    		2] When we enter the inner block of code, ptr points to temp variable having value 10 with static keyword. temp has a global scope and will remain in the memory after the program control moves out of the inner block.

    		3] ptr contains the address of temp. So, ptr will point to a global variable temp with value 10 and will not act as a dangling pointer.
*/
