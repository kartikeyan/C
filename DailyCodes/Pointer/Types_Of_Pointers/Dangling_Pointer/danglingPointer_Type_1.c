/*

	1. De-allocation of memory.

		1] Allocation and deallocation of memory blocks are performed using library functions, like malloc(), calloc() functions are used for allocating a memory block, while free() function is used to deallocate a memory block. 

		2] So, When we deallocate a memory block using free() function and do not modify the pointer value, it will cause the pointer to act as a Dangling Pointer.

		3] The free() function takes a single parameter, i.e. a pointer pointing to the memory to be deallocated.

*/

#include<stdio.h>
#include<stdlib.h>

void main(){
	
	int *ptr = (int*)malloc(sizeof(int));

	*ptr = 10;

	// memory block deallocated using free() function
	free(ptr);

	// here ptr acts as a dangling pointer
	printf("%d\n", *ptr);
	// prints garbage value in the output console
}

/*
 	
    	1] An integer pointer ptr points to an integer variable with value 10, ptr contains the address of the variable allocated dynamically using malloc() method.

    	2] When the integer variable gets deallocated from memory using the free(ptr); function, ptr points to some garbage value i.e. invalid location/data and acts as a dangling pointer.
	
	Explanation :

		In this program, we can see that

    		1] First, an integer pointer ptr has been assigned a memory block of sizeof(int) (generally 4-bytes) using malloc() function. It is acting as a normal pointer for now.

    		2] Integer memory block pointed by ptr has been assigned value 10.

    		3] Next, free(ptr) deallocates the 4-bytes of memory space (containing value 10) pointed by the ptr pointer.

		4] Now, ptr will act as a Dangling Pointer because it is pointing to some deallocated memory block.
*/
