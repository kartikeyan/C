
/*
 	3. Variable goes out of scope

		1] If a variable is declared inside some inner block of code, then the variable will have a local scope, and it will be deleted once the execution of the inner block ends. If the address of this local variable is assigned to a pointer declared outside the scope, then it will act as a Dangling Pointer outside the inner block of the code.

		2] The diagram below shows how a dangling pointer is created in case an integer variable goes out of scope.

    		3] A pointer ptr is declared in the main() function, it is acting as a wild pointer.

    		4] When we enter the inner block of code, ptr points to the temp variable having value 10. temp has local scope and will be deleted from the memory as soon as the program control moves out of the inner block.

    		5] temp goes out of scope and ptr still contains the address of deleted memory. So, ptr will point to some garbage value and will act as a dangling pointer.

*/

#include<stdio.h>

void main(){
	
	int *ptr;

	{
		int temp = 10;

		ptr = &temp;
	}

    	// temp is now removed from the memory (out of scope)
    	// now ptr is a dangling pointer
	printf("%d %d\n", *ptr, temp);
	
	// as temp is not in the memory anymore so it can't be modified using ptr

    	// prints garbage value
	printf("%d\n", *ptr);
}

/*
 	cc danglingPointer_Type_3.c
	danglingPointer_Type_3.c: In function ‘main’:
	danglingPointer_Type_3.c:31:33: error: ‘temp’ undeclared (first use in this function)
	   31 |         printf("%d %d\n", *ptr, temp);
	      |                                 ^~~~
	danglingPointer_Type_3.c:31:33: note: each undeclared identifier is reported only once for each function it appears in
*/
