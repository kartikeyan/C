/*	Program 4 :
 *	
 *	4 - strrev (string reverse)
 *	
 *	There is no strrev library function.
 *
 *	Using User defined Function
*/	

#include<stdio.h>

void mystrrev(char *str){

	char *x = str;

	while(*x != '\0'){
	
		x++;
	}

	x--;

	char temp;

	while(str < x){
	
		temp = *str;
		*str = *x;
		*x = temp;

		str++;
		x--;
	}
}

void main(){

	char str[50] = "Kartik Onkar";

	mystrrev(str);

	puts(str);
}
