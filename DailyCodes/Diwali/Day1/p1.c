/*	Program 1 : WAP to create 1-D Array of length n from the user also take value
 *		    from the user and print whole 1-D Array.
*/

#include<stdio.h>
void main(){

	int size;

	printf("Enter Array size :\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements :\n");
	
	for(int i = 0; i < size; i++){
	
		scanf("%d", &arr[i]);
	}
	
	printf("Array elements are :\n");
	for(int i = 0; i < size; i++){
	
		printf("%d\n", arr[i]);
	}

}
