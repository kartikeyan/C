/*	Program 3 : WAP to create 3-D Array of size given from user and print the 
 *		    sum of all left diagonals elements.
*/

#include<stdio.h>
void main(){

	int plane,row,col;
	
	printf("Enter plane in an Array:\n");
	scanf("%d", &plane);

	printf("Enter row in an Array:\n");
	scanf("%d", &row);

	printf("Enter col in an Array:\n");
	scanf("%d", &col);

	int arr1[plane][row][col];

	printf("Enter Array Elements :\n");

	for(int i = 0; i < plane; i++){
	
		for(int j = 0; j < row; j++){
			
			for(int k = 0; k < col; k++){

				scanf("%d", &arr1[i][j][k]);

			}
		}
	}
	
	int sum = 0;
	for(int i = 0; i < plane; i++){
	
		for(int j = 0; j < row; j++){
			
			for(int k = 0; k < col; k++){

				printf("%d\n", arr1[i][j][k]);

				if(j == k){
				
					sum += arr1[i][j][k];
				}
			}
		}
	}

	printf("Sum of all left diagonals elements is :%d\n", sum);
}
