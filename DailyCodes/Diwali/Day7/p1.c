/*	Program 1 : WAP which shows the example of array os strings and 
 *		    print the array.
*/

#include<stdio.h>

void main(){

	char college[][10] = {"COEP","VIT","PICT"};

	for(int i = 0; i < 3; i++){
	
		puts(college[i]);
		printf("-> %p\n", &college[i]);
	}
}
