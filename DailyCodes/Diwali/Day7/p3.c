/*	Program 3 : WAP to check the given two strings are anagram string or not.
*/

#include<stdio.h>

void main(){

	char str1[10];
	char str2[10];

	printf("ENter str1 : ");
	gets(str1);
	printf("Enter str2 : ");
	gets(str2);

	if(strlen(str1) != strlen(str2)){
	
		printf("String is not anagram\n");
	}else{
	
		int temp = 0;

		for(int i = 0; i < strlen(str1); i++){
		
			for(int j = i+1; j < strlen(str1); j++){
				
				if(str1[i] > str1[j]){
					
					temp = str1[i];
					str1[i] = str1[j];
					str1[j] = temp;
				}

				if(str2[i] > str2[j]){
				
					temp = str2[i];
					str2[i] = str2[j];
					str2[j] = temp;
				}
			}
		}

		int flag = 1;
		for(int i = 0; i < strlen(str1); i++){
			
			if(str1[i] != str2[i]){
				
				flag = 0;
				break;
			}
		}

		if(flag == 1){
			printf("String is anagram\n");
		}else{
			printf("String is not anagram\n");
		}
	}
}	
