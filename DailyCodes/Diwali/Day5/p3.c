/*	Program 3 : Write a function for reverse number, even or odd number, prime number
 *		    and call the all function using array of function.
*/		    

#include<stdio.h>

void rev(int num){

	int rev = 0;
	int rem;

	while(num != 0){
		
		rem = num % 10;
		rev = rev * 10 + rem;
		num = num / 10;
	}

	printf("Reverse number is %d\n", rev);
}

void evenOdd(int num){

	if(num % 2 == 0){
		
		printf("%d is Even number\n", num);
	}else{
	
		printf("%d is Odd number\n", num);
	}
}

void primeNum(int num){

	int flag = 1;

	for(int i = 2; i < num/2; i++){
		
		if(num % i == 0){
		
			flag = 0;
			break;
		}
	}
	if(flag == 1){
		
		printf("%d is Prime number\n", num);
	}else{
	
		printf("%d is not Prime number\n", num);
	}
}

void main(){

	int a;

	printf("Enter the number :\n");

	scanf("%d", &a);

	void (*fptr[3])(int) = {rev, evenOdd, primeNum};

	for(int i=0; i<3; i++){
	
		fptr[i](a);
	}
}
