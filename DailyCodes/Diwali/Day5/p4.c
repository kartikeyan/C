/*	Program 4 : WAP for concept of passing function to function.
*/

#include<stdio.h>

void add(int x, int y){

	printf("The sum of %d & %d is %d\n", x, y, x+y);
}

void fun(int x, int y, void (*ptr)(int, int)){
	
	ptr(x, y);
}	

void main(){

	int a, b;
	printf("Enter value of a and b:\n");
	scanf("%d %d", &a, &b);

	fun(a, b, add);
}
