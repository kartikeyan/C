/*	Program 1 : WAP to create array of size n given by user and 
 *		    take values from the user and print array using pointer.
 *
*/

#include<stdio.h>

void main(){

	int n;
	printf("Enter array of size n\n");
	scanf("%d", &n);

	int arr[n];
	
	printf("Enter array elements :\n");
	for(int i = 0; i < n; i++){
	
		scanf("%d", &arr[i]);
	}
	
	printf("Array elements are :\n");
	for(int i = 0; i < n; i++){
	
		printf("%d\n", *(arr + i));
	}
}
