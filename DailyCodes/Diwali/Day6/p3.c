/*	Program 3 : WAP to your own string copy function and concat it,
 *		    print the copied and concated string.
*/

#include<stdio.h>

char *mystrcpy(char *str2, const char *str1){

	while(*str1 != '\0'){
	
		*str2 = *str1;
		str1++;
		str2++;
	}
	return str2;
}

char *mystrcat(char *str1, char *str2){

	while(*str1 != '\0'){
		str1++;
	}

	while(*str2 != '\0'){
		
		*str1 = *str2;
		str1++;
		str2++;
	}
	*str1 = '\0';
	return str1;
}

void main(){

	char str1[20];

	gets(str1);

	char str2[20];

	printf("\nAfter String Copy\n");
	mystrcpy(str2, str1);		//mystrcpy(dest, source);
	printf("str1->");
	puts(str1);
	printf("str2->");
	puts(str2);

	printf("\nAfter String concat\n");
	mystrcat(str1, str2);		//mystrcat(source, dest);
	printf("str1->");
	puts(str1);
	printf("str2->");
	puts(str2);
}
