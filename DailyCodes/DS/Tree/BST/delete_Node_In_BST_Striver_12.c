#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct BSTNode{
	
	int data;

	struct BSTNode *left;
	struct BSTNode *right;
}node;

struct BSTNode* constructBST(struct BSTNode *root, int ele){
	
	if(root == NULL){
		
		struct BSTNode *newNode = (node*)malloc(sizeof(node));

		newNode->data = ele;

		newNode->left = NULL;

		newNode->right = NULL;
		
		root = newNode;

		return root;
	}

	if(root->data > ele){
		
		root->left = constructBST(root->left, ele);
	}else{

		root->right = constructBST(root->right, ele);
	}

	return root;	// add compulsory
}

void printBST(node *root){	// Inorder traversal in BST is always sorted
	
	if(root == NULL){
		
		return;
	}

	printBST(root->left);

	printf("%d ", root->data);

	printBST(root->right);
}

struct BSTNode* findLastRight(node *root){

        if(root->right == NULL){

                return NULL;
        }

        return findLastRight(root->right);
}


struct BSTNode* helper(node *root){
	
	if(root->left == NULL){
		
		return root->right;
	}else if(root->right == NULL){
		
		return root->left;
	}

	node *rightChild = root->right;

	node *lastRight = findLastRight(root->left);

	lastRight->right = rightChild;

	return root->left;
}

struct BSTNode* deleteInBST(node *root, int key){

	if(root == NULL){
		
		return NULL;
	}	

	if(root->data == key){
		
		return helper(root);
	}

	node* temp = root;

	while(root != NULL){
		
		if(root->data > key){
			
			if(root->left != NULL && root->left->data == key){
				
				root->left = helper(root->left);

				break;
			}else
				root = root->left;
		}else{
			
			if(root->right != NULL && root->right->data == key){
				
				root->right = helper(root->right);

				break;
			}else{
				root = root->right;
			}
		}
	}

	return temp;
}

void main(){
	
	struct BSTNode *root = NULL;

	int nodeCount, ele;

	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
		
		scanf("%d", &ele);

		root = constructBST(root, ele);
	}
	
	printf("BST Traversal is\n");
	printBST(root);
	printf("\n");

	int nodeData;
	printf("Enter node data to be deleted\n");
	scanf("%d", &nodeData);

	root = deleteInBST(root, nodeData);
	
	printf("After Deleting Node\n");
	printBST(root);
	printf("\n");
}
