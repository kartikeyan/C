#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct BSTNode{
	
	int data;

	struct BSTNode *left;
	struct BSTNode *right;
}node;

struct BSTNode* constructBST(struct BSTNode *root, int ele){
	
	if(root == NULL){
		
		struct BSTNode *newNode = (node*)malloc(sizeof(node));

		newNode->data = ele;

		newNode->left = NULL;

		newNode->right = NULL;
		
		root = newNode;

		return root;
	}

	if(root->data > ele){
		
		root->left = constructBST(root->left, ele);
	}else{

		root->right = constructBST(root->right, ele);
	}

	return root;	// add compulsory
}

void printBST(node *root){	// Inorder traversal in BST is always sorted
	
	if(root == NULL){
		
		return;
	}

	printBST(root->left);

	printf("%d ", root->data);

	printBST(root->right);
}

int cnt = 0, ans;

void Inorder(node *root, int k){
	
	if(root == NULL){
		
		return;
	}

	Inorder(root->left, k);

	cnt++;

	if(cnt == k){
		
		ans = root->data;
		return;
	}

	Inorder(root->right, k);
}
	
int kThSmallest(node *root, int k){

	Inorder(root, k);	

	return ans;
}

void main(){
	
	struct BSTNode *root = NULL;

	int nodeCount, ele;

	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
		
		scanf("%d", &ele);

		root = constructBST(root, ele);
	}
	
	printf("BST Traversal is\n");
	printBST(root);
	printf("\n");

	int k;
	printf("Enter Kth Smallest Ele Data\n");
	scanf("%d", &k);

	int ret = kThSmallest(root, k);

	printf("Kth Smallest Ele Data is %d\n", ret);
}
