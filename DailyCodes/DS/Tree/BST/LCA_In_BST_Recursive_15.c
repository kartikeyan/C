#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<limits.h>

typedef struct BSTNode{
	
	int data;

	struct BSTNode *left;
	struct BSTNode *right;
}node;

struct BSTNode* constructBST(struct BSTNode *root, int ele){
	
	if(root == NULL){
		
		struct BSTNode *newNode = (node*)malloc(sizeof(node));

		newNode->data = ele;

		newNode->left = NULL;

		newNode->right = NULL;
		
		root = newNode;

		return root;
	}

	if(root->data > ele){
		
		root->left = constructBST(root->left, ele);
	}else{

		root->right = constructBST(root->right, ele);
	}

	return root;	// add compulsory
}

void printBST(node *root){	// Inorder traversal in BST is always sorted
	
	if(root == NULL){
		
		return;
	}

	printBST(root->left);

	printf("%d ", root->data);

	printBST(root->right);
}

//	LCA Lowest Common Ancestor i.e first common intersection of both the nodes

node* lcaInBST(node *root, int v1, int v2){
	
	if(root == NULL){
		
		return NULL;
	}

	int curr = root->data;

	if(curr < v1 && curr < v2){
		
		return lcaInBST(root->right, v1, v2);
	}

	if(curr > v1 && curr > v2){
		
		return lcaInBST(root->left, v1, v2);
	}

	return root;
}

void main(){
	
	struct BSTNode *root = NULL;

	int nodeCount, ele;

	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
		
		scanf("%d", &ele);

		root = constructBST(root, ele);
	}
	
	printf("BST Traversal is\n");
	printBST(root);
	printf("\n");

	int n1, n2;
	printf("Enter n1 and n2\n");
	scanf("%d %d", &n1, &n2);

	node* ret = lcaInBST(root,n1,n2);
	
	printf("LCA in BST is %d\n", ret->data);
}
