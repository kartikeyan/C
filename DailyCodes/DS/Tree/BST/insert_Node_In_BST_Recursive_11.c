#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct BSTNode{
	
	int data;

	struct BSTNode *left;
	struct BSTNode *right;
}node;

struct BSTNode* constructBST(struct BSTNode *root, int ele){
	
	if(root == NULL){
		
		struct BSTNode *newNode = (node*)malloc(sizeof(node));

		newNode->data = ele;

		newNode->left = NULL;

		newNode->right = NULL;
		
		root = newNode;

		return root;
	}

	if(root->data > ele){
		
		root->left = constructBST(root->left, ele);
	}else{

		root->right = constructBST(root->right, ele);
	}

	return root;	// add compulsory
}

void printBST(node *root){	// Inorder traversal in BST is always sorted
	
	if(root == NULL){
		
		return;
	}

	printBST(root->left);

	printf("%d ", root->data);

	printBST(root->right);
}

node* insertInBST(node *root, int val){

	if(root == NULL){
		
		node* newNode = (node*)malloc(sizeof(node));

		newNode->data = val;

		newNode->left = NULL;

		newNode->right = NULL;

		root = newNode;

		return root;
	}

	if(val < root->data){
		
		root->left = insertInBST(root->left, val);
	}else{
		root->right = insertInBST(root->right, val);
	}

	return root;
}

void main(){
	
	struct BSTNode *root = NULL;

	int nodeCount, ele;

	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
		
		scanf("%d", &ele);

		root = constructBST(root, ele);
	}
	
	printf("BST Traversal is\n");
	printBST(root);
	printf("\n");

	int nodeData;
	printf("Enter node data\n");
	scanf("%d", &nodeData);

	root = insertInBST(root, nodeData);
	
	printf("After Adding Node\n");
	printBST(root);
	printf("\n");
}
