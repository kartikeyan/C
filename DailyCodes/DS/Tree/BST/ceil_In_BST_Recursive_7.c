#include<stdio.h>
#include<stdlib.h>

typedef struct BSTNode{
	
	int data;

	struct BSTNode *left;
	struct BSTNode *right;
}node;

struct BSTNode* constructBST(struct BSTNode *root, int ele){
	
	if(root == NULL){
		
		struct BSTNode *newNode = (node*)malloc(sizeof(node));

		newNode->data = ele;

		newNode->left = NULL;

		newNode->right = NULL;
		
		root = newNode;

		return root;
	}

	if(root->data > ele){
		
		root->left = constructBST(root->left, ele);
	}else{

		root->right = constructBST(root->right, ele);
	}

	return root;	// add compulsory
}

void printBST(node *root){	// Inorder traversal in BST is always sorted
	
	if(root == NULL){
		
		return;
	}

	printBST(root->left);

	printf("%d ", root->data);

	printBST(root->right);
}

int ceilInBST(node *root, int key, int ceil){
	
	if(root == NULL){
		
		return ceil;	// T.C = O(h) height of BST
	}

	if(root->data == key){
		
		return ceil = root->data;
	}

	if(root->data > key){
		
		ceil = root->data;

		return ceilInBST(root->left, key, ceil);
	}

	return ceilInBST(root->right, key, ceil);
}

void main(){
	
	struct BSTNode *root = NULL;

	int nodeCount, ele;

	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
		
		scanf("%d", &ele);

		root = constructBST(root, ele);
	}
	
	printf("BST Traversal is\n");
	printBST(root);
	printf("\n");

	int key;
	printf("Enter key\n");
	scanf("%d", &key);

	int ceil = -1;

	int ret = ceilInBST(root, key, ceil);
	
	printf("Ceil is %d\n", ret);
}
