#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<limits.h>

typedef struct BSTNode{
	
	int data;

	struct BSTNode *left;
	struct BSTNode *right;
}node;

struct BSTNode* constructBST(struct BSTNode *root, int ele){
	
	if(root == NULL){
		
		struct BSTNode *newNode = (node*)malloc(sizeof(node));

		newNode->data = ele;

		newNode->left = NULL;

		newNode->right = NULL;
		
		root = newNode;

		return root;
	}

	if(root->data > ele){
		
		root->left = constructBST(root->left, ele);
	}else{

		root->right = constructBST(root->right, ele);
	}

	return root;	// add compulsory
}

void printBST(node *root){	// Inorder traversal in BST is always sorted
	
	if(root == NULL){
		
		return;
	}

	printBST(root->left);

	printf("%d ", root->data);

	printBST(root->right);
}


bool isValid(node *root, long minVal, long maxVal){
	
	if(root == NULL){
		
		return true;
	}

	if(root->data >= maxVal || root->data <= minVal){
		
		return false;
	}

	return isValid(root->left, minVal, root->data) && isValid(root->right, root->data, maxVal);

}

bool isValidBST(node *root){
	
	return isValid(root, INT_MIN, INT_MAX);
}


int maxInBST(node *root){

        node *temp = root;

	if(root == NULL){

		return INT_MIN;
	}

        while(temp->right != NULL){

                temp = temp->right;
        }

        return temp->data;
}

int minInBST(node *root){

        node *temp = root;

	if(root == NULL){

		return INT_MAX;
        }

        while(temp->left != NULL){

                temp = temp->left;
        }

        return temp->data;
}

bool isValidBST(node *root){
	
	if(root == NULL){
		
		return true;
	}

	int max = maxInBST(root->left);		
	int min = minInBST(root->right);

	if(max > root->data || min < root->data){
		
		return false;
	}

	return isValidBST(root->left) && isValidBST(root->right);
}

void main(){
	
	struct BSTNode *root = NULL;

	int nodeCount, ele;

	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
		
		scanf("%d", &ele);

		root = constructBST(root, ele);
	}
	
	printf("BST Traversal is\n");
	printBST(root);
	printf("\n");

	bool ret = isValidBST(root);
	
	if(ret == true){
		printf("Valid BST\n");
	}else
		printf("Not a Valid BST\n");
}
