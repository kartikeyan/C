#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct BSTNode{
	
	int data;

	struct BSTNode *left;
	struct BSTNode *right;
}node;

struct BSTNode* constructBST(struct BSTNode *root, int ele){
	
	if(root == NULL){
		
		struct BSTNode *newNode = (node*)malloc(sizeof(node));

		newNode->data = ele;

		newNode->left = NULL;

		newNode->right = NULL;
		
		root = newNode;

		return root;
	}

	if(root->data > ele){
		
		root->left = constructBST(root->left, ele);
	}else{

		root->right = constructBST(root->right, ele);
	}

	return root;	// add compulsory
}

void printBST(node *root){	// Inorder traversal in BST is always sorted
	
	if(root == NULL){
		
		return;
	}

	printBST(root->left);

	printf("%d ", root->data);

	printBST(root->right);
}

//	Approach - 1


int cNodes = 0;

void countNodes(node *root){

        if(root == NULL){

                return;
        }

        countNodes(root->left);

        cNodes++;

        countNodes(root->right);
}

int cnt = 0, ans;

void Inorder(node *root, int k){
	
	if(root == NULL){
		
		return;
	}

	Inorder(root->left, k);

	cnt++;

	if(cnt == cNodes-k+1){		
		
		ans = root->data;
		return;
	}

	Inorder(root->right, k);
}

int kThLargest(node *root, int k){

	countNodes(root);

	printf("Node Count is %d\n",cNodes);

	Inorder(root, k);

	return ans;
}


//	2 - Approach - Revers Inorder gives nodes in descending fashion

int cnt = 0, ans;

void inorder(node *root, int k){
	
	if(root == NULL){
		return;
	}

	inorder(root->right, k);

	cnt++;

	if(cnt == k){
		
		ans = root->data;

		return;
	}

	inorder(root->left, k);
}

int kThLargest(node *root, int k){

	inorder(root, k);     

	return ans;
}


void main(){
	
	struct BSTNode *root = NULL;

	int nodeCount, ele;

	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
		
		scanf("%d", &ele);

		root = constructBST(root, ele);
	}
	
	printf("BST Traversal is\n");
	printBST(root);
	printf("\n");

	int k;
	printf("Enter Kth Largest Ele Data\n");
	scanf("%d", &k);

	int ret = kThLargest(root, k);

	printf("%dth Largest Ele Data is %d\n", k,ret);
}
