#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct BSTNode{
	
	int data;

	struct BSTNode *left;
	struct BSTNode *right;
}node;

struct BSTNode* constructBST(struct BSTNode *root, int ele){
	
	if(root == NULL){
		
		struct BSTNode *newNode = (node*)malloc(sizeof(node));

		newNode->data = ele;

		newNode->left = NULL;

		newNode->right = NULL;
		
		root = newNode;

		return root;
	}

	if(root->data > ele){
		
		root->left = constructBST(root->left, ele);
	}else{

		root->right = constructBST(root->right, ele);
	}

	return root;	// add compulsory
}

void printBST(node *root){	// Inorder traversal in BST is always sorted
	
	if(root == NULL){
		
		return;
	}

	printBST(root->left);

	printf("%d ", root->data);

	printBST(root->right);
}

int floorInBST(node *root, int key, int floor){

	if(root == NULL){
		
		return floor;
	}

	if(root->data == key){
		
		return floor = root->data;
	}

	if(key > root->data){
		
		floor = root->data;

		return floorInBST(root->right, key, floor);
	}

	return floorInBST(root->left, key, floor);
}

void main(){
	
	struct BSTNode *root = NULL;

	int nodeCount, ele;

	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
		
		scanf("%d", &ele);

		root = constructBST(root, ele);
	}
	
	printf("BST Traversal is\n");
	printBST(root);
	printf("\n");

	int key;
	printf("Enter key\n");
	scanf("%d", &key);

	int floor = -1;

	int ret = floorInBST(root, key, floor);
	
	printf("Floor is %d\n", ret);
}
