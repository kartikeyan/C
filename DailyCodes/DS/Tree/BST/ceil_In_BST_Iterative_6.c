#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct BSTNode{
	
	int data;

	struct BSTNode *left;
	struct BSTNode *right;
}node;

struct BSTNode* constructBST(struct BSTNode *root, int ele){
	
	if(root == NULL){
		
		struct BSTNode *newNode = (node*)malloc(sizeof(node));

		newNode->data = ele;

		newNode->left = NULL;

		newNode->right = NULL;
		
		root = newNode;

		return root;
	}

	if(root->data > ele){
		
		root->left = constructBST(root->left, ele);
	}else{

		root->right = constructBST(root->right, ele);
	}

	return root;	// add compulsory
}

void printBST(node *root){	// Inorder traversal in BST is always sorted
	
	if(root == NULL){
		
		return;
	}

	printBST(root->left);

	printf("%d ", root->data);

	printBST(root->right);
}

int ceilInBST(node *root, int key){
	
	node *temp = root;

	int ceil = -1;			// T.C = O(h) height of BST

	while(temp != NULL){
		
		if(temp->data == key){
			
			ceil = temp->data;

			return ceil;
		}

		if(key > temp->data){
			
			temp = temp->right;

		}else{
			ceil = temp->data;

			temp = temp->left;
		}
	}

	return ceil;
}

void main(){
	
	struct BSTNode *root = NULL;

	int nodeCount, ele;

	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
		
		scanf("%d", &ele);

		root = constructBST(root, ele);
	}
	
	printf("BST Traversal is\n");
	printBST(root);
	printf("\n");

	int key;
	printf("Enter key\n");
	scanf("%d", &key);

	int ret = ceilInBST(root, key);
	
	printf("Ceil is %d\n", ret);
}
