/*	Program 1 : WAP to make an array of the table of GIven NUmber.
 *		    Print the array using a pointer.
 *
 *		    Input : 2
 *		    Output : 2 4 6 8 10 12 14 16 18 20
*/
#include<stdio.h>
void main(){

	int num;
	printf("Enter num :\n");
	scanf("%d", &num);

	int arr[10];
	
	printf("Array elements are :\n");

	for(int i = 0; i < 10; i++){
		
		arr[i] = (i + 1) * num;
		
		printf("%d ", *(arr + i));
	}
}
