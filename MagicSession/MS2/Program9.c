/*	PRogram 9 : Create a 2D Array and take the input of elements from the user
 *		    And Print using a pointer.
*/

#include<stdio.h>
void main(){

	int row, col;
	printf("Enter row and col :\n");
	scanf("%d %d", &row, &col);

	int arr[row][col];

	printf("Enter array elements :\n");

	for(int i = 0; i < row; i++){
		for(int j = 0; j < col; j++){
			scanf("%d", &arr[i][j]);
		}
	}

	for(int i = 0; i < row; i++){
		for(int j = 0; j < col; j++){
			printf("%d ", *(*(arr + i) + j));
		}
	}
}
