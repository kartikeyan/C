/*	Program 10 : Create a 3D array and take the input of elements from the user and
 *		     Print using a pointer.
*/

#include<stdio.h>
void main(){

	int plane, row, col;
	printf("Enter plane,row,col :\n");
	scanf("%d %d %d", &plane, &row, &col);
	
	int arr[plane][row][col];
	printf("Enter Array elemets :\n");

	for(int i = 0; i < plane ; i++){
		for(int j = 0; j < row; j++){
			for(int k = 0; k < col; k++){
				scanf("%d", &arr[i][j][k]);
			}
		}
	}
	
	for(int i = 0; i < plane ; i++){
		for(int j = 0; j < row; j++){
			for(int k = 0; k < col; k++){
				printf("%d ", *(*(*(arr + i) + j) + k));
			}
		}
	}
}
