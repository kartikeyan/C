/*	Program 5 :
*/

#include<stdio.h>

char *mystrcat(char *str1, char *str2){

	while(*str1 != '\0'){
	
		str1++;
	}

	while(*str2 != '\0'){
		
		*str2 = *str1;
		str1++;
		str2++;
	}

	*str1 = '\0';
	return str1;
}

void main(){

	char str1[100];
	printf("Enter str1 :\n");
	gets(str1);

	char str2[100];
	printf("Enter str2 :\n");
	gets(str2);

	mystrcat(str1, str2);

	puts(str1);
	puts(str2);
}
