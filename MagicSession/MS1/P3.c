/*	Program 3 : WAP to print all the composite numbers between a given range
 *		    
 *		    Input : Enter Start : 1
 *		    	    Enter End : 15
 *
 *		    Output : 4 6 8 9 10 12 14 15
 *
 *		    Input : Enter Start : 31
 *		    	    Enter End : 35
 *
 *		    Output : 32 33 34 35
*/

#include<stdio.h>
void main(){
	
	int start,end;
	printf("Enter the start:");
	scanf("%d",&start);
	
	printf("Enter the start:");
	scanf("%d",&end);

	for (int i = start; i<end; i++) {
		int count = 0;
		
		for (int j = 1; j<=end; j++) {

			if(i%j==0)
				count++;
		}
		if(count>=3)
			printf("%d ",i);
	}
	printf("\n");
	
 
}
