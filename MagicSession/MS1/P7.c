/*	Program 7 : Take no. of rows from the user
 *
 *		    E D C B A
 *		    E D C B
 *		    E D C
 *		    E D
 *		    E
*/

#include<stdio.h>
void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
		
		int x = 69;

		for(int j = row; j >= i; j--){
		
			printf("%c ", x);
			x--;
		}

		printf("\n");
	}
}
