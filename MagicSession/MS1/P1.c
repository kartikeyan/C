/*	Program 1 : WAP to print all divisors of the entered num
*/

#include<stdio.h>

void main(){

	int x;
	printf("Enter value of x :\n");
	scanf("%d", &x);

	for(int i = 1; i <= x; i++){
	
		if(x % i == 0){
		
			printf("%d\n", i);
		}
	}
}
