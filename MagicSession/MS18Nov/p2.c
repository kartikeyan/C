/*	Program 2 : WAP that dynamically allocates a 1-D Array of marks, 
 *		    that takes marks from the user, and prints it.
*/	

#include<stdio.h>
#include<stdlib.h>

void main(){

	int size;
	printf("Enter the size :\n");
	scanf("%d", &size);

	float *marks = (float *) malloc (size*sizeof(float));
	
	printf("Enter Marks in Array :\n");
	for(int i=0;i<size;i++){
		scanf("%f", (marks + i));		// &marks[i]
	}
	
	printf("Marks are :\n"); 
	for(int i = 0; i < size; i++){
	
		printf("%f\n", *(marks + i));		//marks[i]
	}
}
