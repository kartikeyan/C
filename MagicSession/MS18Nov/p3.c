/*	Program 3 : WAP that dynamically allocates a 2-D Array of integers, 
 *		    takes values from the user, and prints it.
*/	

#include<stdio.h>
#include<stdlib.h>

void main(){

	int row;
	printf("Enter no. of rows :\n");
	scanf("%d", &row);

	int col;
	printf("Enter no. of cols :\n");
	scanf("%d", &col);

	int **ptr = (int **) malloc (sizeof(int*) * row);
	
	for(int i = 0; i < row; i++){
	
		ptr[i] = (int *) malloc (sizeof(int*) * col);
	}

	printf("Enter the array elements :\n");
	for(int i = 0; i < row; i++){

		for(int j = 0; j < col; j++){
		
			scanf("%d", (*(ptr + i) + j));
		}
	}
	
	printf("Array elements are :\n");
	for(int i = 0; i < row; i++){
		
		for(int j = 0; j < col; j++){
		
			printf("%d\n", *(*(ptr + i) + j));	
		}
	}
}

/*	int *ptr = (int *) malloc (row*col*sizeof(int));

	for(int i = 0; i < row; i++){

		for(int j = 0; j < col; j++){
		
			scanf("%d", (*(ptr + i) + j));
		}
	}
	
	for(int i = 0; i < row; i++){
		
		for(int j = 0; j < col; j++){

			printf("%d\n", *(*(ptr + i) + j);
		}
	}

*/	
