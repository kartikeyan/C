/*	Program 4 : WAP that dynamically allocates a 3-D Array of integers, 
 *		    takes values from the user, and prints it.
 *
 *		    Plane = 2
 *		    rows = 2
 *		    cloumn = 3 
*/	

#include<stdio.h>
#include<stdlib.h>

void main(){

	int plane, row , col;

	printf("Enter the plane :\n");
	scanf("%d", &plane);

	printf("Enter the row :\n");
	scanf("%d", &row);

	printf("Enter the col :\n");
	scanf("%d", &col);


	int ***ptr = (int ***) malloc (sizeof(int*) * plane);


	for(int i = 0; i < row; i++){
	
		ptr[i] = (int **) malloc (sizeof(int*) * row);

		for(int j = 0; j < col; j++){
			
			ptr[i][j] = (int *) malloc (sizeof(int) * col);
		}
	}

	printf("Enter the array elements :\n");
	for(int i = 0; i < plane; i++){
	
		for(int j = 0; j < row; j++){
		
			for(int k = 0; k < col; k++){
			
				scanf("%d", *(*(ptr + i) + j) + k);
			}
		}
	}

	printf("Array elements are :\n");
	for(int i = 0; i < plane; i++){
	
		for(int j = 0; j < row; j++){
		
			for(int k = 0; k < col; k++){
			
				printf("%d\n", *(*(*(ptr + i) + j) + k));
			}
		}
	}
}
