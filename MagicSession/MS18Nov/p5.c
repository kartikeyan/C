/*	PRogram 5 : WAP to free the memory allocated by the following program
*/

#include<stdio.h>
#include<stdlib.h>

void main(){

	int row, col;
	
	printf("Enter rows :\n");
	scanf("%d", &row);

	printf("Enter cols :\n");
	scanf("%d", &col);

	int **ptr = (int **)malloc(row *sizeof(int *));

	for(int i = 0; i < row; i++){
	
		ptr[i] = (int *)malloc(col * sizeof(int));
	}
	
	printf("Enter the array elements :\n");
	for(int i = 0; i < row; i++){
	
		for(int j = 0; j < col; j++){
		
			scanf("%d", (*(ptr + i) + j)); 
		}
	}

	printf("Array elements are :\n");
	for(int i = 0; i < row; i++){
	
		for(int j = 0; j < col; j++){
		
			printf("%d\n", *(*(ptr + i) + j));
		}
	}

	for(int i = 0; i < row; i++){
	
		free(ptr[i]);
	}

	free(ptr);
	ptr = NULL;
}
