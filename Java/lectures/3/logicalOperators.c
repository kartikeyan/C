
#include<stdio.h>
#include<stdbool.h>

void main(){

	int x = 5, y = 7;

	bool ans1 = x < y && y < x;
	bool ans2 = x < y || y < x;

	printf("%d\n", ans1);
	printf("%d\n", ans2);
}
