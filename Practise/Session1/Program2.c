/* 	Program 2 : WAP to find max among 3 numbers
*/  	

#include<stdio.h>

void main(){
	int x,y,z;

	printf("Enter value of x :\n");
	scanf("%d", &x);

	printf("Enter value of y :\n");
	scanf("%d", &y);

	printf("Enter value of z :\n");
	scanf("%d", &z);

	if(x > y && x > z){
		printf("x is greater than y and z\n");
	}else if(y > x && y > z){
		printf("y is greater than x and y\n");
	}else if(z > x && z > y){
		printf("z is greater than x and y\n");
	}else if(x == y == z){
		printf("x,y,z are all equal\n");
	}else{
		printf("Wrong input\n");
	}
}
