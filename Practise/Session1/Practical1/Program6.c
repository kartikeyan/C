/*	Program 6:WAP to get 10 numbers from Users and find
 *		  their sum and average
*/

#include<stdio.h>

void main(){
	int x;
	int sum = 0;
	float avg = 0;

	printf("Enter 10 numbers :\n");

	for(int i=1; i<=10; i++){
	
		printf("%d no. value is : ", i);
		scanf("%d", &x);

		sum = sum + x;
	}

	avg =(float) sum / 10;

	printf("Total sum of 10 numbers :%d\n", sum);
	printf("Total average of 10 numbers :%f\n", avg);
}
