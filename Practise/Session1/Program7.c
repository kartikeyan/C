/*	Program 7:WAP to print the pythagorian triplets
 *
*/

#include<stdio.h>

void main(){

	int a,b,c;
	
	printf("Enter value of a :\n");
	scanf("%d", &a);

	printf("Enter value of b :\n");
	scanf("%d", &b);
	
	printf("Enter value of c :\n");
	scanf("%d", &c);

	if(a*a + b*b == c*c){
		printf("a,b,c are pythagorian triplets\n");
	}else{
		printf("they are not\n");
	}
}
