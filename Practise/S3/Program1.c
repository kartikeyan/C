/*	Program 1 :  WAP to find the given element from the array.
 *		     Take array size and array elements from the user.
 *
 *		     i/p : enter array : 10 12 13 15 16 14
 *		     	   enter element : 15
 *		     	   o/p : 15 is present
*/

#include<stdio.h>
void main(){
	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array:\n");

	for(int i = 0; i < size; i++){
		scanf("%d", &arr[i]);
	}
	
	printf("Array elements are :\n");
	for(int i = 0; i < size; i++){
		printf("%d\n", arr[i]);
	}
	
	int search;
	printf("Enter element to be searched:\n");
	scanf("%d", &search);
	
	int flag = 0;

	for(int i = 0; i < size; i++){
		
		if(search == arr[i]){
			flag = 1;
		}
	}

	if(flag == 1){
		printf("%d is present\n", search);
	}else{
		printf("%d is not present\n", search);
	}


/*	for(int i = 0; i < size; i++){
		if(search == arr[i]){
			printf("%d is present\n", search);
			break;
		}else{
			printf("%d is not present\n",search);
			break;
		       	
		}
	}*/
}
