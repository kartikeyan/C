/*	Program 7 : Print the maximum integer data
 *		    from the above code.
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	int data;

	struct Demo *next;
}demo;

demo *head = NULL;

void addNodes(){

	demo *newNode = (demo*)malloc(sizeof(demo));

	printf("Enter Integer data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	if(head == NULL){
	
		head = newNode;
	}else{
	
		demo *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void maxData(){

	demo *temp = head;

	int max = temp->data;

	while(temp != NULL){
		
		if(temp->data > max){

			max = temp->data;
		}

		temp = temp->next;
	}

	printf("Maximum Data is :%d\n", max);
}

void printLL(){

	demo *temp = head;

	while(temp != NULL){
	
		printf("|Integer data is : %d |", temp->data);

		temp = temp->next;
	}
	printf("\n");
}

void main(){

	int nodes;
	printf("Enter No. of Nodes\n");
	scanf("%d", &nodes);

	for(int i = 0; i < nodes; i++){
	
		addNodes();
	}

	printLL();
	maxData();
}
