/*	Program 8 : Print the minimum integer data
 *		    from the above code.
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	int data;

	struct Demo *next;
}demo;

demo *head = NULL;

void addNodes(){

	demo *newNode = (demo*)malloc(sizeof(demo));

	printf("Enter Integer data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	if(head == NULL){
	
		head = newNode;
	}else{
	
		demo *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void minData(){

	demo *temp = head;

	int min = temp->data;

	while(temp != NULL){
		
		if(temp->data < min){

			min = temp->data;
		}

		temp = temp->next;
	}

	printf("Minimum Data is :%d\n", min);
}

void printLL(){

	demo *temp = head;

	while(temp != NULL){
	
		printf("|Integer data is : %d |", temp->data);

		temp = temp->next;
	}
	printf("\n");
}

void main(){

	int nodes;
	printf("Enter No. of Nodes\n");
	scanf("%d", &nodes);

	for(int i = 0; i < nodes; i++){
	
		addNodes();
	}

	printLL();
	minData();
}
