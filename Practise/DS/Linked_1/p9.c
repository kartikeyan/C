/*	Program 9 : WAP to check the prime number present in the data
 *		    from the above nodes.
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	int data;

	struct Demo *next;
}demo;

demo *head = NULL;

void addNodes(){

	demo *newNode = (demo*)malloc(sizeof(demo));

	printf("Enter Integer data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	if(head == NULL){
	
		head = newNode;
	}else{
	
		demo *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void prime(){

	demo *temp = head;

	int count = 0;

	while(temp != NULL){
	
		count = 0;

		if(temp->data==1)
			count=1;

		for(int i = 2; i <= (temp->data) ; i++){
			
		
			if((temp->data) % i == 0){
			
				count++;
			}
		}

		if(count ==1){
		
			printf("%d is prime number\n",temp->data);
		}else{
			printf("%d is not prime number\n",temp->data);
		}

		temp = temp->next;
	}
}

void printLL(){

	demo *temp = head;

	while(temp != NULL){
	
		printf("|Integer data is : %d |", temp->data);

		temp = temp->next;
	}

	printf("\n");
}

void main(){

	int nodes;
	printf("Enter No. of Nodes\n");
	scanf("%d", &nodes);

	for(int i = 0; i < nodes; i++){
	
		addNodes();
	}

	printLL();
	prime();
}
