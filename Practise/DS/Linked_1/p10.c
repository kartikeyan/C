/*	Program 10 : Write a real-time example for a linked list
 *		     and print its data. Take 5 nodes from the user.
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Flat{

	int noOfPersons;
	char bName[20];
	float rent;

	struct Flat *next;
}flat;

flat *head = NULL;

void addNodes(){

	flat *newNode = (flat*)malloc(sizeof(flat));

	printf("Enter No of Persons\n");
	scanf("%d", &newNode->noOfPersons);

	printf("Enter Building Name\n");
	scanf("%s", newNode->bName);
	
	printf("Enter Flat rent\n");
	scanf("%f", &newNode->rent);

	newNode->next = NULL;
	
	if(head == NULL){
		head = newNode;
	}else{
		flat *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;

		}
		temp->next = newNode;
	}	
}

void printLL(){
	
	flat *temp = head; 
	
	while(temp != NULL){
		
		printf("|No of Persons :%d ->", temp->noOfPersons);
		printf("Building Name :%s ->", temp->bName);
		printf("Flat Rent :%f |", temp->rent);

		temp = temp->next;
	}
	printf("\n");
}

void main(){

	int nodes;
	printf("Enter No. of nodes :\n");
	scanf("%d", &nodes);

	for(int i = 0; i < nodes; i++){
	
		addNodes();
	}

	printLL();
}
