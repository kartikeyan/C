/*	Program 6 :  Write a program that accepts a singly Linked List from the user.
 *		     Take a number from the user and print the data of the length of
 *		     that number.Length of Kanha = 5
 *
 *		     Input LInked List : |Shashi|->|Ashish|->|kanha|->|Rahul|->|Badhe|
 *
 *		     Input : Enter Number : 5 
 *
 *		     Ouput : Kanha
 *		     	     Rahul
 *		     	     Badhe
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	struct Demo *prev;

	char str[20];

	struct Demo *next;
}demo;

demo *head = NULL;

demo *createNode(){

	demo *newNode = (demo*)malloc(sizeof(demo));

	newNode->prev = NULL;
	printf("Enter String\n");	

	int ch;
	int i = 0;

	while((ch = getchar()) != '\n'){
	
		(*newNode).str[i] = ch;
		i++;
	}

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
	
		demo *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;

		newNode->prev = temp;
	}
}

int mystrlen(char *str){

	int count = 0;

	while(*str != '\0'){
	
		count++;
		str++;
	}
	return count;
}

void len(){
	
	int no;
	printf("Enter Number\n");
	scanf("%d", &no);

	demo *temp = head;

	while(temp != NULL){
	
		int count = mystrlen(temp->str);
		
		if(count == no){
		
			printf("%s\n", temp->str);
		}

		temp = temp->next;
	}
}

void printLL(){

	demo *temp = head;

	while(temp->next != NULL){
	
		printf("|%s|->", temp->str);

		temp = temp->next;
	}

	printf("|%s|\n", temp->str);
}

void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);
	getchar();

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	printLL();

	len();
}
