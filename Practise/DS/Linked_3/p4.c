/*	Program 4 :  Write a program that adds the digits of a data
 *		     element from a singly Linked List and changes the data.(sum of data elemet digits).
 *
 *		     Input LInked List : |11|->|12|->|13|->|141|->|2|->|158|->|15|
 *
 *		     Ouput Linked List : |2|->|3|->|4|->|6|->|2|->|14|->|6|
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	struct Demo *prev;
	int data;
	struct Demo *next;
}demo;

demo *head = NULL;

demo *createNode(){

	demo *newNode = (demo*)malloc(sizeof(demo));

	newNode->prev = NULL;

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
	
		demo *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;

		newNode->prev = temp;
	}
}

int addDigits(){

	demo *temp = head;

	while(temp != NULL){
		
		int sum = 0;

		while(temp->data > 0){
			
			int last = temp->data % 10;
			sum = sum + last;
			temp->data = temp->data / 10;
		}
		
		temp->data = sum;

		temp = temp->next;
	}
}

void printLL(){

	demo *temp = head;

	while(temp->next != NULL){
	
		printf("|%d|->", temp->data);

		temp = temp->next;
	}

	printf("|%d|\n", temp->data);
}

void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	printLL();

	addDigits();

	printLL();
}
