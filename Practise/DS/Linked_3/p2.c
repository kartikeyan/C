/*	Program 2 :  Write a program that searches for the second last occurance of a particular
 *		     element from a singly Linked List.
 *
 *		     Input LInked List : |10|->|20|->|30|->|40|->|30|->|30|->|70|
 *
 *		     Input : Enter Element : 30
 *
 *		     Ouput : 5
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	struct Demo *prev;
	int data;
	struct Demo *next;
}demo;

demo *head = NULL;

demo *createNode(){

	demo *newNode = (demo*)malloc(sizeof(demo));

	newNode->prev = NULL;

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
	
		demo *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;

		newNode->prev = temp;
	}
}

int secLastOcc(){

	int data;
	printf("Enter Element\n");
	scanf("%d", &data);

	int x = 0;
	int y = 0;
	int z = 1;

	demo *temp = head;

	while(temp != NULL){
		
		if(temp->data == data){
			
			x = y;
			y = z;
		}

		z++;

		temp = temp->next;
	}

	printf("%d\n", x);
}

void printLL(){

	demo *temp = head;

	while(temp->next != NULL){
	
		printf("|%d|->", temp->data);

		temp = temp->next;
	}

	printf("|%d|\n", temp->data);
}

void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	printLL();

	secLastOcc();
}
