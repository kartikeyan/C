/*	Program 5 :  Write a program that searches all the palindrome data elements
 *		     from a singly Linked List. And Print the position of palindrome data.
 *
 *		     Input LInked List : |12|->|121|->|30|->|252|->|35|->|151|->|70|
 *
 *		     Ouput Linked List : 
 *
 *		     Palindrome found at 2
 *		     Palindrome found at 4
 *		     Palindrome found at 6
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	struct Demo *prev;
	int data;
	struct Demo *next;
}demo;

demo *head = NULL;

demo *createNode(){

	demo *newNode = (demo*)malloc(sizeof(demo));

	newNode->prev = NULL;

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
	
		demo *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;

		newNode->prev = temp;
	}
}

void palindrome(){

	demo *temp = head;

	while(temp != NULL){
		
		int nodePos = 1;
		int nodeData = temp->data;
		int rev = 0;
		int remainder;

		printf("Node Data is : %d\n", nodeData);

		while(nodeData > 0){
		
			int remainder = nodeData % 10;
			rev = (rev * 10) + remainder;
			nodeData = nodeData / 10;
		}

		printf("Reverse = %d\n", rev);

		if(rev == temp->data){
		
			printf("Palindrome Found at Node : %d\n", nodePos);
			
		}else{
		
			printf("Palindrome Not Found : %d\n", nodePos);
			
		}
		
		nodePos++;
		temp = temp->next;
	}
}

void printLL(){

	demo *temp = head;

	while(temp->next != NULL){
	
		printf("|%d|->", temp->data);

		temp = temp->next;
	}

	printf("|%d|\n", temp->data);
}

void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	printLL();

	palindrome();
}
