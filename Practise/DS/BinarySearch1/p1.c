
/*	Program 1 : Given an sorted array with DISTINCT elements.
 *		    Find the floor of no k in the array.
 *
 *		    floor => greatest element <= k
 *
 *		    input : arr[2,3,6,9,10,11,14,18]
 *
 *		    Ouput:  k = 5,   floor = 3
 *		    	    k = 4,   floor = 3
 *		    	    k = 6,   floor = 6
*/

#include<stdio.h>

int linearSearch(int arr[], int size, int search){
	
	int store = -1;

	for(int i = 0; i < size; i++){
		
		if(arr[i] <= search){
			
			store = arr[i];
		}
	}

	return store;
}

int binarySearch(int arr[], int size, int search){

	int start = 0, end = size - 1, mid;
	
	int store = -1;

	while(start <= mid){
		
		mid = start + (end - start) / 2;

		if(arr[mid] == search){
			
			return arr[mid];
		}
		if(arr[mid] > search){
			
			end = mid - 1;
		}

		if(arr[mid] < search){
			
			store = arr[mid];

			start = mid + 1;
		}
	}

	return store;
}

void main(){
	
	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
	
		scanf("%d", &arr[i]);
	}
	
	int search;
	printf("Enter Search Ele\n");
	scanf("%d", &search);

	int ret1 = linearSearch(arr, size, search);

	int ret2 = binarySearch(arr, size, search);

	printf("%d %d\n", ret1,  ret2);
}
