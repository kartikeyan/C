
/*	Program 7 : Search for a range
 *		    
 *		    Given a sorted array of integrs A(0 based index) of size N,
 *		    find the starting and the endong position of a given integer B in array A.
 *		    Return an array of size 2, such that the
 *		    first element = starting poisition of B in A
 *		    second element = ending position of B in A
 *		    If B is not found in A return [-1,-1]
 *
 *		    input : arr1[5,7,7,8,8,10]  B = 8  ,    Input : arr2[5,17,100,111],    Input : arr3[3,5,7,9,11]
 *		    Ouput : [3,4]  ,		            Output : [-1,-1]               Ouput : [2,2]
 *		    	    
*/

#include<stdio.h>
/*
int linearSearch(int arr[], int size, int search){

	for(int i = 0; i < size; i++){
		
		if(arr[i] == search){
			
			return i;
		}
	}

	return -1;
}
*/
int *binarySearch(int arr[], int size, int search){

	int start = 0, end = size - 1, mid;
	
	int store1, store2 = -1;

	while(start <= mid){
		
		mid = start + (end - start) / 2;

		if(arr[mid] == search){
			
			store1 = mid;

			if(arr[mid+1] == search){
				
				store2 = mid + 1;
			}

			if(arr[mid-1] == search){
				
				store1 = mid - 1;
			}

			end = mid - 1;
		}

		if(arr[mid] < arr[start]){
			
			start = mid + 1;		
		}

		if(arr[mid] > arr[start]){
				
			end = mid - 1;	
		}
	}

	int arr1[] = {store1, store2};
	int *ptr = arr1;
	return ptr;
}

void main(){
	
	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
	
		scanf("%d", &arr[i]);
	}
	
	int search;
	printf("Enter Search Ele\n");
	scanf("%d", &search);

//	int ret1 = linearSearch(arr, size, search);

	int *ret2 = binarySearch(arr, size, search);

	printf("%d\n", *ret2);
}
