
/*	Program 6 : Find a peak element.
 *		    
 *		    Given an array of integrs A, find and return the peak element of it.
 *		    An array element is peak of it is NOT smaller than its neighbors.
 *		    For corner elements, we need to consider only one neighbor.
 *		    We ensure that the answer will be unique.
 *
 *		    NOTE : The array may have duplicate elements
 *
 *		    input : arr1[1,2,3,4,5]   ,    Input : arr2[5,17,100,11]
 *		    Ouput : 5  ,		        Ouput : 100
 *		    	    
*/

#include<stdio.h>
/*
int linearSearch(int arr[], int size){
	
	int store = 0;

	for(int i = 0; i < size; i++){
		
		if(arr[i] > store){
			
			store = arr[i];
		}
	}

	return store;
}
*/
int binarySearch(int arr[], int size){

	int start = 0, end = size - 1, mid;

	while(start < end){
		
		mid = start + (end - start) / 2;

		if(arr[mid] < arr[mid+1]){
			
			start = mid + 1;
		}else{
			end = mid;
		}
	}

	return arr[mid];
}

void main(){
	
	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
	
		scanf("%d", &arr[i]);
	}

//	int ret1 = linearSearch(arr, size);

	int ret2 = binarySearch(arr, size);

	printf("%d\n", ret2);
}
