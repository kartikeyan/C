
/*	Program 5 : Sorted Insert Position
 *		    
 *		    Given an sorted array A of size N and a target value B, return the index
 *		    	
 *		    	-(0-based indexing) if the target is found
 *		    	- If not, return the index where it would be if it were inserted in order.
 *
 *		    input : arr1[1,3,5,6]  B = 5   ,    Input : arr2[1,3,5,7]  B = 6
 *		    Ouput : 2  ,		        Ouput : 3
 *		    	    
*/

#include<stdio.h>

int linearSearch(int arr[], int size, int search){

	int store = -1;

	for(int i = 0; i < size; i++){
		
		if(arr[i] == search){
			
			return i;
		}

		if(arr[i] < search){
			
			store = i;
		}
	}

	return store + 1;
}

int binarySearch(int arr[], int size, int search){

	int start = 0, end = size - 1, mid, store = -1;

	while(start <= mid){
		
		mid = start + (end - start) / 2;

		if(arr[mid] == search){
			
			return mid;
		}

		if(arr[mid] < search){
			
			store = mid;

			start = mid + 1;
		}

		if(arr[mid] > search){
			
			end = mid - 1;
		}
	}

	return store + 2;
}

void main(){
	
	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
	
		scanf("%d", &arr[i]);
	}
	
	int search;
	printf("Enter Search Ele\n");
	scanf("%d", &search);

	int ret1 = linearSearch(arr, size, search);

	int ret2 = binarySearch(arr, size, search);

	printf("%d %d\n", ret1, ret2);
}
