
/*	Program 4 : Single Element in Sorted Array
 *		    
 *		    Given an sorted array of integers A where every element appears twice
 *		    except for one element which appears once, find and return this single
 *		    element that appears only once
 *
 *		    input : arr1[1,1,7],    Input : arr2[2,3,3]
 *		    Ouput : 7,		    Ouput : 2
 *		    	    
*/

#include<stdio.h>

int linearSearch(int arr[], int size, int search){

	for(int i = 0; i < size; i+2){
		
		if(arr[i] && arr[i+1] != search){
			
			return arr[i];
		}
	}

	return -1;
}

void main(){
	
	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
	
		scanf("%d", &arr[i]);
	}
	
	int search;
	printf("Enter Search Ele\n");
	scanf("%d", &search);

	int ret1 = linearSearch(arr, size, search);

	printf("%d\n", ret1);
}
