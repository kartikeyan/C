/*	
 *	Program 5 : WAP that accepts two singly linear lists from the user and 
 *		    also accept range and concate elements of the source linked list
 *		    from that range after destination linked list
 *
 *	Input LL : 
 *		       Source LL  : |30|->|30|->|70|->|80|->|90|->|100|
 *		   Destination LL : |30|->|40|
 *
 *	Input Starting range : 2
 *
 *	Input Ending range : 5
 *
 *	Output LL : 
 *		   Destination LL : |30|->|40|->|30|->|70|->|70|->|80|->|90|
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int no;
	struct Node *next;
}node;

node *head1 = NULL;
node *head2 = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter No\n");
	scanf("%d", &newNode->no);

	newNode->next = NULL;

	return newNode;
}

void addNode(struct Node **head){

	node *newNode = createNode();

	if(*head == NULL){
		*head = newNode;
	}else{
	
		node *temp = *head;

		while(temp->next != NULL){
			
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

int countNodes(){

	node *temp = head1;
	
	int count = 0;

	while(temp != NULL){
		
		count++;

		temp = temp->next;
	}

	return count;
}

void concateLL(){

	node *temp = head2;

	while(temp->next != NULL){
	
		temp = temp->next;
	}

	temp->next = head1;
}

/*
int concateFirstN(int num){
	
	int count = countNodes();

	if(num < 0 || num > count){
		
		printf("Invalid\n");
		return -1;

	}else if(num == 0){
		
		concateLL();
	}else{

		node *temp = head2;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		int val = countNodes() - num;

		node *temp2 = head1;

		while(val){
		
			temp2 = temp2->next;
			val--;
		}

		temp->next = temp2;
	}
}
*/

int concate(int start, int end){
	
	int count = countNodes();

	if(start <= 0 || start > count){
	
		printf("Invalid\n");
		return -1;
	}

	if(end <= 0 || end > count){
	
		printf("Invalid\n");
		return -1;
	}

	node *temp1 = head1;
	node *temp2 = head1;

	int val1 = start - 1;
	
	while(val1 != 0){
	
		temp1 = temp1->next;
		val1--;
	}

	int val2 = end - 1;

	while(val2 != 0){
	
		temp2 = temp2->next;
		val2--;
	}

	free(temp2->next);
	temp2->next = NULL;

	node *temp3 = head2;

	while(temp3->next != NULL){
	
		temp3 = temp3->next;
	}

	temp3->next = temp1;
}

int printLL(node *head){

	if(head == NULL){
		
		printf("LL is empty\n");
		return -1;
	}else{
	
		node *temp = head2;

		while(temp->next != NULL){
		
			printf("|%d|->", temp->no);

			temp = temp->next;
		}

		printf("|%d|\n", temp->no);
	}
}

void main(){

	int nodeCount;
	printf("Enter Node Count : SourceLL\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode(&head1);
	}

	printf("Enter Node Count : DestinationLL\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode(&head2);
	}

	printLL(head1);

	printLL(head2);

/*	int num;
	printf("Enter No of Nodes to connect\n");
	scanf("%d", &num);

	concateFirstN(num);
*/	
	int start, end;
	printf("Input Starting range\n");
	scanf("%d", &start);

	printf("Input ending range\n");
	scanf("%d", &end);
	
	concate(start, end);

	printLL(head2);
}
