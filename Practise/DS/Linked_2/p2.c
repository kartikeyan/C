/*	Program 2 : Write a program that searches for the second last occurance of a
 *		    particular element from a singly Linked list
 *
 *		    Input Linked List : |10|->|20|->|30|->|40|->|30|->|30|->|70|
 *
 *		    Input Enter Element : 30
 *
 *		    Output = 5
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;
}node;

node *head = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	node *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
	}else{
		
		node *temp = head;

		while(temp->next != NULL){
			
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void secLastOcc(){

	int data;
	printf("Enter particular data\n");
	scanf("%d", &data);

	int x = 0;
	int y = 0;
	int z = 1;

	node *temp = head;

	while(temp != NULL){
	
		if(temp->data == data){
		
			x = y;
			y = z;
		}

		z++;

		temp = temp->next;
	}

	printf("%d\n", x);
}

void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	secLastOcc();
}
