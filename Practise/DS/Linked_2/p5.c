/*	Program 5 : Write a program that searches all the Palindrome data elements
 *		   from a singly Linked list. And Print the position of palindrome data.
 *
 *		    Input Linked List : |12|->|121|->|30|->|252|->|35|->|151|->|70|
 *
 *		    Output Linked List : 
 *
 *		    Palindrome found at 2
 *		    Palindrome found at 4
 *		    Palindrome fount at 6
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;
}node;

node *head = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	node *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
	}else{
		
		node *temp = head;

		while(temp->next != NULL){
			
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void palindrome(){
	
	node *temp = head;
	
	while(temp != NULL){
		
		int nodeAt = 1;
		int nodeData = temp->data;
		int rev = 0;
		int remainder;
		
		printf("Node data is : %d\n", nodeData);

		while(nodeData > 0){
	
			int remainder = nodeData % 10;
			rev = (rev * 10) + remainder;
			nodeData = nodeData / 10;
		}

		printf("Reverse = %d\n", rev);

		if(rev == temp->data){
	
			printf("Palindrome found at node count : %d\n", nodeAt);
		}else{
	
			printf("Palindrome not found\n");
		}

		nodeAt++;
		temp = temp->next;
	}
}

void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	palindrome();
}
