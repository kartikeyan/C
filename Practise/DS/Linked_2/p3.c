/*	Program 3 : Write a program that searches the occurance of a
 *		    particular element from a singly Linked list
 *
 *		    Input Linked List : |10|->|20|->|30|->|40|->|50|->|30|->|70|
 *
 *		    Input Enter Element : 30
 *
 *		    Output = 2 times
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;
}node;

node *head = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	node *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
	}else{
		
		node *temp = head;

		while(temp->next != NULL){
			
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void findOcc(){

	int data;
	printf("Enter Data for particular element\n");
	scanf("%d", &data);

	node *temp = head;
	
	int count = 0;

	while(temp != NULL){
	
		if(temp->data == data){
		
			count++;
		}

		temp = temp->next;
	}

	printf("%d\n", count);
}

void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	findOcc();
}
