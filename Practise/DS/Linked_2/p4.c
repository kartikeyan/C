/*	Program 4 : Write a program that adds thr digits of a data element
 *		   from a singly Linked list and changes the data.(sum of data element digits)
 *
 *		    Input Linked List : |11|->|12|->|13|->|141|->|2|->|158|
 *
 *		    Output Linked List : |2|->|3|->|4|->|6|->|2|->|14|
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;
}node;

node *head = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	node *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
	}else{
		
		node *temp = head;

		while(temp->next != NULL){
			
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void addDigits(){

	node *temp = head;

	while(temp != NULL){

		int sum = 0;
	
		while(temp->data > 0){
		
			int last = temp->data % 10;
			sum = sum + last;
			temp->data = temp->data / 10;
		}

		temp->data = sum;

		temp = temp->next;
	}
}

int printLL(){

	node *temp = head;
	
	if(head == NULL){
	
		printf("LL is empty\n");
		return -1;
	}

	while(temp->next != NULL){
	
		printf("|%d|->", temp->data);

		temp = temp->next;
	}

	printf("|%d|\n", temp->data);
}

void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	addDigits();

	printLL();
}
