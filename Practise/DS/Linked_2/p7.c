/*	PRogram 7 : Write a program that accepts a singly linear linked list from the user.
 *		    Reverse the data elements from the linked list.
 *
 *		    Input Linked List : |Shashi|->|Ashish|->|Kanha|->|Rahul|->|Badhe|
 *
 *		    Output Linked List : |ihsahS|->|hsihsA|->|ahnaK|->|luhaR|->|ehdaB|
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct node{

	char str[20];
	struct node *next; 
}node;

node *head = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter String\n");

	int ch;
	int i = 0;

	while((ch = getchar()) != '\n'){
		
		(*newNode).str[i] = ch;
		i++;
	}

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	node *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
	}else{
	
		node *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

char *mystrrev(char *str){

	char *temp = str;

	while(*temp != '\0'){
	
		temp++;
	}

	temp--;

	while(str < temp){
	
		char x = *str;
		*str = *temp;
		*temp = x;

		str++;
		temp--;
	}

	return str;
}

void reverse(){

	node *temp = head;

	while(temp != NULL){
	
		mystrrev(temp->str);

		temp = temp->next;
	}

	printf("\n");
}

void printLL(){

	node *temp = head;

	while(temp->next != NULL){
	
		printf("|%s|->", temp->str);

		temp = temp->next;
	}

	printf("|%s|\n", temp->str);
}

void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	getchar();

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}
	
	if(head != NULL){

		reverse();
		printf("After reverse the elements\n");

		printLL();
	}else{
	
		printf("NO nodes are present add some nodes\n");
	}
}
