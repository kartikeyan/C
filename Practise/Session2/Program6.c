#include<stdio.h>

void main(){

	int x, rem;
	int count = 0;

	printf("Enter number x:\n");
	scanf("%d", &x);

	while(x > 0){
		rem = x % 10;
		count++;
		x = x / 10;
	}
	printf("Number of Digits are %d\n", count);
}
