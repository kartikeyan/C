/*	Program 4:WAP to take a number as input and print whether it is prime number or not
 *
*/

/*
#include<stdio.h>

void main(){

	int n;
	printf("Enter value of n :\n");
	scanf("%d", &n);
	int count = 0;
		for (int i = 1; i<=n; i++){
			if(n%i==0)
				count++;
		}
		if(count==2) 
			printf("%d is a prime number\n", n);
		else 
			printf("%d is not a prime number\n", n);
}*/

#include<stdio.h>
void main(){
	int n;

	printf("Enter number :\n");
	scanf("%d", &n);

	int i;
	for(i = 2; i < n; i++){
		if(n % i == 0){
			printf("%d is non-prime number\n", n);
			break;
		}
	}
	if(n == i){
		printf("%d is prime number\n", i);
	}

}
