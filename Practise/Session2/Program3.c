/*	Program 3:WAP to print the divisiors and count the divisiors of the entered num.
 *
 *	Input : 15
 *	Output : the divisiors are 1 3 5 15
 *		 The count of divisiors is 4
 *		 Addition of divisiors 24
*/

#include<stdio.h>

void main(){
	int x;
	int count = 0;
	int sum = 0;
	
	printf("Enter number x :\n");
	scanf("%d", &x);
	
	printf("The divisors are :");
	
	int i;
	for(i=1; i<=x; i++){
		if(x % i == 0){
			printf("%d ", i);

			sum = sum + i;
			count++;
		}
	}
	printf("\n");
	printf("The count of divisors are :%d\n", count);
	printf("Additions of divisors :%d\n", sum);
}
